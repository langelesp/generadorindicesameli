package org.csg.Indexador;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * Clase que se encarga de la escritura del Log
 * 
 * @author Usuario
 *
 */
public class FicheroLog {
	public static Logger logger = Logger.getLogger("MyLog");
	public static String fechaS = "";
	public static FileHandler fh;

	/**
	 * Se encarga de realizar la escritura de la salida del programa a un archivo de
	 * texto plano.
	 * 
	 * @param log
	 *            String que contiene el texto que ser� escrito.
	 * @param rutaLog
	 *            Ruta en la que se almacenar� el archivo Log
	 * @return Una variable de tipo String que almacena la ruta donde se alojo el
	 *         fichero Log.
	 */
	public void crearLog(String rutaLog) {
		File f;
		f = new File(rutaLog + "\\logIndexador");
		if (!f.exists()) {
			f.mkdirs();
		}
		Date fecha = new Date();
		fechaS = fecha.toString().replaceAll("\\s", "_").replaceAll(":", "-");
		fechaS = rutaLog + "\\logIndexador\\" + fechaS + ".log";
		System.out.println("\nLog creado -> " + fechaS);
		f = new File(fechaS);
		try {
			if (!f.exists()) {
				f.createNewFile();
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		try {
			try {
				fh = new FileHandler(fechaS);
			} catch (IOException e) {
				e.printStackTrace();
			}
			logger.addHandler(fh);
			SimpleFormatter formatter = new SimpleFormatter();
			fh.setFormatter(formatter);
			
		} catch (SecurityException e) {
			e.printStackTrace();
		}
	}



}
