package org.csg.Indexador;

import java.io.File;
import java.sql.Connection;
import java.util.ArrayList;

import org.apache.commons.io.FilenameUtils;
import org.csg.ConectionPostgreSQL.PostgreSQLJDBC;
import org.csg.Indexador.DAO.ArticuloDAO;

public class RunIndexador {
	static ArrayList<String> filesXML = new ArrayList<>();
	public static void main(String[] args) {

		PostgreSQLJDBC cpostgree = new PostgreSQLJDBC();
		Connection connect = null;

		connect = cpostgree.conectar();
		listarXMLs("F:\\jatsRepo");
		eliminarIndex("C:\\indiceArticulos");

		Thread hiloArt = new ArticuloDAO("C:\\indiceArticulos", filesXML, connect);
		hiloArt.start();
	}

	public static void listarXMLs(String directory) {
		try {
			File file = new File(directory);
			if (file.exists()) {
				if (file.isDirectory()) {
					
					try {
						File[] ficherosJATS = file.listFiles();

						for (int i = 0; i < ficherosJATS.length; i++) {
							if (ficherosJATS[i].isDirectory()) {
								listarXMLs(ficherosJATS[i].toString());
							} else {
								String xml = ficherosJATS[i].toString();///>>
//						tarea1		String origen = xml.contains("redalyc") ? ">>>R" : ">>>A";
//								xml = xml + origen;
								System.out.println("xml----" + xml);
								String ext = FilenameUtils.getExtension(xml);
								if (ext.equals("xml")) {
									filesXML.add(xml);
								}
							}
						}
					} catch (Exception ex) {
						System.out.println(">>>>>>  El directorio -> " + file.toString() + " no contiene archivos ");
					}
				}
			} else {
				System.out.println(">>>>>>  �ERROR!.. Directorio de XMLs No Valido ");
				System.exit(0);
			}
		} catch (Exception ex) {
			System.out.println(">>>>>>  �ERROR!.. Directorio de XMLs No Valido ");
			System.exit(0);
		}
	}
	public static void eliminarIndex(String pathIndex) {
		File file = new File(pathIndex);
		if (file.isDirectory()) {
			try {
				File[] ficheros = file.listFiles();
				for (int i = 0; i < ficheros.length; i++) {
					if (ficheros[i].isFile() && !ficheros[i].getName().equals("stopwords.txt")) {
						ficheros[i].delete();
					}
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		} else {
			System.out.println("�ERROR! Directorio no valido -> M�todo eliminarIndex");
		}

	}

}
