package org.csg.Indexador;

import java.util.ArrayList;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.csg.Indexador.Modelo.ModeloField;

/**
 * <p>
 * Clase que realiza la creaci�n del documento que sera indizado al �ndice
 * previamente creado.
 * <p>
 * 
 * @author Usuario
 *
 */

public class DocumentoIndex {

	/**
	 * Contruye el modelo del Documento que ser� indizado, en este se indica el
	 * nombre que tendra cada campo y el contenido correspondiente
	 * 
	 * 
	 * @param modeloDoc
	 *            Lista que almacena el nombre del campo y su respoectivo contenido
	 *            de acuerdo a cada uno
	 * @return doc Una variable de tipo <b>Document</b> que contiene el documento
	 *         que será indizado
	 */
	public Document indizarDocumento(ArrayList<ModeloField> modeloDoc) {
		Document doc = new Document();
		
		for (int i = 0; i < modeloDoc.size(); i++) {
			doc.add(new TextField(
					
					modeloDoc.get(i).getNameField(),
					
					modeloDoc.get(i).getExiste() == true? (modeloDoc.get(i).getContenidoField() == null ? "" : modeloDoc.get(i).getContenidoField()): "",
					
							Field.Store.YES));
		}
		return doc;
	}

}
