package org.csg.Indexador.Modelo;

import java.util.ArrayList;

public class ModeloJsonColeccion {
	
	ArrayList<ModeloAreasColeccion> areas;
	ArrayList<ModeloNumerariasColeccion> paises;
	ArrayList<ModeloNumerariasColeccion> instituciones;
	
	public ModeloJsonColeccion(ArrayList<ModeloAreasColeccion> _areas, ArrayList<ModeloNumerariasColeccion> _paises, ArrayList<ModeloNumerariasColeccion> _ins){
	this.areas = _areas;
	this.paises = _paises;
	this.instituciones = _ins;
	}


	public ArrayList<ModeloNumerariasColeccion> getPaises() {
		return paises;
	}

	public void setPaises(ArrayList<ModeloNumerariasColeccion> paises) {
		this.paises = paises;
	}

	public ArrayList<ModeloNumerariasColeccion> getInstituciones() {
		return instituciones;
	}

	public void setInstituciones(ArrayList<ModeloNumerariasColeccion> instituciones) {
		this.instituciones = instituciones;
	}


	public ArrayList<ModeloAreasColeccion> getAreas() {
		return areas;
	}


	public void setAreas(ArrayList<ModeloAreasColeccion> areas) {
		this.areas = areas;
	}

	

}
