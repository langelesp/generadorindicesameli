package org.csg.Indexador.Modelo;

public class ModeloField {

	private String nameField;
	private String contenidoField;
	private Boolean existe;
	private int pos;
	private boolean prueba;

	public ModeloField(String nameField, String contenidoField, Boolean existe, int pos, boolean prueba) {
		this.nameField = nameField;
		this.contenidoField = contenidoField;
		this.existe = existe;
		this.pos = pos;
	}
	
	public ModeloField(String nameField, String contenidoField, Boolean existe, int pos) {
		this.nameField = nameField;
		this.contenidoField = contenidoField;
		this.existe = existe;
		this.pos = pos;
	}

	public String getNameField() {
		return nameField;
	}

	public void setNameField(String nameField) {
		this.nameField = nameField;
	}

	public String getContenidoField() {
		return contenidoField;
	}

	public void setContenidoField(String contenidoField) {
		this.contenidoField = contenidoField;
	}

	public Boolean getExiste() {
		return existe;
	}

	public void setExiste(Boolean existe) {
		this.existe = existe;
	}

	public int getPos() {
		return pos;
	}

	public void setPos(int pos) {
		this.pos = pos;
	}

}
