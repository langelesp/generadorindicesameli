package org.csg.Indexador.Modelo;

import java.util.ArrayList;

public class ModeloAreasColeccion {

	ArrayList<ModeloNumerariasColeccion> cienciasSociales;
	ArrayList<ModeloNumerariasColeccion> cienciasNaturales;
	ArrayList<ModeloNumerariasColeccion> arteHumanidades;
	ArrayList<ModeloNumerariasColeccion> multidisciplinarias;

	public ModeloAreasColeccion(ArrayList<ModeloNumerariasColeccion> _sociales,
			ArrayList<ModeloNumerariasColeccion> _naturales, ArrayList<ModeloNumerariasColeccion> _arteHumanidades,
			ArrayList<ModeloNumerariasColeccion> _multidisciplinarias) {

		this.cienciasSociales = _sociales;
		this.cienciasNaturales = _naturales;
		this.arteHumanidades = _arteHumanidades;
		this.multidisciplinarias = _multidisciplinarias;

	}

	public ArrayList<ModeloNumerariasColeccion> getCienciasSociales() {
		return cienciasSociales;
	}

	public void setCienciasSociales(ArrayList<ModeloNumerariasColeccion> cienciasSociales) {
		this.cienciasSociales = cienciasSociales;
	}

	public ArrayList<ModeloNumerariasColeccion> getCienciasNaturales() {
		return cienciasNaturales;
	}

	public void setCienciasNaturales(ArrayList<ModeloNumerariasColeccion> cienciasNaturales) {
		this.cienciasNaturales = cienciasNaturales;
	}

	public ArrayList<ModeloNumerariasColeccion> getArteHumanidades() {
		return arteHumanidades;
	}

	public void setArteHumanidades(ArrayList<ModeloNumerariasColeccion> arteHumanidades) {
		this.arteHumanidades = arteHumanidades;
	}

	public ArrayList<ModeloNumerariasColeccion> getMultidisciplinarias() {
		return multidisciplinarias;
	}

	public void setMultidisciplinarias(ArrayList<ModeloNumerariasColeccion> multidisciplinarias) {
		this.multidisciplinarias = multidisciplinarias;
	}

}
