package org.csg.Indexador.Modelo;

public class ModeloJsonColeccionMapa {

	private String nombre;
	private String clave;
	private String codeISO3;
	private String codeISO2;
	private int z;

	public ModeloJsonColeccionMapa(String name, String clave, String codeISO3, String codeISO2, int total) {
		this.nombre = name;
		this.clave = clave;
		this.codeISO3 = codeISO3;
		this.codeISO2 = codeISO2;
		this.z = total;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getCodeISO3() {
		return codeISO3;
	}

	public void setCodeISO3(String codeISO3) {
		this.codeISO3 = codeISO3;
	}

	public String getCodeISO2() {
		return codeISO2;
	}

	public void setCodeISO2(String codeISO2) {
		this.codeISO2 = codeISO2;
	}

	public int getZ() {
		return z;
	}

	public void setZ(int z) {
		this.z = z;
	}



	public String getNombre() {
		return nombre;
	}



	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


}
