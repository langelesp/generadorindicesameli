package org.csg.Indexador.Modelo;

public class ModeloNumerariasColeccion {

	String clave;
	String nombre;
	int total;

	public ModeloNumerariasColeccion(String nombre, String clave, String total) {
		this.clave = clave;
		this.nombre = nombre;
		this.total = Integer.parseInt(total);
	}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

}
