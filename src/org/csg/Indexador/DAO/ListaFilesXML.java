package org.csg.Indexador.DAO;

import java.io.File;
import java.util.ArrayList;

import org.apache.commons.io.FilenameUtils;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.paint.Color;

public class ListaFilesXML extends Thread {

	public static ArrayList<String> filesXML = new ArrayList<>();
	private String directory = "";
	Label lblValConex = new Label();
	Button btnGenerar = new Button();
	int cont = 0;

	public ListaFilesXML() {

	}

	public ListaFilesXML(String jatsRepo, Label lblValConex, Button btnGenerar) {
		this.directory = jatsRepo;
		this.lblValConex = lblValConex;
		this.btnGenerar = btnGenerar;

	}

	public void run() {
		filesXML.clear();
		listarXMLs(this.directory);
		if (filesXML.isEmpty()) {
			lblValConex.setTextFill(Color.web("#EC7063"));
			lblValConex.setText(lblValConex.getText() + " �ERROR! No se encontrar�n XMLs en la ruta indicada");
		} else {
			btnGenerar.setDisable(false);
		}
		System.out.println(filesXML.size());

	}

	public void listarXMLs(String directory) {
		try {
			File file = new File(directory);
			if (file.exists()) {
				if (file.isDirectory()) {
					try {
						File[] ficherosJATS = file.listFiles();

						for (int i = 0; i < ficherosJATS.length; i++) {
							if (ficherosJATS[i].isDirectory()) {
								listarXMLs(ficherosJATS[i].toString());
							} else {
								String xml = ficherosJATS[i].toString();
								String ext = FilenameUtils.getExtension(xml);
								if (ext.equals("xml")) {
									ListaFilesXML.filesXML.add(xml);
									cont++;
									System.out.println("Listando: " + cont + " xmls ");
								}
							}
						}
					} catch (Exception ex) {
						System.out.println(">>>>>>  El directorio -> " + file.toString() + " no contiene archivos ");
					}
				}
			} else {
				System.out.println(">>>>>>  ¡ERROR!.. Directorio de XMLs No Valido ");
				System.exit(0);
			}
		} catch (Exception ex) {
			System.out.println(">>>>>>  ¡ERROR!.. Directorio de XMLs No Valido ");
			System.exit(0);
		}
	}

}
