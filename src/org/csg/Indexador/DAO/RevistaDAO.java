package org.csg.Indexador.DAO;

import java.io.BufferedReader;
import java.io.FileReader;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.ResourceBundle;

import org.apache.lucene.analysis.CharArraySet;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.csg.ConectionPostgreSQL.PostgreSQLJDBC;
import org.csg.ConnectionOracle.OracleSQLJDBC;
import org.csg.Indexador.DocumentoIndex;
import org.csg.Indexador.FicheroLog;
import org.csg.Indexador.Modelo.ModeloField;

import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 * <p>
 * Clase que realiza el proceso de obtención de datos de Revistas, para ser
 * indizados.
 * <p>
 * <p>
 * Esta clase se conecta a la base de datos para obtener dichos datos, estre
 * estos estan:
 * <p>
 * <ul>
 * <li>Clave de Revista</li>
 * <li>Nombre</li>
 * <li>ISSN impreso</li>
 * <li>ISSN electrónico</li>
 * <li>Institución</li>
 * <li>Idioma principal</li>
 * <li>País</li>
 * <li>Correo de la revista</li>
 * <li>Disciplina principal</li>
 * <li>Disciplina secundaria</li>
 * <li>Disciplina terciaria</li>
 * <li>Periodicidad</li>
 * <li>Característica de la publicación</li>
 * <li>Sitio web de la revista</li>
 * <li>Sitio web de la institución editora</li>
 * <li>Estado de la revista</li>
 * <li>Revista en CSG</li>
 * <li>Clave de revista en Redalyc</li>
 * <li>Indizaciones</li>
 * <li>Directorio</li>
 * <li>Imagen de la revista</li>
 * <li>Normas para los autores</li>
 * </ul>
 * 
 * @author Usuario
 *
 */
public class RevistaDAO extends Thread {

	ArrayList<ModeloField> fieldsRev = new ArrayList<>();
	ArrayList<String> cvesRevs = new ArrayList<>();
	private Connection connect = null;
	private Connection oracleConnect = null;
	PostgreSQLJDBC postgresql = new PostgreSQLJDBC();
	private OracleSQLJDBC oraclesql = new OracleSQLJDBC();
	DocumentoIndex docIndex = new DocumentoIndex();
	FicheroLog ficherolog = new FicheroLog();
	ResourceBundle resourcebundle = null;
	ResourceBundle bundlePaths = null;
	ResultSet resulSet = null;
	String query = "";
	StandardAnalyzer analyzer;
	Directory index;
	IndexWriterConfig config;
	IndexWriter writoo;
	String textoTextArea = "";
	RadioButton radioArt = new RadioButton();
	RadioButton radioRev = new RadioButton();
	Button btnValidar = new Button();
	TextArea txtarea = new TextArea();
	TextArea txtLog = new TextArea();
	ProgressBar pb = new ProgressBar();
	TextField fieldprogress = new TextField();
	String progreso = "";
	String rutaIndice = "";
	ArrayList<Object> componentes = new ArrayList<>();

	/**
	 * Contructor que inicializa las variables para obtener los datos y realizar la
	 * indexación
	 * 
	 * @param pathIndex
	 *            String que indica la ruta donde se alojara el índice
	 * @param connect
	 *            Conexión realizada previamente a la base de datos
	 * @param componentes
	 *            Lista que contiene los componentes que se muestran en la escena
	 *            para ser actualizados en esta clase
	 */
	public RevistaDAO(String pathIndex, Connection connect, ArrayList<Object> componentes) {
		rutaIndice = pathIndex;
		this.componentes = componentes;
		this.radioArt = (RadioButton) componentes.get(0);
		this.radioRev = (RadioButton) componentes.get(1);
		this.btnValidar = (Button) componentes.get(2);
		this.txtarea = (TextArea) componentes.get(5);
		this.pb = (ProgressBar) componentes.get(6);
		this.fieldprogress = (TextField) componentes.get(8);
		this.txtLog = (TextArea) componentes.get(9);
		
		ficherolog.crearLog(rutaIndice);
		resourcebundle = ResourceBundle.getBundle("querys");
		bundlePaths = ResourceBundle.getBundle("paths");
		try {
			this.connect = connect;
			this.oracleConnect = oraclesql.connection();
//			System.out.println("oraclesql.iniciado oracleConnect.consultacualquiera:::");
//			oraclesql.consultapre(this.oracleConnect);
			analyzer = new StandardAnalyzer(stopwords());
			index = FSDirectory.open(Paths.get(pathIndex));
			config = new IndexWriterConfig(analyzer);
			writoo = new IndexWriter(index, config);
		} catch (Exception ex) {

		}
	}

	/**
	 * Método que contiene el subproceso a realizar la indexación de los datos
	 * 
	 */
	public void run() {
		try {
//			indizaRedalyc();
			obtenerCvesRevs();
			String cveRev = "";
			String rutaLog = FicheroLog.fechaS;
			if (!cvesRevs.isEmpty()) {
				int total = cvesRevs.size();
				double progress = 0;
				double aux = 0;
				for (int i = 0; i < cvesRevs.size(); i++) {
					System.out.println("cvesRevs:::" + cvesRevs.get(i));
					try {
					startFielsRev();/*inicia el objeto que llenará el indice*/
					fieldsRev.get(32).setContenidoField("Ameli");
					fieldsRev.get(32).setExiste(true);
					cveRev = cvesRevs.get(i);
					obtDatosGrales(cveRev);
					obtIndizacion(cveRev);
					obtComite(cveRev);
					obtNumeros(cveRev);
					obtAreas(cveRev);
					valCambioRev(cveRev);
					obtEncabezado(cveRev);
					obtpalClave(cveRev);
					indexarDoc();
					textoTextArea = ">>> Se ha indizado REVISTA con clave -> " + cveRev + " Nombre: "
							+ fieldsRev.get(1).getContenidoField();
//					txtarea.setText((textoTextArea + "\n"));
					FicheroLog.logger.info(textoTextArea);
					
					aux = ((i + 1) * 1);
					progress = (aux / total);
					progreso = String.valueOf(progress * 100);
					progreso = progreso.substring(0, progreso.indexOf('.'));
//					fieldprogress.setText(progreso + "%");
					pb.setProgress(progress);
					fieldsRev.clear();
					}catch(Exception e) {
						
					}
				}
				try {
//					txtLog.setText("Log creado -> " + " " + rutaLog);
					writoo.close();
					postgresql.closeConnect(this.connect);
					FicheroLog.fh.close();
				} catch (Exception ex) {

				}
			}
		}catch(IllegalStateException iee) {
			
		} catch (Exception ex) {
//			ex.printStackTrace();
			textoTextArea = textoTextArea + ">>> ERROR! No se puede continuar con la indexación -> " + ex.getCause();
			txtarea.setText(txtarea.getText() + (textoTextArea + "\n"));
			FicheroLog.logger.warning(textoTextArea);
			
			textoTextArea = textoTextArea + ">>> Es necesario reiniar la Indexación ";
			txtarea.setText(txtarea.getText() + textoTextArea);
			FicheroLog.logger.warning(textoTextArea);
			System.exit(0);
		}
		try {
		((RadioButton) componentes.get(0)).setDisable(false);
		((RadioButton) componentes.get(1)).setDisable(false);
		((RadioButton) componentes.get(10)).setDisable(false);
		((RadioButton) componentes.get(11)).setDisable(false);
		((RadioButton) componentes.get(13)).setDisable(false);
		
		((Button) componentes.get(2)).setDisable(false);
		btnValidar.setDisable(false);
		}catch(Exception e) {
			
		}
	}
	
//	private void indizaRedalyc() {
//		ArrayList<String> cvesRevsReda = obtenerCvesRevsRedalyc();
////		System.out.println(cvesRevsReda.toString());
////		System.out.println(cvesRevsReda.size());
//		String cveRev = "";
//		String rutaLog = FicheroLog.fechaS;
//		if (!cvesRevsReda.isEmpty()) {
//			int total = cvesRevsReda.size();
//			int total1 = 0;
//			double progress = 0;
//			double aux = 0;
//			try {
//				for (int i = 0; i < cvesRevsReda.size(); i++) {
//				cveRev = cvesRevsReda.get(i);/*agrega la clave de revista*/
//					total1++;
//					startFielsRev();/*inicia el objeto que llenará el indice*/
//					fieldsRev.get(32).setContenidoField("Redalyc");
//					fieldsRev.get(32).setExiste(true);
//					obtDatosGralesRedalyc(cveRev);
//					obtIndizacionRedalyc(cveRev);/*2partes.properties*/
//					obtComiteRedalyc(cveRev);/*checar alma*/
//					obtNumerosRedalyc(cveRev);
//					obtAreasRedalyc(cveRev);
//					valCambioRevRedalyc(cveRev);
//					obtEncabezadoRedalyc(cveRev);
//					obtpalClaveRedalyc(cveRev);
//					indexarDoc();
//					textoTextArea = ">>> Se ha indizado REVISTA con clave -> " + cveRev + " Nombre: "
//							+ fieldsRev.get(1).getContenidoField();
//					//txtarea.setText((textoTextArea + "\n"));
//					FicheroLog.logger.info(textoTextArea);
//					aux = ((i + 1) * 1);
//					progress = (aux / total);
//					progreso = String.valueOf(progress * 100);
//					progreso = progreso.substring(0, progreso.indexOf('.'));
//					//fieldprogress.setText(progreso + "%");
//					pb.setProgress(progress);
//					fieldsRev.clear();
//			}
//			
////				txtLog.setText("Log creado -> " + " " + rutaLog);
//				//writoo.close();
//				oraclesql.closeConnect(this.oracleConnect);
//				FicheroLog.fh.close();
//			} catch (Exception ex) {
//
//			}
//			System.out.println("total:::" + total + ":::total1:::" + total1);
//		}
//	}

	/**
	 * Obtiene todas las claves de revistas activas y con estado 9, 10
	 * 
	 * @return ArrayList con las claves de revista
	 */
	public ArrayList<String> obtenerCvesRevs() {
		String query = resourcebundle.getString("obtenerCvesRevs");
		resulSet = postgresql.consultar(query, connect);
		try {
			if (resulSet != null) {
				while (resulSet.next()) {
					cvesRevs.add(resulSet.getString("claveRev"));
				}
			} else {
				System.exit(0);
			}
		} catch (Exception ex) {

		}
		return cvesRevs;
	}
	
	/**
	 * Obtiene todas las claves de revistas activas y con estado 9, 10
	 * 
	 * @return ArrayList con las claves de revista
	 */
	public ArrayList<String> obtenerCvesRevsRedalyc() {
		ArrayList<String> cvesRevsL = new ArrayList<String>();
		String query = resourcebundle.getString("obtenerCvesRevsRedalyc");
		resulSet = oraclesql.consultar(query, oracleConnect);
		try {
			if (resulSet != null) {
				while (resulSet.next()) {
					cvesRevsL.add(resulSet.getString("claveRev"));
				}
			} else {
				System.out.println("no encontró resultados");
				System.exit(0);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return cvesRevsL;
	}

	/**
	 * Obtiene los datos principales de revistas, que se encuentran en la tabla
	 * TBLENTREV
	 * 
	 * @param cveRev
	 */
	public void obtDatosGrales(String cveRev) {
		query = (resourcebundle.getString("obtDatosGrales") + cveRev + " ;");
		try {
			resulSet = postgresql.consultar(query, connect);
			if (resulSet != null) {
				while (resulSet.next()) {
					fieldsRev.get(0).setContenidoField(resulSet.getString("claveRev"));
					fieldsRev.get(2).setContenidoField(resulSet.getString("edoRev"));
					fieldsRev.get(1).setContenidoField(resulSet.getString("nameRev"));
					fieldsRev.get(23).setContenidoField(resulSet.getString("cveInst"));
					fieldsRev.get(3).setContenidoField(resulSet.getString("nameIns"));
					fieldsRev.get(24).setContenidoField(resulSet.getString("cveIdioma"));
					fieldsRev.get(22).setContenidoField(resulSet.getString("nameIdioma"));
					fieldsRev.get(25).setContenidoField(resulSet.getString("cvePais"));
					fieldsRev.get(4).setContenidoField(resulSet.getString("namePais"));
					fieldsRev.get(12).setContenidoField(resulSet.getString("imgRev"));
					fieldsRev.get(13).setContenidoField(resulSet.getString("imgIns"));
					fieldsRev.get(30).setContenidoField(resulSet.getString("revcsg"));
					fieldsRev.get(31).setContenidoField(resulSet.getString("cverevred"));
					//fieldsRev.get(33).setContenidoField(resulSet.getString("cvePais"));
				}
			}
		} catch (Exception ex) {

		}
		
		//query para consultar el id de aura.revista, campo=(cverevameli || cverevredalyc)=cveRev
		this.query = (this.resourcebundle.getString("obtClaveRevistaAura") + "cverevameli" + "\"=" + cveRev + ";");
		try {
			this.resulSet = this.postgresql.consultar(this.query, this.connect);
			if (this.resulSet != null)
				while (resulSet.next())
					this.fieldsRev.get(33).setContenidoField(Integer.toString(resulSet.getInt("cveRevista")));
		}catch(Exception e) {
			
		}
	}

	/**
	 * Obtiene los datos principales de revistas, que se encuentran en la tabla
	 * TBLENTREV
	 * 
	 * @param cveRev
	 */
	public void obtDatosGralesRedalyc(String cveRev) {
		query = (resourcebundle.getString("obtDatosGralesRedalyc") + cveRev);
		try {
			resulSet = oraclesql.consultar(query, oracleConnect);
			if (resulSet != null) {
				while (resulSet.next()) {
					fieldsRev.get(0).setContenidoField(resulSet.getString("claveRev"));
					fieldsRev.get(2).setContenidoField(resulSet.getString("edoRev"));
					fieldsRev.get(1).setContenidoField(resulSet.getString("nameRev"));
//					System.out.println("nameRev:" + resulSet.getString("nameRev"));
					fieldsRev.get(23).setContenidoField(resulSet.getString("cveInst"));
					fieldsRev.get(3).setContenidoField(resulSet.getString("nameIns"));
					fieldsRev.get(24).setContenidoField(resulSet.getString("cveIdioma"));
					fieldsRev.get(22).setContenidoField(resulSet.getString("nameIdioma"));
					fieldsRev.get(25).setContenidoField(resulSet.getString("cvePais"));
					fieldsRev.get(4).setContenidoField(resulSet.getString("namePais"));
					fieldsRev.get(12).setContenidoField(resulSet.getString("imgRev"));
					fieldsRev.get(13).setContenidoField(resulSet.getString("imgIns"));
					fieldsRev.get(30).setContenidoField(resulSet.getString("revcsg"));
					fieldsRev.get(31).setContenidoField(resulSet.getString("cverevred"));
					//fieldsRev.get(33).setContenidoField("cumbion loko");
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	/**
	 * Obtiene la indización de la revista con el siguiente formato:
	 * <ul>
	 * <li>indizacion1'<<<'indizacion'<<<'indizacion3'<<<'</li>
	 * </ul>
	 * 
	 * @param cveRev
	 *            String que indica la clave de revista por la cual se realiza la
	 *            busqueda
	 */
	public void obtIndizacion(String cveRev) {
		String indizacion = "";
		query = (resourcebundle.getString("obtIndizacionPart1") + cveRev
				+ resourcebundle.getString("obtIndizacionPart2"));
		try {
			resulSet = postgresql.consultar(query, connect);
			if (resulSet != null) {
				while (resulSet.next()) {
					indizacion += resulSet.getString("indizacion") + "<<<";
				}
				fieldsRev.get(19).setContenidoField(indizacion);
			}
		} catch (Exception ex) {

		}
	}
	
	/**
	 * Obtiene la indización de la revista con el siguiente formato:
	 * <ul>
	 * <li>indizacion1'<<<'indizacion'<<<'indizacion3'<<<'</li>
	 * </ul>
	 * 
	 * @param cveRev
	 *            String que indica la clave de revista por la cual se realiza la
	 *            busqueda
	 */
	public void obtIndizacionRedalyc(String cveRev) {
		String indizacion = "";
//		System.out.println("obtIndizacionRedalyc:::" + resourcebundle.getString("obtIndizacionPart1Redalyc") + cveRev
//				+ resourcebundle.getString("obtIndizacionPart2Redalyc"));
		query = (resourcebundle.getString("obtIndizacionPart1Redalyc")+ " " + cveRev + " "
				+ resourcebundle.getString("obtIndizacionPart2Redalyc"));
		try {
			resulSet = oraclesql.consultar(query, oracleConnect);
			if (resulSet != null) {
				while (resulSet.next()) {
					indizacion += resulSet.getString("indizacion") + "<<<";
				}
//				System.out.println("indizacion::" + indizacion);
				fieldsRev.get(19).setContenidoField(indizacion);
			}
		} catch (Exception ex) {

		}
	}

	/**
	 * Obtiene el comite de la revista con el siguiente formato:
	 * <ul>
	 * <li>Jorge Arroyo'<<'Asistente editorial'<<<'Violeta Vázquez Rojas
	 * Maldonado'<<'Editor'<<<'</li>
	 * </ul>
	 * 
	 * @param cveRev
	 *            String que indica la clave de revista por la cual se realizará la
	 *            busqueda
	 */
	public void obtComite(String cveRev) {
		String comite = "";
		query = "SELECT ( cies.NOMBRE || ' ' || cies.APELLIDOS || '<<' || cargo.NOMENTCAR ) AS nombre FROM tblentrev rev, tblcomcie cies, tblentcar cargo WHERE rev.CVEENTREV =" + cveRev +
				" AND cies.CVEENTREV = rev.CVEENTREV " + 
				"AND cargo.CVEENTCAR = cies.CARGO " + 
				"order by cies.cargo;";
		try {
			resulSet = postgresql.consultar(query, connect);
			if (resulSet != null) {
				while (resulSet.next()) {
					comite += resulSet.getString("nombre") + "<<<";
				}
				comite = comite.replaceAll("\\s+", " ");
				FicheroLog.logger.info("\n--------------Comite------------------" + comite);
				fieldsRev.get(20).setContenidoField(comite);
			}
		} catch (Exception ex) {

		}
	}

	/**
	 * Obtiene el comite de la revista con el siguiente formato:
	 * <ul>
	 * <li>Jorge Arroyo'<<'Asistente editorial'<<<'Violeta Vázquez Rojas
	 * Maldonado'<<'Editor'<<<'</li>
	 * </ul>
	 * 
	 * @param cveRev
	 *            String que indica la clave de revista por la cual se realizará la
	 *            busqueda
	 */
	public void obtComiteRedalyc(String cveRev) {
		String comite = "";
//		System.out.println("SELECT ( cies.NOMBRE || ' ' || cies.APELLIDOS || '<<' || cargo.NOMENTCAR ) AS nombre FROM tblentrev rev, tblcomcie cies, tblentcar cargo, tblrevcan can " + 
//				"WHERE rev.CVEENTREV =" + cveRev + " AND cies.cverevcan = can.cverevcan and can.cveentrev=rev.CVEENTREV AND cargo.CVEENTCAR = cies.CARGO order by cies.cargo");
		query = "SELECT ( cies.NOMBRE || ' ' || cies.APELLIDOS || '<<' || cargo.NOMENTCAR ) AS nombre FROM tblentrev rev, tblcomcie cies, tblentcar cargo, tblrevcan can " + 
				"WHERE rev.CVEENTREV =" + cveRev + " AND cies.cverevcan = can.cverevcan and can.cveentrev=rev.CVEENTREV AND cargo.CVEENTCAR = cies.CARGO order by cies.cargo";
		try {
			resulSet = oraclesql.consultar(query, oracleConnect);
			if (resulSet != null) {
				while (resulSet.next()) {
					comite += resulSet.getString("nombre") + "<<<";
				}
				comite = comite.replaceAll("\\s+", " ");
				FicheroLog.logger.info("\n--------------Comite------------------" + comite);
//				System.out.println("comite::" + comite);
				fieldsRev.get(20).setContenidoField(comite);
			}
		} catch (Exception ex) {

		}
	}

	/**
	 * Obtiene los numeros de la revista con el siguiente formato:
	 * <ul>
	 * <li>
	 * '<<<'2017'<<'4-1-1-54563'<<'4-2-1-54564'<<<'2016'<<'3-1-1-55002'<<'3-2-1-55003'<<<'
	 * </li>
	 * </ul>
	 * 
	 * @param cveRev
	 *            String que indica la clave de revista por la cual se realizará la
	 *            busqueda
	 */
	public void obtNumeros(String cveRev) {
		query = (resourcebundle.getString("obtNumerosPart1") + cveRev + resourcebundle.getString("obtNumerosPart2"));
		String numeros = "";
		try {
			resulSet = postgresql.consultar(query, connect);
			if (resulSet != null) {
				while (resulSet.next()) {
					numeros += "<<<" + resulSet.getString("anoedcnum") + "<<" + resulSet.getString("orden");
				}
				numeros = numeros.replaceAll("(-<<<+|<<<-+)", "<<<").replaceAll("((-+<<-?)+)", "<<").replaceAll("\\s+",
						"");
				fieldsRev.get(26).setContenidoField(numeros);
			}
		} catch (Exception ex) {

		}
	}
	
	/**
	 * Obtiene los numeros de la revista con el siguiente formato:
	 * <ul>
	 * <li>
	 * '<<<'2017'<<'4-1-1-54563'<<'4-2-1-54564'<<<'2016'<<'3-1-1-55002'<<'3-2-1-55003'<<<'
	 * </li>
	 * </ul>
	 * 
	 * @param cveRev
	 *            String que indica la clave de revista por la cual se realizará la
	 *            busqueda
	 */
	public void obtNumerosRedalyc(String cveRev) {
//		System.out.println("obtNumerosRedalyc:::" + resourcebundle.getString("obtNumerosPart1Redalyc") + " " + cveRev + " " + resourcebundle.getString("obtNumerosPart2Redalyc"));
		query = (resourcebundle.getString("obtNumerosPart1Redalyc") + " " + cveRev + " " + resourcebundle.getString("obtNumerosPart2Redalyc"));
		String numeros = "";
		try {
			resulSet = oraclesql.consultar(query, oracleConnect);
			if (resulSet != null) {
				while (resulSet.next()) {
					numeros += "<<<" + resulSet.getString("anoedcnum") + "<<" + resulSet.getString("orden");
				}
				numeros = numeros.replaceAll("(-<<<+|<<<-+)", "<<<").replaceAll("((-+<<-?)+)", "<<").replaceAll("\\s+","");
//				System.out.println("numeros:::" + numeros);
				fieldsRev.get(26).setContenidoField(numeros);
			}
		} catch (Exception ex) {

		}
	}

	/**
	 * Obtiene las areas de la revistas, primaria, secundaria y terciaria.
	 * 
	 * @param cveRev
	 *            String que indica la clave de revista por la cual se realizará la
	 *            busqueda
	 */
	public void obtAreas(String cveRev) {
		query = (resourcebundle.getString("obtArea1") + cveRev);
		try {
			resulSet = postgresql.consultar(query, connect);
			if (resulSet != null) {
				while (resulSet.next()) {
					fieldsRev.get(6).setContenidoField(resulSet.getString("CVEAREPRIN"));
					fieldsRev.get(5).setContenidoField(resulSet.getString("NOMENTARE"));
				}
			}
		} catch (Exception ex) {
		}
		query = (resourcebundle.getString("obtArea2") + cveRev);
		try {
			resulSet = postgresql.consultar(query, connect);
			if (resulSet != null) {
				while (resulSet.next()) {
					fieldsRev.get(8).setContenidoField(resulSet.getString("CVEARESEC"));
					fieldsRev.get(7).setContenidoField(resulSet.getString("NOMENTARE"));
				}
			}
		} catch (Exception ex) {
		}
		query = (resourcebundle.getString("obtArea3") + cveRev);
		try {
			resulSet = postgresql.consultar(query, connect);
			if (resulSet != null) {
				while (resulSet.next()) {
					fieldsRev.get(10).setContenidoField(resulSet.getString("CVEARETER"));
					fieldsRev.get(9).setContenidoField(resulSet.getString("NOMENTARE"));
				}
			}
		} catch (Exception ex) {
		}
	}
	

	/**
	 * Obtiene las areas de la revistas, primaria, secundaria y terciaria.
	 * 
	 * @param cveRev
	 *            String que indica la clave de revista por la cual se realizará la
	 *            busqueda
	 */
	public void obtAreasRedalyc(String cveRev) {
//		System.out.println("area1:::" + resourcebundle.getString("obtArea1Redalyc") + cveRev);
		query = (resourcebundle.getString("obtArea1Redalyc") + cveRev);
		try {
			resulSet = oraclesql.consultar(query, oracleConnect);
			if (resulSet != null) {
				while (resulSet.next()) {
					fieldsRev.get(6).setContenidoField(resulSet.getString("CVEAREPRIN"));
					fieldsRev.get(5).setContenidoField(resulSet.getString("NOMENTARE"));
//					System.out.println("nomentare:::" + resulSet.getString("NOMENTARE"));
				}
			}
		} catch (Exception ex) {
		}
//		System.out.println("area2:::" +resourcebundle.getString("obtArea2Redalyc") + cveRev);
		query = (resourcebundle.getString("obtArea2Redalyc") + cveRev);
		try {
			resulSet = oraclesql.consultar(query, oracleConnect);
			if (resulSet != null) {
				while (resulSet.next()) {
					fieldsRev.get(8).setContenidoField(resulSet.getString("CVEARESEC"));
					fieldsRev.get(7).setContenidoField(resulSet.getString("NOMENTARE"));
//					System.out.println("nomentare:::" + resulSet.getString("NOMENTARE"));
				}
			}
		} catch (Exception ex) {
		}
//		System.out.println("area3:::" +resourcebundle.getString("obtArea3Redalyc") + cveRev);
		query = (resourcebundle.getString("obtArea3Redalyc") + cveRev);
		try {
			resulSet = oraclesql.consultar(query, oracleConnect);
			if (resulSet != null) {
				while (resulSet.next()) {
					fieldsRev.get(10).setContenidoField(resulSet.getString("CVEARETER"));
					fieldsRev.get(9).setContenidoField(resulSet.getString("NOMENTARE"));
//					System.out.println("nomentare:::" + resulSet.getString("NOMENTARE"));
				}
			}
		} catch (Exception ex) {
		}
	}

	/**
	 * Obtiene la clave de revista nueva, si es que una anterior ah cambiado de
	 * nombre, lo anterior se indizará con el siguiente formato:
	 * <ul>
	 * <li>cverevnue'<<<'3586 -> En el caso de que existase una revista nueva</li>
	 * <li>cverevant'<<<'154 -> En el caso de que existase una revista anterior</li>
	 * </ul>
	 * 
	 * @param cveRev
	 *            String que indica la clave de revista por la cual se realizará la
	 *            busqueda
	 */
	public void valCambioRev(String cveRev) {
		String cambio = "";
		query = (resourcebundle.getString("obtCambioRevNue") + cveRev + " ;");
		try {
			resulSet = postgresql.consultar(query, connect);
			while (resulSet.next()) {
				cambio = "cverevnue<<<" + resulSet.getString("cverevnue");
				fieldsRev.get(27).setContenidoField(cambio);
			}
			query = (resourcebundle.getString("obtCambioRevAnt") + cveRev + " ;");
			resulSet = postgresql.consultar(query, connect);
			while (resulSet.next()) {
				cambio = "cverevant<<<" + resulSet.getString("cverevant");
				fieldsRev.get(27).setContenidoField(cambio);
			}
		} catch (Exception ex) {

		}

	}
	
	/**
	 * Obtiene la clave de revista nueva, si es que una anterior ah cambiado de
	 * nombre, lo anterior se indizará con el siguiente formato:
	 * <ul>
	 * <li>cverevnue'<<<'3586 -> En el caso de que existase una revista nueva</li>
	 * <li>cverevant'<<<'154 -> En el caso de que existase una revista anterior</li>
	 * </ul>
	 * 
	 * @param cveRev
	 *            String que indica la clave de revista por la cual se realizará la
	 *            busqueda
	 */
	public void valCambioRevRedalyc(String cveRev) {
		String cambio = "";
//		System.out.println(resourcebundle.getString("obtCambioRevNueRedalyc") + cveRev); 
		query = (resourcebundle.getString("obtCambioRevNueRedalyc") + cveRev);
		try {
			resulSet = oraclesql.consultar(query, oracleConnect);
			while (resulSet.next()) {
				cambio = "cverevnue<<<" + resulSet.getString("cverevnue");
				fieldsRev.get(27).setContenidoField(cambio);
//				System.out.println("cambioNue::"+ cambio);
			}
//			System.out.println(resourcebundle.getString("obtCambioRevAntRedalyc") + cveRev);
			query = (resourcebundle.getString("obtCambioRevAntRedalyc") + cveRev);
			resulSet = oraclesql.consultar(query, oracleConnect);
			while (resulSet.next()) {
				cambio = "cverevant<<<" + resulSet.getString("cverevant");
				fieldsRev.get(27).setContenidoField(cambio);
//				System.out.println("cambioAnt::"+ cambio);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	/**
	 * Obtiene los datos que forman parte del encabezado de la home de revistas
	 * 
	 * @param cveRev
	 *            String que indica la clave de revista por la cual se realizará la
	 *            busqueda
	 */
	public void obtEncabezado(String cveRev) {
		query = (resourcebundle.getString("obtEncabezado") + cveRev + " ;");
		String[] pag;
		String pagRev = "";
		String pagIns = "";

		try {
			resulSet = postgresql.consultar(query, connect);
			if (resulSet != null) {
				while (resulSet.next()) {

					fieldsRev.get(14).setContenidoField(resulSet.getString("issnppub"));
					fieldsRev.get(15).setContenidoField(resulSet.getString("issnepub"));
					fieldsRev.get(29).setContenidoField(resulSet.getString("correo"));
					fieldsRev.get(16).setContenidoField(resulSet.getString("periodi"));
					fieldsRev.get(17).setContenidoField(resulSet.getString("caractePub"));
					fieldsRev.get(18).setContenidoField(resulSet.getString("normas"));
					
					pag = resulSet.getString("pagrevins").split(",");
					
					if (pag.length == 1) {
						pagRev = pag[0];
					} else if (pag.length > 1) {
						pagRev = pag[0];
						pagIns = pag[1];
					}
					fieldsRev.get(11).setContenidoField(pagRev);
					fieldsRev.get(21).setContenidoField(pagIns);

				}
			}
		} catch (Exception ex) {

		}
	}

	/**
	 * Obtiene los datos que forman parte del encabezado de la home de revistas
	 * 
	 * @param cveRev
	 *            String que indica la clave de revista por la cual se realizará la
	 *            busqueda
	 */
	public void obtEncabezadoRedalyc(String cveRev) {
//		System.out.println(resourcebundle.getString("obtEncabezadoRedalyc") + cveRev);
		query = (resourcebundle.getString("obtEncabezadoRedalyc") + cveRev);
		String[] pag;
		String pagRev = "";
		String pagIns = "";

		try {
			resulSet = oraclesql.consultar(query, oracleConnect);
			if (resulSet != null) {
				while (resulSet.next()) {

					fieldsRev.get(14).setContenidoField(resulSet.getString("issnppub"));
					fieldsRev.get(15).setContenidoField(resulSet.getString("issnepub"));
					fieldsRev.get(29).setContenidoField(resulSet.getString("correo"));
					fieldsRev.get(16).setContenidoField(resulSet.getString("periodi"));
					fieldsRev.get(17).setContenidoField(resulSet.getString("caractePub"));
					fieldsRev.get(18).setContenidoField(resulSet.getString("normas"));
					
					pag = resulSet.getString("pagrevins").split(",");
					
					if (pag.length == 1) {
						pagRev = pag[0];
					} else if (pag.length > 1) {
						pagRev = pag[0];
						pagIns = pag[1];
					}
					fieldsRev.get(11).setContenidoField(pagRev);
					fieldsRev.get(21).setContenidoField(pagIns);
//					System.out.println("pagRev::" + pagRev + "::pagIns::" + pagIns);

				}
			}
		} catch (Exception ex) {

		}
	}

	/**
	 * Obtiene las palabras clave de revista y su respectiva frecuencia, con el
	 * siguiente formato:
	 * <ul>
	 * <li>'<<<'crecimiento'<<'6'<<<'rendimiento'<<'5'<<<'toxicidad'<<'5'<<<'cluster'<<'4'<<<'brotador'<<'4'<<<'</li>
	 * </ul>
	 * 
	 * @param cveRev
	 *            String que indica la clave de revista por la cual se realizará la
	 *            busqueda
	 */
	public void obtpalClave(String cveRev) {
		String palabras = "";
		query = (resourcebundle.getString("palClave") + cveRev);
		try {
			resulSet = postgresql.consultar(query, connect);
			if (resulSet != null) {
				while (resulSet.next()) {
					palabras += resulSet.getString("palclave");
				}
				fieldsRev.get(28).setContenidoField(palabras);
			}
		} catch (Exception ex) {
		}
	}
	
	/**
	 * Obtiene las palabras clave de revista y su respectiva frecuencia, con el
	 * siguiente formato:
	 * <ul>
	 * <li>'<<<'crecimiento'<<'6'<<<'rendimiento'<<'5'<<<'toxicidad'<<'5'<<<'cluster'<<'4'<<<'brotador'<<'4'<<<'</li>
	 * </ul>
	 * 
	 * @param cveRev
	 *            String que indica la clave de revista por la cual se realizará la
	 *            busqueda
	 */
	public void obtpalClaveRedalyc(String cveRev) {
		String palabras = "";
//		System.out.println("obtpalClaveRedalyc:::" + resourcebundle.getString("palClaveRedalyc") + cveRev);
		query = (resourcebundle.getString("palClaveRedalyc") + cveRev);
		try {
			resulSet = oraclesql.consultar(query, oracleConnect);
			if (resulSet != null) {
				while (resulSet.next()) {
					palabras += resulSet.getString("palclave");
				}
//				System.out.println("palabras::" + palabras);
				fieldsRev.get(28).setContenidoField(palabras);
			}
		} catch (Exception ex) {
		}
	}

	/**
	 * Crea el modelo que tendra el indice, los datos que se necesitan para crear
	 * cada campo que será indizado se muestran en la clase Modelo, y se construyen
	 * desde este método.
	 * 
	 * @see org.csg.Indexador.Modelo#ModeloField
	 * 
	 * @return
	 */
	public ArrayList<ModeloField> startFielsRev() {
		fieldsRev.add(0, new ModeloField("clave", "", true, -1, false));
		fieldsRev.add(1, new ModeloField("Nombre", "", true, -1, false));
		fieldsRev.add(2, new ModeloField("Status", "", true, -1, false));
		fieldsRev.add(3, new ModeloField("Institucion", "", true, -1, true));
		fieldsRev.add(4, new ModeloField("pais", "", true, -1, true));
		fieldsRev.add(5, new ModeloField("area1", "", true, -1, true));
		fieldsRev.add(6, new ModeloField("claveDelArea1", "", true, -1, false));
		fieldsRev.add(7, new ModeloField("area2", "", true, -1, true));
		fieldsRev.add(8, new ModeloField("claveDelArea2", "", true, -1, false));
		fieldsRev.add(9, new ModeloField("area3", "", true, -1, true));
		fieldsRev.add(10, new ModeloField("claveDelArea3", "", true, -1, false));
		fieldsRev.add(11, new ModeloField("sitioWeb", "", true, -1, false));
		fieldsRev.add(12, new ModeloField("imagenLogotipo", "", true, -1, false));
		fieldsRev.add(13, new ModeloField("imagenLogotipoInstitucion", "", true, -1, false));
		fieldsRev.add(14, new ModeloField("issn", "", true, -1, false));
		fieldsRev.add(15, new ModeloField("issnElectronico", "", true, -1, false));
		fieldsRev.add(16, new ModeloField("perioricidad", "", true, -1, false));
		fieldsRev.add(17, new ModeloField("caracteristicasDeLaPublicacion", "", true, -1, false));
		fieldsRev.add(18, new ModeloField("normasParaAutores", "", true, -1, false));
		fieldsRev.add(19, new ModeloField("indizaciones", "", true, -1, false));
		fieldsRev.add(20, new ModeloField("directorio", "", true, -1, false));
		fieldsRev.add(21, new ModeloField("sitioInstitucion", "", true, -1, false));
		fieldsRev.add(22, new ModeloField("idioma", "", true, -1, true));
		fieldsRev.add(23, new ModeloField("claveInstitucion", "", true, -1, false));
		fieldsRev.add(24, new ModeloField("claveIdioma", "", true, -1, false));
		fieldsRev.add(25, new ModeloField("clavePais", "", true, -1, true));
		fieldsRev.add(26, new ModeloField("numeros", "", true, -1, false));
		fieldsRev.add(27, new ModeloField("cambioRevista", "", true, -1, false));
		fieldsRev.add(28, new ModeloField("palabrasClave", "", true, -1, false));
		fieldsRev.add(29, new ModeloField("correo", "", true, -1, false));
		fieldsRev.add(30, new ModeloField("revcsg", "", true, -1, false));
		fieldsRev.add(31, new ModeloField("cverevred", "", true, -1, false));
		fieldsRev.add(32, new ModeloField("origen", "", true, -1, true));//tarea1
		fieldsRev.add(33, new ModeloField("cveRevistaAura", "", true, -1));//tarea2
		
		return fieldsRev;
	}

	/**
	 * Metodo que inicia el proceso de indexación de los datos obtenidos por
	 * Revista, dicho creación de documento de realiza la siguiente clase linkeada.
	 * 
	 * @see org.csg.Indexador#DocumentoIndex
	 */
	public void indexarDoc() {
		try {
			writoo.addDocument(docIndex.indizarDocumento(fieldsRev));
			writoo.commit();
		} catch (Exception ex) {
			System.err.println("ERROR! Metódo indexar" + ": " + ex.getMessage());
			ex.printStackTrace();
			System.exit(0);
		}
	}

	/**
	 * Crear un CharArraySet con las estopwords que serán utilizadas por el
	 * analizador
	 * 
	 * @return Una lista de tipo CharArraySet que contiene las stopwords
	 */
	public CharArraySet stopwords() {
		CharArraySet stopSet = null;
		try {
			stopSet = CharArraySet.copy(StandardAnalyzer.STOP_WORDS_SET);
			BufferedReader bufferedReader = new BufferedReader(new FileReader(bundlePaths.getString("stopwords")));
			String palabraStopWord;

			while ((palabraStopWord = bufferedReader.readLine()) != null) {
				stopSet.add(palabraStopWord);
			}
			bufferedReader.close();
		} catch (Exception ex) {

		}
		return stopSet;
	}
}
