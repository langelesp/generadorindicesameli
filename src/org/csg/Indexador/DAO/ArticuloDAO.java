package org.csg.Indexador.DAO;

import java.io.BufferedReader;
import java.io.FileReader;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.io.FilenameUtils;
import org.apache.lucene.analysis.CharArraySet;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import org.csg.ConectionPostgreSQL.PostgreSQLJDBC;
import org.csg.Indexador.Modelo.ModeloField;
import org.csg.Indexador.DocumentoIndex;
import org.csg.Indexador.FicheroLog;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.traversal.DocumentTraversal;
import org.w3c.dom.traversal.NodeFilter;
import org.w3c.dom.traversal.NodeIterator;

import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;

/**
 * Esta clase contiene los metodos para obtener los datos de articulos tanto de
 * base de datos, como de XMLs. <br>
 * El proceso que se realiza antes de indizar estos datos, es validar que el
 * numero al que pertenecen se encuentre terminado, y que la revista a la que
 * pertenecen se encuentre disponible para CSG <br>
 * <br>
 * Inicializa las variables de conexion a base de datos, analizador y escritor
 * del indice.
 * 
 * ESTA CLASE NO SE USA DURANTE EL PROCESO DE EJECUCIÓN DE INDICES...
 * ESTA CLASE FUE LA PRIMERA VERSIÓN DE PARSEO DE LOS XML UTILIZANDO  JDOM. 
 * POSTERIORMENTE SE REEMPLAZO POR DOM4J
 * 
 * @author USUARIO
 *
 */
public class ArticuloDAO extends Thread {
	ArrayList<ModeloField> fieldsRev = new ArrayList<>();
	DocumentBuilderFactory factory;
	DocumentBuilder loader;
	Document document;
	DocumentTraversal trav;
	ArrayList<String> datosXML = new ArrayList<>();
	ArrayList<String> datosArtDB = new ArrayList<>();
	ArrayList<String> filesXML = new ArrayList<>();
	ResourceBundle resourceBundle = null;
	NodeIterator it;
	StandardAnalyzer analyzer;
	Directory index;
	IndexWriterConfig config;
	IndexWriter writoo;
	DocumentoIndex indexDoc = new DocumentoIndex();
	PostgreSQLJDBC consultar = new PostgreSQLJDBC();
	FicheroLog ficherolog = new FicheroLog();
	String claveID = "";
	private Connection connect = null;
	String log = "";
	RadioButton radioArt = new RadioButton();
	RadioButton radioRev = new RadioButton();
	Button btnValidar = new Button();
	TextArea txtarea = new TextArea();
	TextArea txtLog = new TextArea();
	// ProgressBar pb = new ProgressBar();
	// TextField fieldprogress = new TextField();
	String progreso = "";
	String rutaIndice = "";

	/**
	 * Constructor que inicializa las variables para leer y escribir los xmls, as-
	 * como el -ndice.
	 * 
	 * @param pathIndex
	 *            String que indica la ruta en donde se guarda el indice creado
	 * @param filesXML
	 *            ArrayList de tipo String, que almacena las rutas de los xmls que
	 *            para leer
	 */
	public ArticuloDAO(String pathIndex, ArrayList<String> filesXML, Connection connect) {
		try {
			this.rutaIndice = pathIndex;
			this.filesXML = filesXML;
			this.connect = connect;
			factory = DocumentBuilderFactory.newInstance();
			loader = factory.newDocumentBuilder();
			ficherolog.crearLog(pathIndex);

			try {
				analyzer = new StandardAnalyzer(stopwords());
				index = FSDirectory.open(Paths.get(pathIndex));
				config = new IndexWriterConfig(analyzer);
				writoo = new IndexWriter(index, config);
				resourceBundle = ResourceBundle.getBundle("querys");
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public ArticuloDAO(String pathIndex, ArrayList<String> filesXML, Connection connect,
			ArrayList<Object> componentes) {
		this.rutaIndice = pathIndex;
		this.filesXML = filesXML;
		this.radioArt = (RadioButton) componentes.get(0);
		this.radioRev = (RadioButton) componentes.get(1);
		this.btnValidar = (Button) componentes.get(2);
		this.txtarea = (TextArea) componentes.get(5);
		// this.pb = (ProgressBar) componentes.get(6);
		// this.fieldprogress = (TextField) componentes.get(8);
		this.txtLog = (TextArea) componentes.get(9);
		ficherolog.crearLog(pathIndex);
		try {
			this.connect = connect;
			factory = DocumentBuilderFactory.newInstance();
			loader = factory.newDocumentBuilder();

			try {
				analyzer = new StandardAnalyzer(stopwords());
				index = FSDirectory.open(Paths.get(pathIndex));
				config = new IndexWriterConfig(analyzer);
				writoo = new IndexWriter(index, config);
				resourceBundle = ResourceBundle.getBundle("querys");
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	/**
	 * Metodo que inicia con el proceso de lectura de los XMLs. Atraves de una
	 * iteraci-n se obtiene la ruta de ubicaci-n del XML, posteriormente se obtiene
	 * la clave de articulo de este, y se valida el estado del n-mero al que
	 * pertenece, para determinar si es un n-mero terminado.
	 */
	public void run() {
		try {
			log = ">>>>>>  Inicia la lectura de los XMLs <<<<<<";
			txtarea.setText("El progreso para la generacion de este indice sera mostrado en  la consola! "
					+ "\n Ejecutese el Indexador.jar en lineas de comandos para mostrar dicho progreso...");
			FicheroLog.logger.info(log);

			int total = filesXML.size();
			double progress = 0;
			double aux = 0;
			String rutaLog = FicheroLog.fechaS;
			Boolean parseStatus = true;
			
			for (int i = 0; i < filesXML.size(); i++) {
				parseStatus = true;
				claveID = obtenerClave(filesXML.get(i));
				if (!claveID.equals("")) {
					if (consultNumTerminado(claveID)) {

						log = "\n>>>>>> Parseando " + (i + 1) + " de " + (filesXML.size());
						// txtarea.setText(txtarea.getText() + (log + "\n"));
						FicheroLog.logger.info(log);

						try {
							document = loader.parse(filesXML.get(i));
						} catch (Exception ex) {
							parseStatus = false;

							log = "\n -> El articulo -> " + claveID + " no se indizar-, !ERROR TYPE XML!";
							// txtarea.setText(txtarea.getText() + (log + "\n"));
							FicheroLog.logger.warning(log);
						}
						if (parseStatus) {
							trav = (DocumentTraversal) document;
							it = trav.createNodeIterator(document.getDocumentElement(), NodeFilter.SHOW_ELEMENT, null,
									true);
							startFielsRev();
							getCamposXML();
							datosXML.clear();
							fieldsRev.clear();
						}

					} else {
						log = "\n>>>>>> Parseando " + (i + 1) + " de " + (filesXML.size());
						// txtarea.setText(txtarea.getText() + (log + "\n"));
						FicheroLog.logger.info(log);

						log = "\nEl n-mero al que pertenece el art-culo -> " + claveID
								+ " no se ha terminado o la revista revcsg=0 ! No se indizar-";
						// txtarea.setText(txtarea.getText() + (log + "\n"));
						FicheroLog.logger.info(log);

					}
				} else {
					log = "\n>>>>>> XML invalido ERROR TYPE! -> " + filesXML.get(i);
					// txtarea.setText(txtarea.getText() + (log + "\n"));
					FicheroLog.logger.warning(log);
				}
				aux = ((i + 1) * 1);
				progress = (aux / total);
				progreso = String.valueOf(progress * 100);
				progreso = progreso.substring(0, progreso.indexOf('.'));
				// fieldprogress.setText(progreso + "%");
				// pb.setProgress(progress);
			}
			log = "\n|************ Se indizaron: " + writoo.numDocs() + " Articulos **********|";
			// txtarea.setText(txtarea.getText() + (log + "\n"));
			FicheroLog.logger.info(log);

			txtLog.setText("Log creado -> " + " " + rutaLog);
			writoo.close();
			FicheroLog.fh.close();
			consultar.closeConnect(connect);
		} catch (Exception ex) {
			ex.printStackTrace();

			log = "\n************ ERROR de acceso al JatsRepo.. Indexaci-n CANCELADA **********";
			// txtarea.setText(txtarea.getText() + (log + "\n"));
			FicheroLog.logger.warning(log);
		}
		radioArt.setDisable(false);
		radioRev.setDisable(false);
		consultar.closeConnect(connect);
		btnValidar.setDisable(false);
	}

	/**
	 * Metodo que realiza en parseo de los XMLs, para poder leerlos. Este m-todo
	 * hace uso de las siguientes variables: </br>
	 * 
	 * 
	 * <ul>
	 * <li><b>existentes :</b> Lista de tipo <code>existAttib</code>, esta lista
	 * almacena en cada pocisi-n el nombre, false o true si el campo existe, y la
	 * posici-n en la que se encuentra dentro de la lista existentes.
	 * <li><b>datosXML :</b> Lista que guarda la informaci-n de los campos que se
	 * obtienen del xml
	 * <li><b>doi :</b> String que guarda la direcci-n <code>doi</code> del
	 * articulo, si cuenta con ella.
	 * <li><b>autores :</b> String que almacena a los autores del articulo separados
	 * por '<<<' p. ej. "autor1 apellido<<<autor2 apellido"
	 * <li><b>keywords :</b> String que guarda las palabras clave del articulo,
	 * separadas por '<<<' p. ej. "keyword1<<<keyword2<<<keyword3"
	 * <li><b>resumen :</b> String que almacena el resumen del articulo y su
	 * respectivo idioma (puede haber articulos con resumen en otros idiomas)
	 * separados por '<<<' p. ej. "es<<<resumen<<<en<<<resumen<<<pt<<<resumen"
	 * <li><b>idArt :</b> String que almacena la clave del articulo
	 * <li><b>title : </b> String que almacena el titulo del articulo y su repectivo
	 * idioma (existen titulos con m-s de un idioma) separados pod '<<<' p. ej.
	 * "es<<<titulo<<<en<<<titulo<<<pt<<<titulo"
	 * <li><b>detPos :</b> Entero, este contador tambien se a-ade a la lista
	 * <code>exitentes</code> para indicar en que posici-n de la lista
	 * <code>datosXML</code> se encuentra para obtener el campo
	 * </ul>
	 */
	public void getCamposXML() {
		String doi = "";
		String autores = "";
		String keywords = "";
		String resumen = "";
		String idArt = "";
		String title = "";
		String surnames = "";
		Boolean indizar = false;

		for (Node node = it.nextNode(); node != null; node = it.nextNode()) {
			String name = node.getNodeName();

			if (name.equals("journal-id") && node.getParentNode().getNodeName().equals("journal-meta")) {
				Element e = (Element) node;
				String type = e.getAttribute("journal-id-type");

				if (node.getTextContent().trim().matches("[0-9]*")) {
					if(revistaEnBase(node.getTextContent().trim())) {
						fieldsRev.get(0).setExiste(true);
						fieldsRev.get(0).setContenidoField(node.getTextContent().trim());
						indizar = true;
					}else {
						FicheroLog.logger.info("\n************ ERROR clave de Revista no existente en DB... Articulo No se Indizara **********");
						break;
					}
				} else {
					FicheroLog.logger.info("\n************ ERROR clave de Revista Invalida en XML... Articulo No se Indizara **********");
					break;
				}

			}
			if (indizar) {
				if (name.equals("article-id")) {
					Element e = (Element) node;
					String use = e.getAttribute("pub-id-type");
					if (use.equals("art-access-id")) {
						idArt = node.getTextContent().trim();
						idArt = idArt.replaceAll("\n", " ").replaceAll("\\s+", " ");
					} else if (use.equals("doi")) {
						doi = node.getTextContent().trim();
						doi = doi.replaceAll("\n", " ").replaceAll("\\s+", " ");
						fieldsRev.get(13).setExiste(true);
						fieldsRev.get(13).setContenidoField(doi);
					}
				}

				else if (name.equals("journal-title")
						&& node.getParentNode().getNodeName().equals("journal-title-group")) {

					fieldsRev.get(1).setExiste(true);
					fieldsRev.get(1).setContenidoField(
							node.getTextContent().trim().replaceAll("\n", " ").replaceAll("\\s+", " "));

				} else if (name.equals("publisher-name")
						&& node.getParentNode().getParentNode().getNodeName().equals("journal-meta")) {
					fieldsRev.get(2).setExiste(true);
					fieldsRev.get(2).setContenidoField(
							node.getTextContent().trim().replaceAll("\n", " ").replaceAll("\\s+", " "));

				} else if (name.equals("country") && node.getParentNode().getNodeName().equals("publisher-loc")) {

					fieldsRev.get(3).setExiste(true);
					fieldsRev.get(3).setContenidoField(
							node.getTextContent().trim().replaceAll("\n", " ").replaceAll("\\s+", " "));

				} else if (name.equals("article-title")
						&& node.getParentNode().getParentNode().getNodeName().equals("article-meta")) {
					Element e = (Element) node;
					String language = e.getAttribute("xml:lang");
					title += language + "<<<";
					String aux = "";
					NodeList childs = node.getChildNodes();
					for (int i = 0; i < childs.getLength(); i++) {
						Node child = childs.item(i);
						aux += child.getTextContent().trim() + " ";
					}
					title += aux.trim().replaceAll("\n", " ").replaceAll("\\s+", " ");

				} else if (name.equals("trans-title")
						&& node.getParentNode().getParentNode().getNodeName().equals("title-group")) {

					Element e = (Element) node;
					String language = e.getAttribute("xml:lang");
					title += "<<<" + language + "<<<";
					String aux = "";
					NodeList childs = node.getChildNodes();
					for (int i = 0; i < childs.getLength(); i++) {
						Node child = childs.item(i);
						aux += child.getTextContent().trim() + " ";
					}
					title += aux.trim().replaceAll("\n", " ").replaceAll("\\s+", " ");

				} else if (name.equals("surname")
						&& node.getParentNode().getParentNode().getNodeName().equals("contrib")) {
					surnames = "";
					surnames += node.getTextContent().trim().replaceAll("\n", " ").replaceAll("\\s+", " ") + " ";

				} else if (name.equals("given-names")
						&& node.getParentNode().getParentNode().getNodeName().equals("contrib")) {
					autores += node.getTextContent().trim().replaceAll("\n", " ").replaceAll("\\s+", " ") + " "
							+ surnames.trim() + "<<<";

				} else if (name.equals("year") && node.getParentNode().getNodeName().equals("pub-date")) {
					fieldsRev.get(6).setExiste(true);
					fieldsRev.get(6).setContenidoField(
							node.getTextContent().trim().replaceAll("\n", " ").replaceAll("\\s+", " "));

				} else if (name.equals("volume") && node.getParentNode().getNodeName().equals("article-meta")) {
					fieldsRev.get(7).setExiste(true);
					fieldsRev.get(7).setContenidoField(
							node.getTextContent().trim().replaceAll("\n", " ").replaceAll("\\s+", " "));

				} else if (name.equals("issue") && node.getParentNode().getNodeName().equals("article-meta")) {
					fieldsRev.get(8).setExiste(true);
					fieldsRev.get(8).setContenidoField(
							node.getTextContent().trim().replaceAll("\n", " ").replaceAll("\\s+", " "));

				} else if (name.equals("abstract") && node.getParentNode().getNodeName().equals("article-meta")) {
					String resaux = "";
					Element e = (Element) node;
					String language = e.getAttribute("xml:lang");
					resumen += language + ":";

					NodeList childs = node.getChildNodes();
					for (int i = 0; i < childs.getLength(); i++) {
						Node child = childs.item(i);
						if (!child.getNodeName().equals("title")) {
							resaux = child.getTextContent().trim().replaceAll("\n", " ").replaceAll("\\s+", " ");
							if (!resaux.equals(" ") && !resaux.equals("")) {
								resumen += resaux.trim() + " ";
							}
						}
					}

				} else if (name.equals("trans-abstract") && node.getParentNode().getNodeName().equals("article-meta")) {
					String resaux = "";
					Element e = (Element) node;
					String language = e.getAttribute("xml:lang");
					resumen += "<<<" + language + ":";

					NodeList childs = node.getChildNodes();
					for (int i = 0; i < childs.getLength(); i++) {
						Node child = childs.item(i);
						if (!child.getNodeName().equals("title")) {
							resaux = child.getTextContent().replaceAll("\n", " ").replaceAll("\\s+", " ");
							if (!resaux.equals(" ")) {
								resumen += resaux.trim() + " ";
							}
						}
					}
				} else if (name.equals("kwd-group") && node.getParentNode().getNodeName().equals("article-meta")) {
					Element e = (Element) node;
					String language = e.getAttribute("xml:lang");
					keywords += "<<<" + language + ":";

				} else if (name.equals("kwd") && node.getParentNode().getNodeName().equals("kwd-group")) {
					keywords += node.getTextContent().trim().replaceAll("\n", " ").replaceAll("\\s+", " ") + "<<";

				} else if (name.equals("body")) {
					String contenido = "";
					NodeList childs = node.getChildNodes();
					for (int i = 0; i < childs.getLength(); i++) {
						Node child = childs.item(i);
						contenido += " " + child.getTextContent().trim() + " ";
					}
					contenido = contenido.replaceAll("\n", " ").replaceAll("\\s+", " ");

					// Para acortar el contenido! **No recomendado
					// if (contenido.length() > 500) {
					// contenido = contenido.substring(0, 450);
					// }

					fieldsRev.get(10).setExiste(true);
					fieldsRev.get(10).setContenidoField(contenido);
				} else if (name.equals("back")) {
					break;
				}
			}
		}
		if (indizar) {
			fieldsRev.get(5).setExiste(true);
			fieldsRev.get(5).setContenidoField(title);
			fieldsRev.get(4).setExiste(true);
			fieldsRev.get(4).setContenidoField(claveID);

			if (!resumen.equals("") && !resumen.equals(" ") && resumen != null) {
				fieldsRev.get(9).setExiste(true);
				fieldsRev.get(9).setContenidoField(resumen.replaceAll("\n", "").replaceAll("\\s+", " ")
						.replaceAll(": ", ":").replaceAll(" :", ":"));
			} else {
				fieldsRev.get(9).setNameField("resumen");
				fieldsRev.get(9).setExiste(false);
			}
			fieldsRev.get(11).setExiste(true);
			fieldsRev.get(11).setContenidoField(autores);

			if (!keywords.equals("") && !keywords.equals(" ") && keywords != null) {
				fieldsRev.get(12).setExiste(true);
				keywords = keywords.replaceAll("<<<<<", "<<<");
				keywords = keywords.substring(3, keywords.length() - 2);
				fieldsRev.get(12).setContenidoField(keywords);
			} else {
				fieldsRev.get(12).setNameField("palClave");
				fieldsRev.get(12).setExiste(false);
			}
			getCamposDB();
			indexar();
			log = " ---> XML " + claveID + " con formato valido Indizado <<<<<<";
			// txtarea.setText(txtarea.getText() + (log + "\n"));
			FicheroLog.logger.info(log);
			// txtarea.setText(log);
		} else {
			log = " ---> XML " + claveID + " con formato NO valido NO se ha indizado <<<<<<";
			// txtarea.setText(txtarea.getText() + (log + "\n"));
			FicheroLog.logger.info(log);
		}

	}

	/**
	 * Obtiene los datos de articulos que vienen desde la base de datos
	 */
	public void getCamposDB() {
		ResultSet resultados = null;
		String query = resourceBundle.getString("datosArtdb") + fieldsRev.get(0).getContenidoField() + ";";
		try {
			resultados = consultar.consultar(query, connect);
			if (resultados != null) {
				while (resultados.next()) {
					fieldsRev.get(14).setExiste(true);
					fieldsRev.get(14).setContenidoField(resultados.getString("edorev"));
					fieldsRev.get(15).setExiste(true);
					fieldsRev.get(15).setContenidoField(resultados.getString("cvearea"));
					fieldsRev.get(16).setExiste(true);
					fieldsRev.get(16).setContenidoField(resultados.getString("namearea"));
					fieldsRev.get(17).setExiste(true);
					fieldsRev.get(17).setContenidoField(resultados.getString("cveinst"));
					fieldsRev.get(18).setExiste(true);
					fieldsRev.get(18).setContenidoField(resultados.getString("cvepais"));
					fieldsRev.get(19).setExiste(true);
					fieldsRev.get(19).setContenidoField(resultados.getString("cveidioma"));
					fieldsRev.get(20).setExiste(true);
					fieldsRev.get(20).setContenidoField(resultados.getString("nameidioma"));

				}

			}
			String query2 = resourceBundle.getString("numTerminado") + claveID + "';";
			ResultSet resultados2 = consultar.consultar(query2, connect);
			String imgPortada = "";
			while (resultados2.next()) {
				fieldsRev.get(21).setExiste(true);
				fieldsRev.get(21).setContenidoField(resultados2.getString("cverevnum"));
				try {
					imgPortada = resultados2.getString("nomimgdes").toString();
				} catch (Exception ex) {
					imgPortada = "-";
				}

				if (!imgPortada.equals("-") && !imgPortada.equals(null) && !imgPortada.equals("")) {
					fieldsRev.get(22).setExiste(true);
					fieldsRev.get(22).setContenidoField(imgPortada);
				}
			}

		} catch (Exception ex) {
			// txtarea.setText(txtarea.getText() + "ERROR! Met-do getCamposDB" + ": " +
			// ex.getMessage() + " Indizaci-n cancelada");
			FicheroLog.logger.warning("ERROR! Met-do getCamposDB" + ": " + ex.getMessage() + " Indizaci-n cancelada");
			System.exit(0);
		}

	}

	/**
	 * Obtiene la clave del articulo del el nombre de el XML, para determinar de
	 * igual manera si es valido el formato o no
	 * 
	 * @param path
	 *            Ruta del XML
	 * @return Un String con la clave del articulo obtenido del el nombre del xml
	 */
	public String obtenerClave(String path) {
		String baseName = FilenameUtils.getBaseName(path);
		if (baseName.matches(".*[A-Za-z].*")) {
			return "";
		} else {
			return baseName;
		}
	}

	/**
	 * Valida que el n-mero al que pertenece el articulo este terminado, es decir,
	 * que se encuentre en estado 3 - 1
	 * 
	 * @param doc
	 * @return
	 */
	public Boolean validarNumTerminado(Document doc) {
		Boolean terminado = false;
		Document aux = doc;
		aux.getDocumentElement().normalize();

		NodeList nList = document.getElementsByTagName("article-meta");
		Node nNode = nList.item(0);
		Element elem = (Element) nNode;

		Node auxNode = elem.getElementsByTagName("article-id").item(0);
		Element auxElem = (Element) auxNode;
		claveID = auxElem.getTextContent().trim();
		terminado = consultNumTerminado(auxElem.getTextContent().trim());
		if (terminado)
			return true;
		else
			return false;
	}

	public Boolean consultNumTerminado(String claveArt) {
		Boolean terminado = false;
		int edojats = 0;
		int revcsg = 0;
		String query = resourceBundle.getString("numTerminado") + claveArt + "';";
		ResultSet resultado = consultar.consultar(query, connect);
		if (resultado != null) {
			try {
				while (resultado.next()) {
					edojats = resultado.getInt("edojats");
					revcsg = resultado.getInt("revcsg");
				}
				if ((edojats == 3 || edojats == 1) && revcsg == 1) {
					terminado = true;
				} else {
					terminado = false;
				}
			} catch (Exception ex) {
				// txtarea.setText(txtarea.getText() + "ERROR! Met-do consultNumTerminado" + ":
				// " + ex.getMessage() + " Indizaci-n cancelada");
				FicheroLog.logger.warning(
						"ERROR! Met-do consultNumTerminado" + ": " + ex.getMessage() + " Indizaci-n cancelada");
				System.exit(0);
			}

		}
		return terminado;
	}

	public Boolean revistaEnBase(String claveRev) {
		Boolean existe = false;
		String res = "";
		String query = resourceBundle.getString("comprobarRevista") + claveRev + ";";
		ResultSet resultado = consultar.consultar(query, connect);
		if (resultado != null) {
			try {
				while (resultado.next()) {
					res = resultado.getString("cveentrev");
					if(!res.equals("")) {
						existe = true;
					}
				}
			} catch (Exception ex) {
				FicheroLog.logger.warning(
						"ERROR! Metódo revistaEnBase" + ": " + ex.getMessage() + " Comprobacion de Revista Cancelada");
				System.exit(0);
			}

		}
		return existe;
	}

	/**
	 * Indiza el documento creado dentro del -ndice
	 */
	public void indexar() {
		try {
			writoo.addDocument(indexDoc.indizarDocumento(fieldsRev));
			writoo.commit();
		} catch (Exception ex) {
			// txtarea.setText(txtarea.getText() + "ERROR! Met-do indexar" + ": " +
			// ex.getMessage() + " Indizaci-n cancelada");
			FicheroLog.logger.warning("ERROR! Met-do indexar" + ": " + ex.getMessage() + " Indizaci-n cancelada");
			System.exit(0);
		}
	}

	/**
	 * Crea el modelo del documento que sera indexado
	 * 
	 * @return Una lista con los campos creados y su contenido inicializado
	 */
	public ArrayList<ModeloField> startFielsRev() {
		fieldsRev.add(0, new ModeloField("claveRevista", "", false, 0));
		fieldsRev.add(1, new ModeloField("nombreRevista", "", false, 0));
		fieldsRev.add(2, new ModeloField("institucion", "", false, 0));
		fieldsRev.add(3, new ModeloField("pais", "", false, 0));
		fieldsRev.add(4, new ModeloField("claveArt", "", false, 0));
		fieldsRev.add(5, new ModeloField("tituloArt", "", false, 0));
		fieldsRev.add(6, new ModeloField("anio", "", false, 0));
		fieldsRev.add(7, new ModeloField("volumen", "", false, 0));
		fieldsRev.add(8, new ModeloField("numero", "", false, 0));
		fieldsRev.add(9, new ModeloField("resumen", "", false, 0));
		fieldsRev.add(10, new ModeloField("contenido", "", false, 0));
		fieldsRev.add(11, new ModeloField("autores", "", false, 0));
		fieldsRev.add(12, new ModeloField("palClave", "", false, 0));
		fieldsRev.add(13, new ModeloField("doi", "", false, 0));
		fieldsRev.add(14, new ModeloField("edoRev", "", false, 0));
		fieldsRev.add(15, new ModeloField("cveArea", "", false, 0));
		fieldsRev.add(16, new ModeloField("area", "", false, 0));
		fieldsRev.add(17, new ModeloField("cveInst", "", false, 0));
		fieldsRev.add(18, new ModeloField("cvePais", "", false, 0));
		fieldsRev.add(19, new ModeloField("cveIdioma", "", false, 0));
		fieldsRev.add(20, new ModeloField("idioma", "", false, 0));
		fieldsRev.add(21, new ModeloField("cveNumero", "", false, 0));
		fieldsRev.add(22, new ModeloField("imgPortada", "", false, 0));
		return fieldsRev;
	}

	/**
	 * Crear un CharArraySet con las estopwords que ser-n utilizadas por el
	 * analizador
	 * 
	 * @return Una lista de tipo CharArraySet que contiene las stopwords
	 */
	public CharArraySet stopwords() {
		CharArraySet stopSet = null;
		try {
			stopSet = CharArraySet.copy(StandardAnalyzer.STOP_WORDS_SET);
			BufferedReader bufferedReader = new BufferedReader(new FileReader(resourceBundle.getString("stopwords")));
			String palabraStopWord;

			while ((palabraStopWord = bufferedReader.readLine()) != null) {
				stopSet.add(palabraStopWord);
			}
			bufferedReader.close();
		} catch (Exception ex) {

		}
		return stopSet;
	}

}
