package org.csg.Indexador.DAO;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.ResourceBundle;

import org.apache.lucene.analysis.CharArraySet;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.csg.ConectionPostgreSQL.PostgreSQLJDBC;
import org.csg.Indexador.Modelo.ModeloAreasColeccion;
import org.csg.Indexador.Modelo.ModeloJsonColeccion;
import org.csg.Indexador.Modelo.ModeloJsonColeccionMapa;
import org.csg.Indexador.Modelo.ModeloNumerariasColeccion;

import java.io.FileWriter;
import java.io.IOException;

import org.json.simple.JSONObject;
import com.google.gson.Gson;
import com.sun.xml.internal.ws.api.Component;

import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Label;
import javafx.scene.control.Labeled;


public class jsonColeccionRevistas extends Thread {

	Directory directoryR;
	Directory directoryA;
	Directory directoryL;
	StandardAnalyzer analyzer;
	IndexSearcher searcher;
	ResourceBundle resourceBundle = null;
	ResourceBundle bundlePaths = null;
	ScoreDoc[] hits = null;
	ArrayList<Object> componentes = new ArrayList<>();

	PostgreSQLJDBC postgresql = new PostgreSQLJDBC();
	private Connection connect = null;
	ResultSet resulSet = null;

	ArrayList<ModeloNumerariasColeccion> numerosHome = new ArrayList<>();

	ArrayList<ModeloNumerariasColeccion> cienciasSociales = new ArrayList<>();
	ArrayList<ModeloNumerariasColeccion> cienciasNaturales = new ArrayList<>();
	ArrayList<ModeloNumerariasColeccion> multidiciplinarias = new ArrayList<>();
	ArrayList<ModeloNumerariasColeccion> arteHumanidades = new ArrayList<>();
	ArrayList<ModeloAreasColeccion> areas = new ArrayList<>();
	ArrayList<ModeloNumerariasColeccion> paises = new ArrayList<>();
	ArrayList<ModeloNumerariasColeccion> instituciones = new ArrayList<>();
	ArrayList<ModeloJsonColeccion> jsonColeccion = new ArrayList<>();

	ArrayList<ModeloJsonColeccionMapa> paisesMapa = new ArrayList<>();

	public jsonColeccionRevistas(String pathRevistas, String pathArt, String pathLibros, ArrayList<Object> componentes) {
		try {
			resourceBundle = ResourceBundle.getBundle("querys");
			bundlePaths = ResourceBundle.getBundle("paths");
			System.out.println("bundlePaths:::" + bundlePaths);
			directoryR = FSDirectory.open(Paths.get(pathRevistas));
			directoryA = FSDirectory.open(Paths.get(pathArt));
			directoryL = FSDirectory.open(Paths.get(pathLibros));
			analyzer = new StandardAnalyzer(stopwords());
			this.componentes = componentes;
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	public jsonColeccionRevistas(String pathRevistas, String pathArt, String pathLibros) {
		try {
			resourceBundle = ResourceBundle.getBundle("querys");
			bundlePaths = ResourceBundle.getBundle("paths");
			System.out.println("bundlePaths:::" + bundlePaths);
			directoryR = FSDirectory.open(Paths.get(pathRevistas));
			directoryA = FSDirectory.open(Paths.get(pathArt));
			directoryL = FSDirectory.open(Paths.get(pathLibros));
			analyzer = new StandardAnalyzer(stopwords());
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public jsonColeccionRevistas(String pathRevistas, String pathArt) {
		try {
			resourceBundle = ResourceBundle.getBundle("querys");
			bundlePaths = ResourceBundle.getBundle("paths");
			System.out.println("bundlePaths:::" + bundlePaths);
			directoryR = FSDirectory.open(Paths.get(pathRevistas));
			directoryA = FSDirectory.open(Paths.get(pathArt));
			analyzer = new StandardAnalyzer(stopwords());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// Hilo para funcionamiento en vista
	public void run() {
		((ProgressBar) componentes.get(6)).setProgress(0.3);
		((TextArea) componentes.get(5)).setText("Creando Json para Numerarias Home");
		obtenerNumerariasHome();
		((ProgressBar) componentes.get(6)).setProgress(0.6);
		((TextArea) componentes.get(5)).setText("Creando Json para Colecciones");
		crearJsonColeccionHome();
		((ProgressBar) componentes.get(6)).setProgress(0.9);
		((TextArea) componentes.get(5)).setText("Creando Json para Mapa de Colecciones");
		generarJsonMapaColeccion();
		((ProgressBar) componentes.get(6)).setProgress(1);
		
		((TextArea) componentes.get(5)).setText("Se han creado los JSON");
		((RadioButton) componentes.get(0)).setDisable(false);
		((RadioButton) componentes.get(1)).setDisable(false);
		((RadioButton) componentes.get(10)).setDisable(false);
		((RadioButton) componentes.get(11)).setDisable(false);
		((RadioButton) componentes.get(13)).setDisable(false);
		
		((Button) componentes.get(2)).setDisable(false);
		((Label) componentes.get(3)).setText("");
		
		
//		radioRev.setDisable(false);
//		btnValidar.setDisable(false);
	}

	// Generar Jsons de Numerarias en main
	public void generarJsonsNumerarias() {
		obtenerNumerariasHome();
		crearJsonColeccionHome();
	}

	public void generarJsonMapaColeccion() {
		this.connect = postgresql.conectar();
		obtenerAllPaises(2);
		crearEstructuraJsonMapa();
		System.out.println("Se genero el json para mapa de coleccion...");
		postgresql.closeConnect(connect);

	}

	public void crearJsonColeccionHome() {
		initAreasGrupo1();
		initAreasGrupo2();
		initAreasGrupo3();
		initAreasGrupo4();
		obtenerTotalAllAreas();
		obtenerAllPaises(1);
		obtenerAllInstituciones();

		areas.add(new ModeloAreasColeccion(cienciasSociales, cienciasNaturales, arteHumanidades, multidiciplinarias));

		jsonColeccion.add(new ModeloJsonColeccion(areas, paises, instituciones));
		jsonconGSON();

	}

	public void jsonconGSON() {
		try {
			Gson gson = new Gson();
			String cadenaJson = gson.toJson(jsonColeccion);
			System.out.println("jsonconGSON:::" + bundlePaths.getString("rutaDefaultJsonColecciones"));
			FileWriter fileWriter = new FileWriter(bundlePaths.getString("rutaDefaultJsonColecciones"));
			fileWriter.write(cadenaJson);
			fileWriter.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void crearEstructuraJsonMapa() {
		try {
			Gson gson = new Gson();
			String cadenaJson = gson.toJson(paisesMapa);
			System.out.println("crearEstructuraJsonMapa:::" + bundlePaths.getString("rutaDefaultJsonMapa"));
			FileWriter fileWriter = new FileWriter(bundlePaths.getString("rutaDefaultJsonMapa"));
			fileWriter.write(cadenaJson);
			fileWriter.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void obtenerNumerariasHome() {
		int totalNumeros = 0;
//		int totalArticulos = leerIndiceArticulos("claveArt:[0 TO 9]");
		int totalArticulos = leerIndiceArticulos("origen:[aA TO zZ]");
//		int totalRevistas = leerIndiceRevistas("clave:[0 TO 9]");
		int totalRevistas = leerIndiceRevistas("origen:[aA TO zZ]");
		int totalLibros = leerIndiceLibros("titulo:[aA TO Zz]");

		Query qu = null;
		try {
			try {
				qu = new QueryParser(" ", analyzer).parse("clave:[0 TO 9]");
			} catch (ParseException e) {

				e.printStackTrace();
			}
			int hitsPerPage = 100000;
			IndexReader reader;
			reader = DirectoryReader.open(directoryR);
			IndexSearcher searcher = new IndexSearcher(reader);
			TopDocs docs = searcher.search(qu, hitsPerPage);
			ScoreDoc[] hits = docs.scoreDocs;
			for (int i = 0; i < hits.length; i++) {
				int docId = hits[i].doc;
				Document doc = searcher.doc(docId);
				String[] anios = doc.get("numeros").split("<<<");
				for (String object : anios) {
					String[] numeros = object.split("<<");
					totalNumeros += (numeros.length - 1);
				}
			}
			reader.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		crearJsonHome(totalArticulos, totalRevistas, totalLibros, totalNumeros);
	}

	@SuppressWarnings("unchecked")
	public void crearJsonHome(int totalArticulos, int totalRevistas, int totalLibros, int totalNumeros) {

		JSONObject obj = new JSONObject();

		JSONObject innerObj = new JSONObject();
		innerObj.put("totalArticulos", totalArticulos);
		innerObj.put("totalRevistas", totalRevistas);
		innerObj.put("totalLibros", totalLibros);
		innerObj.put("totalNumeros", totalNumeros);

		obj.put("NumerariasHome", innerObj);

		try {
			System.out.println("crearJsonHome:::" + bundlePaths.getString("rutaDefaultJsonNumerarias"));
			FileWriter file = new FileWriter(bundlePaths.getString("rutaDefaultJsonNumerarias"));
			file.write(obj.toJSONString());
			file.flush();
			file.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void obtenerAllPaises(int tipo) {
		Boolean found = false;
		Query qu = null;
		String claveISO2 = "";

		try {
			try {
				qu = new QueryParser(" ", analyzer).parse("Nombre:[Aa TO Zz]");
			} catch (ParseException e) {

				e.printStackTrace();
			}
			int hitsPerPage = 100000;
			IndexReader reader;
			reader = DirectoryReader.open(directoryR);
			IndexSearcher searcher = new IndexSearcher(reader);
			TopDocs docs = searcher.search(qu, hitsPerPage);
			ScoreDoc[] hits = docs.scoreDocs;
			for (int i = 0; i < hits.length; i++) {
				found = false;
				int docId = hits[i].doc;
				Document doc = searcher.doc(docId);
				if (tipo == 1) {
					if (!paises.isEmpty()) {
						for (int j = 0; j < paises.size(); j++) {
							if (doc.get("clavePais").equals(paises.get(j).getClave())) {
								paises.get(j).setTotal(paises.get(j).getTotal() + 1);
								found = true;
								break;
							}
						}
						if (!found) {
							paises.add(new ModeloNumerariasColeccion(doc.get("pais"), doc.get("clavePais"), "1"));
						}
					} else {
						paises.add(new ModeloNumerariasColeccion(doc.get("pais"), doc.get("clavePais"), "1"));
					}
				} else {
					claveISO2 = obtenerClavesISO(doc.get("pais"));
					if (!paisesMapa.isEmpty()) {
						for (int j = 0; j < paisesMapa.size(); j++) {
							if (doc.get("clavePais").equals(paisesMapa.get(j).getClave())) {
								paisesMapa.get(j).setZ(paisesMapa.get(j).getZ() + 1);
								found = true;
								break;
							}
						}
						if (!found) {
							paisesMapa.add(new ModeloJsonColeccionMapa(doc.get("pais"), doc.get("clavePais"), "",
									claveISO2, 1));
						}
					} else {
						paisesMapa.add(
								new ModeloJsonColeccionMapa(doc.get("pais"), doc.get("clavePais"), "", claveISO2, 1));
					}
				}

			}
			reader.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void obtenerAllInstituciones() {
		Boolean found = false;
		Query qu = null;
		try {
			try {
				qu = new QueryParser(" ", analyzer).parse("Nombre:[Aa TO Zz]");
			} catch (ParseException e) {

				e.printStackTrace();
			}
			int hitsPerPage = 100000;
			IndexReader reader;
			reader = DirectoryReader.open(directoryR);
			IndexSearcher searcher = new IndexSearcher(reader);
			TopDocs docs = searcher.search(qu, hitsPerPage);
			ScoreDoc[] hits = docs.scoreDocs;
			for (int i = 0; i < hits.length; i++) {
				found = false;
				int docId = hits[i].doc;
				Document doc = searcher.doc(docId);
				if (!instituciones.isEmpty()) {
					for (int j = 0; j < instituciones.size(); j++) {
						if (doc.get("claveInstitucion").equals(instituciones.get(j).getClave())) {
							instituciones.get(j).setTotal((instituciones.get(j).getTotal() + 1));
							found = true;
							break;
						}
					}
					if (!found) {
						instituciones.add(new ModeloNumerariasColeccion(doc.get("Institucion"),
								doc.get("claveInstitucion"), "1"));
					}
				} else {
					instituciones.add(
							new ModeloNumerariasColeccion(doc.get("Institucion"), doc.get("claveInstitucion"), "1"));
				}

			}
			reader.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void obtenerTotalAllAreas() {
		String consulta;
		int total;
		for (int i = 0; i < cienciasSociales.size(); i++) {
			consulta = "claveDelArea1:" + cienciasSociales.get(i).getClave();
			total = leerIndiceRevistas(consulta);
			cienciasSociales.get(i).setTotal(total);
		}
		for (int i = 0; i < cienciasNaturales.size(); i++) {
			consulta = "claveDelArea1:" + cienciasNaturales.get(i).getClave();
			total = leerIndiceRevistas(consulta);
			cienciasNaturales.get(i).setTotal(total);
		}
		for (int i = 0; i < multidiciplinarias.size(); i++) {
			consulta = "claveDelArea1:" + multidiciplinarias.get(i).getClave();
			total = leerIndiceRevistas(consulta);
			multidiciplinarias.get(i).setTotal(total);
		}
		for (int i = 0; i < arteHumanidades.size(); i++) {
			consulta = "claveDelArea1:" + arteHumanidades.get(i).getClave();
			total = leerIndiceRevistas(consulta);
			arteHumanidades.get(i).setTotal(total);
		}
	}

	public int leerIndiceRevistas(String consulta) {
		Query qu = null;
		try {
			try {
				qu = new QueryParser(" ", analyzer).parse(consulta);
			} catch (ParseException e) {

				e.printStackTrace();
			}
			int hitsPerPage = 100000;
			IndexReader reader;
			reader = DirectoryReader.open(directoryR);
			IndexSearcher searcher = new IndexSearcher(reader);
			TopDocs docs = searcher.search(qu, hitsPerPage);
			hits = docs.scoreDocs;
			reader.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return hits.length;
	}

	public int leerIndiceArticulos(String consulta) {
		Query qu = null;
		try {
			try {
				qu = new QueryParser(" ", analyzer).parse(consulta);
			} catch (ParseException e) {

				e.printStackTrace();
			}
			int hitsPerPage = 100000;
			IndexReader reader;
			reader = DirectoryReader.open(directoryA);
			IndexSearcher searcher = new IndexSearcher(reader);
			TopDocs docs = searcher.search(qu, hitsPerPage);
			hits = docs.scoreDocs;
			reader.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return hits.length;
	}
	
	public int leerIndiceLibros(String consulta) {
		Query qu = null;
		try {
			try {
				qu = new QueryParser(" ", analyzer).parse(consulta);
			} catch (ParseException e) {

				e.printStackTrace();
			}
			int hitsPerPage = 100000;
			IndexReader reader;
			reader = DirectoryReader.open(directoryL);
			IndexSearcher searcher = new IndexSearcher(reader);
			TopDocs docs = searcher.search(qu, hitsPerPage);
			hits = docs.scoreDocs;
			reader.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return hits.length;
	}

	public void initAreasGrupo1() {
		cienciasSociales.add(new ModeloNumerariasColeccion("Comunicación", "3", "0"));
		cienciasSociales.add(new ModeloNumerariasColeccion("Estudios Culturales", "4", "0"));
		cienciasSociales.add(new ModeloNumerariasColeccion("Demografía", "5", "0"));
		cienciasSociales.add(new ModeloNumerariasColeccion("Derecho", "6", "0"));
		cienciasSociales.add(new ModeloNumerariasColeccion("Administración y Contabilidad", "1", "0"));
		cienciasSociales.add(new ModeloNumerariasColeccion("Antropología", "2", "0"));
		cienciasSociales.add(new ModeloNumerariasColeccion("Divulgación Científica", "7", "0"));
		cienciasSociales.add(new ModeloNumerariasColeccion("Economía y Finanzas", "8", "0"));
		cienciasSociales.add(new ModeloNumerariasColeccion("Educación", "9", "0"));
		cienciasSociales.add(new ModeloNumerariasColeccion("Estudios Territoriales", "10", "0"));
		cienciasSociales.add(new ModeloNumerariasColeccion("Geografía Social", "12", "0"));
		cienciasSociales.add(new ModeloNumerariasColeccion("Política", "14", "0"));
		cienciasSociales.add(new ModeloNumerariasColeccion("Psicología", "15", "0"));
		cienciasSociales.add(new ModeloNumerariasColeccion("Sociología", "16", "0"));
		cienciasSociales.add(new ModeloNumerariasColeccion("Estudios Agrarios", "22", "0"));
		cienciasSociales.add(new ModeloNumerariasColeccion("Multidisciplinarias (Ciencias Sociales)", "17", "0"));
		cienciasSociales.add(new ModeloNumerariasColeccion("Estudios Ambientales", "19", "0"));
		cienciasSociales.add(new ModeloNumerariasColeccion("Salud", "20", "0"));
		cienciasSociales.add(new ModeloNumerariasColeccion("Ciencias de la Información", "23", "0"));
		cienciasSociales.add(new ModeloNumerariasColeccion("Relaciones Internacionales", "25", "0"));
		cienciasSociales.add(new ModeloNumerariasColeccion("Revistas Institucionales", "26", "0"));
		cienciasSociales.add(new ModeloNumerariasColeccion("Estudios de Turismo", "47", "0"));
	}

	public void initAreasGrupo2() {
		cienciasNaturales.add(new ModeloNumerariasColeccion("Química", "41", "0"));
		cienciasNaturales
				.add(new ModeloNumerariasColeccion("Multidisciplinaria (Ciencias Naturales y Exactas)", "44", "0"));
		cienciasNaturales.add(new ModeloNumerariasColeccion("Veterinaria", "43", "0"));
		cienciasNaturales.add(new ModeloNumerariasColeccion("Ciencias de la Tierra", "48", "0"));
		cienciasNaturales.add(new ModeloNumerariasColeccion("Biología", "31", "0"));
		cienciasNaturales.add(new ModeloNumerariasColeccion("Computación", "33", "0"));
		cienciasNaturales.add(new ModeloNumerariasColeccion("Física", "34", "0"));
		cienciasNaturales.add(new ModeloNumerariasColeccion("Geología", "36", "0"));
		cienciasNaturales.add(new ModeloNumerariasColeccion("Física, Astronomía y Matemáticas", "49", "0"));
		cienciasNaturales.add(new ModeloNumerariasColeccion("Ingeniería", "38", "0"));
		cienciasNaturales.add(new ModeloNumerariasColeccion("Agrociencias", "28", "0"));
		cienciasNaturales.add(new ModeloNumerariasColeccion("Matemáticas", "39", "0"));
		cienciasNaturales.add(new ModeloNumerariasColeccion("Medicina", "40", "0"));
	}

	public void initAreasGrupo3() {
		multidiciplinarias.add(new ModeloNumerariasColeccion(
				"Multidisciplinarias (Ciencias, Ciencias Sociales, Artes y Humanidades)", "45", "0"));

	}

	public void initAreasGrupo4() {
		arteHumanidades.add(new ModeloNumerariasColeccion("Teología", "50", "0"));
		arteHumanidades.add(new ModeloNumerariasColeccion("Arquitectura", "29", "0"));
		arteHumanidades.add(new ModeloNumerariasColeccion("Filosofía", "11", "0"));
		arteHumanidades.add(new ModeloNumerariasColeccion("Historia", "13", "0"));
		arteHumanidades.add(new ModeloNumerariasColeccion("Arte", "20", "0"));
		arteHumanidades.add(new ModeloNumerariasColeccion("Lengua y Literatura", "24", "0"));
	}

	public CharArraySet stopwords() {
		CharArraySet stopSet = null;
		try {
			stopSet = CharArraySet.copy(StandardAnalyzer.STOP_WORDS_SET);
			BufferedReader bufferedReader = new BufferedReader(new FileReader(resourceBundle.getString("stopwords")));
			String palabraStopWord;

			while ((palabraStopWord = bufferedReader.readLine()) != null) {
				stopSet.add(palabraStopWord);
			}
			bufferedReader.close();
		} catch (Exception ex) {

		}
		return stopSet;
	}

	public String obtenerClavesISO(String name) {
		String query = "select codentnac from tblentnac where nomentnac like '" + name + "%' limit 1";
		resulSet = postgresql.consultar(query, connect);
		String claveISO = "";
		try {
			if (resulSet != null) {
				while (resulSet.next()) {
					claveISO = resulSet.getString("codentnac");
				}
			}
		} catch (Exception ex) {

		}
		return claveISO;
	}
}
