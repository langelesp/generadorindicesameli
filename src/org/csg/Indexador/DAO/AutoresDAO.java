package org.csg.Indexador.DAO;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import org.apache.commons.io.FilenameUtils;
import org.apache.lucene.analysis.CharArraySet;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.csg.ConectionPostgreSQL.PostgreSQLJDBC;
import org.csg.Indexador.DocumentoIndex;
import org.csg.Indexador.FicheroLog;
import org.csg.Indexador.Modelo.ModeloField;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;
import org.dom4j.Element;
import org.dom4j.Node;

/**
 * Clase que realiza el parseo de los xml, para obtener los autores
 * de los articulos.
 * Tambien lee el índice de libros, para obtener a sus autores
 * y crear un indice de ambos tipos de autores.
 * 
 * @author Carina
 *
 */
public class AutoresDAO extends Thread {

	private Connection connect = null;
	private PostgreSQLJDBC consultar = new PostgreSQLJDBC();

	private StandardAnalyzer analyzer;
	private Directory index;
	private IndexWriterConfig config;
	private IndexWriter writoo;
	private Directory directoryA;
	private Directory directoryL;
	private DocumentoIndex indexDoc = new DocumentoIndex();

	private FicheroLog ficherolog = new FicheroLog();

	private String log = "";
	private String rutaIndice = "";
	private String claveArticulo = "";
	private String idsContribs = "";
	private String progreso = "";

	private ResourceBundle resourceBundle = null;
	private ResourceBundle bundlePaths = null;

	private ArrayList<ModeloField> fieldsFormasAutor = new ArrayList<>();
	private ArrayList<String> filesXML = new ArrayList<>();
	private ArrayList<Object> componentes = new ArrayList<>();

	public AutoresDAO() {

	}

	/**
	 * Constructor de cuatro argumentos para inicializar las variables
	 * @param rutaIndex String que contiene la ruta del índice de libros
	 * @param filesXML Lista de Strings, que contienen las rutas de los Xml, obtenidas del jatsRepo ingresado
	 * @param connect Variable que guarda la conexión realizada a la base de datos de postgres
	 * @param componentes Lista de objetos que forman parte de la venta de la vista
	 */
	public AutoresDAO(String rutaIndex, ArrayList<String> filesXML, Connection connect, ArrayList<Object> componentes) {
		this.rutaIndice = rutaIndex;
		this.filesXML = filesXML;
		this.componentes = componentes;
		this.ficherolog.crearLog(rutaIndice);
		this.bundlePaths = ResourceBundle.getBundle("paths");
		this.resourceBundle = ResourceBundle.getBundle("querys");
		try {
			this.connect = connect;
			this.directoryA = FSDirectory.open(Paths.get(bundlePaths.getString("rutaDefaultIndexArt")));
			this.directoryL = FSDirectory.open(Paths.get(bundlePaths.getString("rutaDefaultIndexLibros")));
			this.analyzer = new StandardAnalyzer(stopwords());
			this.index = FSDirectory.open(Paths.get(rutaIndice));
			this.config = new IndexWriterConfig(analyzer);
			this.writoo = new IndexWriter(index, config);
		} catch (Exception ex) {

		}
	}

	/**
	 * Inicia el proceso de obtención de datos para el índice
	 */
	@Override
	public void run() {
		log = ">>>>>>  Inicia la lectura de los XMLs <<<<<<";
		FicheroLog.logger.info(log);
		String rutaLog = FicheroLog.fechaS;
		Boolean parseStatus = true;
		
		int total = filesXML.size();
		double progress = 0;
		double aux = 0;

		try {
			/*Indizando autores de articulos, lectura de XMLS del JatsRepo*/
			for (int i = 0; i < filesXML.size(); i++) {
				parseStatus = true;
				startFielsAutores();
				claveArticulo = obtenerClave(filesXML.get(i));

				if (!claveArticulo.equals("")) {
					if (comprobarArticuloEnIndice(claveArticulo)) {
						getDatosAutoresXML(filesXML.get(i));
						fieldsFormasAutor.clear();

					} else {
						FicheroLog.logger.warning("\n>>>>> ** No se indizarán los autores del articulo : "
								+ claveArticulo + " Clave NO encontrada en Indice de Artículos ** <<<<<<");
						fieldsFormasAutor.clear();
					}
				} else {
					fieldsFormasAutor.clear();
					log = "\n>>>>>> XML invalido ERROR TYPE! -> " + filesXML.get(i);
					((TextArea) componentes.get(5)).setText("XML invalido ERROR TYPE! -> " + filesXML.get(i));
					FicheroLog.logger.warning(log);
				}
//				aux = ((i + 1) * 1);
//				progress = (aux / total);
//				progreso = String.valueOf(progress * 100);
//				progreso = progreso.substring(0, progreso.indexOf('.'));
//				((TextField) componentes.get(8)).setText(progreso + "%");
//				((ProgressBar) componentes.get(6)).setProgress(progress);
			}
			
			/*Indizando autores de libros, lectura del Indice de libros*/
			getAutoresDeLibros();
			
			
			log = "\n|************ Se indizaron: " + writoo.numDocs() + " Autores **********|";
			((TextArea) componentes.get(5)).setText("************ Se indizaron: " + writoo.numDocs() + " Autores **********");
			FicheroLog.logger.info(log);
			writoo.close();
			FicheroLog.fh.close();
			consultar.closeConnect(connect);
		} catch (Exception ex) {
			FicheroLog.logger.warning("\n************ ERROR de acceso al JatsRepo.. Indexación CANCELADA **********");
			((TextArea) componentes.get(5)).setText("************ ERROR de acceso al JatsRepo.. Indexación CANCELADA **********");
		}
		((TextArea) componentes.get(9)).setText("Log creado -> " + " " + rutaLog);
		((RadioButton) componentes.get(0)).setDisable(false);
		((RadioButton) componentes.get(1)).setDisable(false);
		((RadioButton) componentes.get(10)).setDisable(false);
		((RadioButton) componentes.get(11)).setDisable(false);
		((RadioButton) componentes.get(13)).setDisable(false);
		
		((Button) componentes.get(2)).setDisable(false);

	}

	/**
	 * Obtiene los datos de los autores del respectivo XML
	 * @param ruta String que guarda la ruta, de donde se ira a buscar el XML a parsear.
	 */
	public void getDatosAutoresXML(String ruta) {
		try {
			File inputFile = new File(ruta);
			SAXReader reader = new SAXReader();
			Document document = reader.read(inputFile);

			List<Node> nodes = document.selectNodes("/article/front/article-meta/contrib-group/contrib");
			String affAutorID = "";
			if (nodes.isEmpty()) {
				FicheroLog.logger.info("\n>>>>> ** El articulo " + claveArticulo + " NO cuenta con Autores** <<<<<< "+ruta);
			} else {
				((TextArea) componentes.get(5)).setText("Indizando autores del ARTICULO: " + claveArticulo);//
				for (Node node : nodes) {
					//System.out.println(node.valueOf("@contrib-type"));
					if(node.valueOf("@contrib-type")!=null)
						if (node.valueOf("@contrib-type").equals("author")) {
							List<Node> contribsID = node.selectNodes("contrib-id");
							if (!contribsID.isEmpty()) {
								for (Node contribsid : contribsID) {
									if (contribsid.valueOf("@contrib-id-type").equals("redalyc")
											|| contribsid.valueOf("@contrib-id-type").equals("orcid")) {
										idsContribs += contribsid.valueOf("@contrib-id-type") + "<<" + contribsid.getText()
												+ "<<<";
									}
								}
								fieldsFormasAutor.get(3).setContenidoField(idsContribs);
								idsContribs = "";
							}
							//System.out.println( node.selectSingleNode("name")!=null );
							Node childNames = node.selectSingleNode("name");
							fieldsFormasAutor.get(0).setContenidoField(childNames.selectSingleNode("given-names").getText()
									+ " " + childNames.selectSingleNode("surname").getText());
	
							FicheroLog.logger
									.info("\n>>>>> ** Indizando autor : " + fieldsFormasAutor.get(0).getContenidoField()
											+ " del ARTICULO -> " + claveArticulo + " ** <<<<<<");
							
							if(node.selectSingleNode("email")!= null)
								fieldsFormasAutor.get(16).setContenidoField(node.selectSingleNode("email").getText());
							
							Node aff = node.selectSingleNode("xref");
							if(aff != null) {
								affAutorID = aff.valueOf("@rid");
								List<Node> AffAutorAttribs = document.selectNodes("/article/front/article-meta/aff");
								for (Node affautoratt : AffAutorAttribs) {
									if (affautoratt.valueOf("@id").equals(affAutorID)) {
										List<Node> instituciones = affautoratt.selectNodes("institution");
										for (Node ins : instituciones) {
											if (ins.valueOf("@content-type").equals("orgname")) {
												fieldsFormasAutor.get(1).setContenidoField(
														ins.getText().replaceAll("\n", " "));
												break;
											}
										}
										fieldsFormasAutor.get(2).setContenidoField(affautoratt.selectSingleNode("country")
												.getText().replaceAll("\n", "").replaceAll("\\s+", " "));
										obtenerClavePaisPorNombre(fieldsFormasAutor.get(2).getContenidoField());
										break;
									}
								}
							}
							//System.out.println(inputFile.getParentFile().getName());
							fieldsFormasAutor.get(23).setContenidoField(inputFile.getParentFile().getName());
							fieldsFormasAutor.get(21).setContenidoField("Artículo");
							indexar();
						}
				}
			}
		} catch (Exception ex) {
			FicheroLog.logger.warning(">>>> ** Ocurrio un error mientras se obtenian los datos"
					+ " del XML método getDatosAutoresXML** <<<<"+ruta);
		}

	}
	/**
	 * Lee el índice de libros y obtiene a sus autores
	 */
	public void getAutoresDeLibros() {
		fieldsFormasAutor.clear();
		Query qu = null;
		try {
			try {
				qu = new QueryParser(" ", analyzer).parse("titulo:[aA TO zZ]");
			} catch (ParseException e) {
				log = "-------No se pudo realizar la consulta al índice de Libros------";
				FicheroLog.logger.warning(log);
				e.printStackTrace();
			}
			
			IndexReader reader;
			reader = DirectoryReader.open(directoryL);
			IndexSearcher searcher = new IndexSearcher(reader);
			TopDocs docs = searcher.search(qu, 100000);
			ScoreDoc[] hits = docs.scoreDocs;
			String [] autores;
			String [] fecha;
			String anioLibro = "";
			String autorLibro = "";
			String autoresDeLibro =  "";
			Boolean foundAnio =  false;
			System.out.println("consultando del indice Libros"+this.directoryL);
			for (int i = 0; i < hits.length; i++) {
				//System.out.println(+i);
				int docId = hits[i].doc;
				org.apache.lucene.document.Document doc = searcher.doc(docId);
				if(doc.get("autores")!= null) {//autores
					//System.out.println("existe autor");
					autores = doc.get("autores").split("<<<");
					//System.out.println(autores+"<- nombre");
					autoresDeLibro = doc.get("autores").replaceAll("<<<", ";");
					
					try {
						fecha = doc.get("anio").split("-");
						for(int a = 0; a < fecha.length; a++) {
							if(fecha[a].length() == 4) {
								anioLibro = fecha[a];
								foundAnio = true;
								break;
							}
						}
						if(!foundAnio) {
							anioLibro = doc.get("anio");
						}
						foundAnio = false;
						
					}catch(Exception ex) {
						anioLibro = doc.get("anio");
					}
	
					for(int j = 0; j < autores.length; j++) {
						startFielsAutores();
						
						autorLibro = autores[j];
	
						fieldsFormasAutor.get(6).setContenidoField(autoresDeLibro);
						fieldsFormasAutor.get(21).setContenidoField("Libro");
						fieldsFormasAutor.get(0).setContenidoField(autorLibro);
						fieldsFormasAutor.get(22).setContenidoField(doc.get("titulo"));
						fieldsFormasAutor.get(23).setContenidoField(doc.get("link"));
						fieldsFormasAutor.get(24).setContenidoField(anioLibro);
						fieldsFormasAutor.get(25).setContenidoField(doc.get("lenguaje"));
						fieldsFormasAutor.get(26).setContenidoField(doc.get("editor"));
						
						((TextArea) componentes.get(5)).setText("Indizando autores del LIBRO: " + doc.get("titulo"));//
						FicheroLog.logger.info("\n>>>>> ** Indizando autor : " + fieldsFormasAutor.get(0).getContenidoField()
								+ " del LIBRO -> " + doc.get("titulo") + " ** <<<<<<");
						indexar();
						fieldsFormasAutor.clear();
					}
			}
		}
			reader.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * Obtiene la clave de país del autor, realiza una consulta a la base de datos, para obtener este dato
	 * @param namePais String que guarda el nombre del país al cual se consultara su clave
	 * @return String clavePais que guarda la clave de país obtenida
	 */
	public String obtenerClavePaisPorNombre(String namePais) {
		String clavePais = "";

		ResultSet resultado = null;
		String query = (resourceBundle.getString("obtenerClavePaisPorNombre") + namePais + "';");
		try {
			resultado = consultar.consultar(query, connect);
			while (resultado.next()) {
				fieldsFormasAutor.get(14).setContenidoField(resultado.getString("cveentnac"));
			}
		} catch (Exception ex) {
			FicheroLog.logger
					.warning(">>>> ** Ocurrio un error mientras se obtenia la clave" + " de país del Autor** <<<<");
		}
		return clavePais;
	}

	/**
	 * Inizializa la lista de los datos que conforman a cada autor.
	 * @return ArrayList<ModeloField>
	 */
	public ArrayList<ModeloField> startFielsAutores() {
		fieldsFormasAutor.add(0, new ModeloField("nombreAutor", "", true, -1, false));
		fieldsFormasAutor.add(1, new ModeloField("nombreInsDeAutor", "", true, -1, false));
		fieldsFormasAutor.add(2, new ModeloField("nombrePaisDeAutor", "", true, -1, true));
		fieldsFormasAutor.add(3, new ModeloField("idsContribuciones", "", true, -1, false));
		fieldsFormasAutor.add(4, new ModeloField("claveArticulo", "", true, -1, false));
		fieldsFormasAutor.add(5, new ModeloField("tituloArticulo", "", true, -1, false));
		fieldsFormasAutor.add(6, new ModeloField("autoresDeArticulo", "", true, -1, false));
		fieldsFormasAutor.add(7, new ModeloField("nombreRevista", "", true, -1, false));
		fieldsFormasAutor.add(8, new ModeloField("volumen", "", true, -1, false));
		fieldsFormasAutor.add(9, new ModeloField("numero", "", true, -1, false));
		fieldsFormasAutor.add(10, new ModeloField("anio", "", true, -1, false));
		fieldsFormasAutor.add(11, new ModeloField("nombreInstitucionDeArticulo", "", true, -1, false));
		fieldsFormasAutor.add(12, new ModeloField("nombrePaisDeArticulo", "", true, -1, false));
		fieldsFormasAutor.add(13, new ModeloField("urlFoto", "", true, -1, false));
		fieldsFormasAutor.add(14, new ModeloField("clavePaisAutor", "", true, -1, false));
		fieldsFormasAutor.add(15, new ModeloField("claveAreaPri", "", true, -1, false));
		fieldsFormasAutor.add(16, new ModeloField("email", "", true, -1, false));
		fieldsFormasAutor.add(17, new ModeloField("area", "", true, -1, true));
		fieldsFormasAutor.add(18, new ModeloField("claveRevista", "", true, -1, false));
		fieldsFormasAutor.add(19, new ModeloField("clavePaisArticulo", "", true, -1, false));
		fieldsFormasAutor.add(20, new ModeloField("claveNumeroArticulo", "", true, -1, false));
		fieldsFormasAutor.add(21, new ModeloField("tipo", "", true, -1, false));
		fieldsFormasAutor.add(22, new ModeloField("tituloLibro", "", true, -1, false));
		fieldsFormasAutor.add(23, new ModeloField("linkLibro", "", true, -1, false));
		fieldsFormasAutor.add(24, new ModeloField("anioLibro", "", true, -1, false));
		fieldsFormasAutor.add(25, new ModeloField("idiomaLibro", "", true, -1, false));
		fieldsFormasAutor.add(26, new ModeloField("editorLibro", "", true, -1, false));

		return fieldsFormasAutor;
	}

	/**
	 * Comprueba si el articulo al que pertenece el autor, se encuentra en el índice de Articulos.
	 * Esta condición solo aplica para autores de articulos, no de libros
	 * @param claveArticulo String que guarda la clave del articulo a consultar en el índice
	 * @return Boolean True: Se encuentra en el índice Flase: No se encuentra en el índice
	 */
	public Boolean comprobarArticuloEnIndice(String claveArticulo) {

		Boolean foundArticle = false;

		Query qu = null;
		try {
			try {
				qu = new QueryParser(" ", analyzer).parse("claveArt:" + claveArticulo);
			} catch (ParseException e) {
				log = "-------No se pudo realizar la consulta del articulo en el índice------";
				FicheroLog.logger.warning(log);
				e.printStackTrace();
			}
			int hitsPerPage = 5;
			IndexReader reader;
			reader = DirectoryReader.open(directoryA);
			IndexSearcher searcher = new IndexSearcher(reader);
			TopDocs docs = searcher.search(qu, hitsPerPage);
			ScoreDoc[] hits = docs.scoreDocs;
			foundArticle = hits.length > 0 ? true : false;

			for (int i = 0; i < hits.length; i++) {
				int docId = hits[i].doc;
				org.apache.lucene.document.Document doc = searcher.doc(docId);
				fieldsFormasAutor.get(4).setContenidoField(doc.get("claveArt"));
				fieldsFormasAutor.get(5).setContenidoField(doc.get("tituloArt"));
				fieldsFormasAutor.get(6).setContenidoField(doc.get("autores"));
				fieldsFormasAutor.get(7).setContenidoField(doc.get("nombreRevista"));
				fieldsFormasAutor.get(8).setContenidoField(doc.get("volumen"));
				fieldsFormasAutor.get(9).setContenidoField(doc.get("numero"));
				fieldsFormasAutor.get(10).setContenidoField(doc.get("anio"));
				fieldsFormasAutor.get(11).setContenidoField(doc.get("institucion"));
				fieldsFormasAutor.get(12).setContenidoField(doc.get("pais"));
				fieldsFormasAutor.get(15).setContenidoField(doc.get("cveArea"));
				fieldsFormasAutor.get(17).setContenidoField(doc.get("area"));
				fieldsFormasAutor.get(18).setContenidoField(doc.get("claveRevista"));
				fieldsFormasAutor.get(19).setContenidoField(doc.get("cvePais"));
				fieldsFormasAutor.get(20).setContenidoField(doc.get("cveNumero"));
			}
			reader.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return foundArticle;

	}

	/**
	 * Obtiene la clave del articulo, apartir del nombre del xml
	 * @param path String que almacena la ruta del xml
	 * @return String que guarda el nombre del xml, si el nombre no cumple con las condiciones, para
	 * ser una clave devuelve una cadena vacia.
	 */
	public String obtenerClave(String path) {
		String baseName = FilenameUtils.getBaseName(path);
		System.out.println(baseName);
		if (baseName.matches(".*[A-Za-z].*")) {
			return "";
		} else {
			return baseName;
		}
	}

	/**
	 * Indexa los autores validados y creados
	 */
	public void indexar() {
		try {
			writoo.addDocument(indexDoc.indizarDocumento(fieldsFormasAutor));
			writoo.commit();
		} catch (Exception ex) {
			FicheroLog.logger.warning("ERROR! Metodo Indexar" + ": " + ex.getMessage() + " Indización cancelada");
			System.exit(0);
		}
	}

	public CharArraySet stopwords() {
		CharArraySet stopSet = null;
		try {
			stopSet = CharArraySet.copy(StandardAnalyzer.STOP_WORDS_SET);
			BufferedReader bufferedReader = new BufferedReader(new FileReader(bundlePaths.getString("stopwords")));
			String palabraStopWord;

			while ((palabraStopWord = bufferedReader.readLine()) != null) {
				stopSet.add(palabraStopWord);
			}
			bufferedReader.close();
		} catch (Exception ex) {

		}
		return stopSet;
	}
}
