
package org.csg.Indexador.DAO;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.io.FilenameUtils;
import org.apache.lucene.analysis.CharArraySet;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import org.csg.ConectionPostgreSQL.PostgreSQLJDBC;
import org.csg.ConnectionOracle.OracleSQLJDBC;
import org.csg.Indexador.Modelo.ModeloField;
import org.csg.Indexador.DocumentoIndex;
import org.csg.Indexador.FicheroLog;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;
import org.dom4j.Element;
import org.dom4j.Node;

import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 * Esta clase contiene los metodos para obtener los datos de articulos tanto de
 * base de datos, como de XMLs. <br>
 * El proceso que se realiza antes de indizar estos datos, es validar que el
 * numero al que pertenecen se encuentre terminado, y que la revista a la que
 * pertenecen se encuentre disponible para CSG <br>
 * <br>
 * Inicializa las variables de conexion a base de datos, analizador y escritor
 * del indice.
 * 
 * @author USUARIO
 *
 */
public class ArticuloSDAO extends Thread {
	ArrayList<ModeloField> fieldsRev = new ArrayList<>();
	SAXReader reader = new SAXReader();
	Document document;
	
	ArrayList<String> datosXML = new ArrayList<>();
	ArrayList<String> datosArtDB = new ArrayList<>();
	ArrayList<String> filesXML = new ArrayList<>();
	private ArrayList<Object> componentes = new ArrayList<>();
	

	private Connection oracleConnect = null;
	private OracleSQLJDBC oraclesql = new OracleSQLJDBC();
	
	ResourceBundle resourceBundle = null;

	StandardAnalyzer analyzer;
	Directory index;
	IndexWriterConfig config;
	IndexWriter writoo;
	DocumentoIndex indexDoc = new DocumentoIndex();
	PostgreSQLJDBC consultar = new PostgreSQLJDBC();
	
	FicheroLog ficherolog = new FicheroLog();
	String claveID = "";
	private Connection connect = null;
	String log = "";
//	RadioButton radioArt = new RadioButton();
//	RadioButton radioRev = new RadioButton();
//	Button btnValidar = new Button();
//	TextArea txtarea = new TextArea();
//	TextArea txtLog = new TextArea();
	String progreso = "";
	String rutaIndice = "";
	String contenido_recursivo = "";

	/**
	 * Constructor que inicializa las variables para leer y escribir los xmls, as-
	 * como el -ndice.
	 * 
	 * @param pathIndex
	 *            String que indica la ruta en donde se guarda el indice creado
	 * @param filesXML
	 *            ArrayList de tipo String, que almacena las rutas de los xmls que
	 *            para leer
	 */
	public ArticuloSDAO(String pathIndex, ArrayList<String> filesXML, Connection connect) {
		try {
			this.rutaIndice = pathIndex;
			this.filesXML = filesXML;
			this.connect = connect;
			ficherolog.crearLog(pathIndex);
			
			try {
				analyzer = new StandardAnalyzer(stopwords());
				index = FSDirectory.open(Paths.get(pathIndex));
				config = new IndexWriterConfig(analyzer);
				writoo = new IndexWriter(index, config);
				resourceBundle = ResourceBundle.getBundle("querys");
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public ArticuloSDAO(String pathIndex, ArrayList<String> filesXML, Connection connect,
		ArrayList<Object> componentes) {
		this.rutaIndice = pathIndex;
		this.filesXML = filesXML;
		this.componentes = componentes;
		ficherolog.crearLog(pathIndex);
		try {
			this.connect = connect;

			try {
				analyzer = new StandardAnalyzer(stopwords());
				index = FSDirectory.open(Paths.get(pathIndex));
				config = new IndexWriterConfig(analyzer);
				writoo = new IndexWriter(index, config);
				resourceBundle = ResourceBundle.getBundle("querys");
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	/**
	 * Metodo que inicia con el proceso de lectura de los XMLs. Atraves de una
	 * iteraci-n se obtiene la ruta de ubicaci-n del XML, posteriormente se obtiene
	 * la clave de articulo de este, y se valida el estado del n-mero al que
	 * pertenece, para determinar si es un n-mero terminado.
	 */
	private int generatedcont = 0;
	public void run() {
		try {

			this.oracleConnect = oraclesql.connection();
//			System.out.println("oraclesql.iniciado oracleConnect.consultacualquiera:::");
//			oraclesql.consultapre(this.oracleConnect);
			log = ">>>>>>  Inicia la lectura de los XMLs <<<<<<";

//			((TextArea) componentes.get(5)).setText("El progreso para la generacion de este indice sera mostrado en  la consola! "
//					+ "\n Ejecutese el Indexador.jar en lineas de comandos para mostrar dicho progreso...");
			FicheroLog.logger.info(log);

			int total = filesXML.size();
			double progress = 0;
			double aux = 0;
			generatedcont = 0;
			String rutaLog = FicheroLog.fechaS;
			Boolean parseStatus = true;
			int redalycExcluidos = 0;
			int redalycIncluidos = 0;
			for (int i = 0; i < filesXML.size(); i++) {
				parseStatus = true;
//				System.out.println("antes de consulta::" + filesXML.get(i));
				claveID = obtenerClave(filesXML.get(i));
//				System.out.println("filesXML::" + filesXML.get(i) + " JatsRepoRedalyc?:" + filesXML.get(i).contains("JatsRepoRedalyc"));
				String origen = filesXML.get(i).contains("JatsRepoRedalyc") ? "Redalyc":"Ameli";
//				System.out.println("origen::" + origen + " is R?:" + origen.equals("Redalyc"));
				String cveArt = "";
				if(origen.equals("Redalyc")) {
					//revista
//					String subrutaRev = filesXML.get(i).substring(filesXML.get(i).lastIndexOf("JatsRepoRedalyc"));
//					subrutaRev = subrutaRev.replace("JatsRepoRedalyc\\", "");
//					System.out.println("subruta::" + subrutaRev);
//					String cveRevista = subrutaRev.substring(0, subrutaRev.indexOf("\\"));
//					System.out.println("cveRevista::" + cveRevista);
					//articulo
					String subrutaArt = filesXML.get(i).substring(filesXML.get(i).lastIndexOf("\\"));
					subrutaArt = subrutaArt.replace("\\", "");
//					System.out.println("subrutaArt::" + subrutaArt);
					cveArt = subrutaArt.substring(0, subrutaArt.indexOf("."));
//					System.out.println("cveArt::" + cveArt);
					ResultSet resultados = null;
					String query1 = resourceBundle.getString("obtCveRevistaRedalyc") + cveArt;
					System.out.println("query1::parafiltrarRevista:::" + query1);
					resultados = oraclesql.consultar(query1, oracleConnect);
					if (resultados != null) {
						String cveRev = "";
						if(resultados!=null) {
							while (resultados.next()) {
								cveRev = resultados.getString("cveentrev");
							}
						}
						if(cveRev == null) {
							claveID = "";
						}else{
							claveID = cveRev;
							redalycIncluidos = redalycIncluidos + 1;
							System.out.println("genera indice...incluidos::" + redalycIncluidos);
						}
					}
					System.out.println("claveID::::" + claveID);
				}
				if (!claveID.equals("")) {
					boolean articuloEsEliminado = articuloESeliminado(claveID);
					if(!articuloEsEliminado|origen.equals("Redalyc")) {
						
						if (consultNumTerminado(claveID)|origen.equals("Redalyc")) {

						log = "\n>>>>>> Parseando " + (i + 1) + " de " + (filesXML.size());
						FicheroLog.logger.info(log);

						try {
							File inputFile = new File(filesXML.get(i));
							document = reader.read(inputFile);
							
						} catch (Exception ex) {
							parseStatus = false;
							log = "\n -> El articulo -> " + claveID + " no se indizará, !ERROR TYPE XML!";
							FicheroLog.logger.warning(log);
						}
						if (parseStatus) {
							startFielsRev();
//							System.out.println(filesXML + "::origen:::" + origen);
							getCamposXML(document, origen, cveArt);/*tarea1 aqui se pone el origen r|a getCamposXML(document, filesXML.);*/
							datosXML.clear();
							fieldsRev.clear();
						}

					} else {
						log = "\n>>>>>> Parseando " + (i + 1) + " de " + (filesXML.size());
						FicheroLog.logger.info(log);

						log = "\nEl número al que pertenece el artículo -> " + claveID
								+ " no se ha terminado o la revista revcsg=0 ! No se indizará de:Origen::" + origen + "::equals:" + origen.equals("Redalyc")
								+ "::articuloEsEliminado:::" + !articuloEsEliminado;
						FicheroLog.logger.info(log);

					}
					
				}else {
					log = "\n>>>>>> Articulo Eliminado, no se ha indizado -> " + filesXML.get(i);
					FicheroLog.logger.warning(log);
				}
				} else {
					log = "\n>>>>>> XML invalido ERROR TYPE! -> " + filesXML.get(i);
					FicheroLog.logger.warning(log);
				}
//				aux = ((i + 1) * 1);
//				progress = (aux / total);
//				progreso = String.valueOf(progress * 100);
//				progreso = progreso.substring(0, progreso.indexOf('.'));
//				((TextField) componentes.get(8)).setText(progreso + "%");
//				((ProgressBar) componentes.get(6)).setProgress(progress);
			}
			log = "\n|************ Se indizaron: " + writoo.numDocs() + " Articulos **********|";
//			((TextArea) componentes.get(5)).setText("|************ Se indizaron: " + writoo.numDocs() + " Articulos **********|");
			FicheroLog.logger.info(log);
			System.out.println("generados:::" + generatedcont);
			System.out.println("redalycIncuidos:::" + redalycIncluidos);
			System.out.println("redalycExcuidos:::" + redalycExcluidos);
			
//			((TextArea) componentes.get(9)).setText("Log creado -> " + " " + rutaLog);
			writoo.close();
			FicheroLog.fh.close();
			consultar.closeConnect(connect);
			oraclesql.closeConnect(oracleConnect);
		}catch(IllegalStateException iex) {
			
		}catch (Exception ex) {
			ex.printStackTrace();

			log = "\n************ ERROR de acceso al JatsRepo.. Indexaci-n CANCELADA **********";
			// txtarea.setText(txtarea.getText() + (log + "\n"));
			FicheroLog.logger.warning(log);
		}
//		((RadioButton) componentes.get(0)).setDisable(false);
//		((RadioButton) componentes.get(1)).setDisable(false);
//		((RadioButton) componentes.get(10)).setDisable(false);
//		((RadioButton) componentes.get(11)).setDisable(false);
//		((RadioButton) componentes.get(13)).setDisable(false);
//		
//		((Button) componentes.get(2)).setDisable(false);
		
		consultar.closeConnect(connect);
	}

	/**
	 * Metodo que realiza en parseo de los XMLs, para poder leerlos. Este m-todo
	 * hace uso de las siguientes variables: </br>
	 * 
	 * 
	 * <ul>
	 * <li><b>existentes :</b> Lista de tipo <code>existAttib</code>, esta lista
	 * almacena en cada pocisi-n el nombre, false o true si el campo existe, y la
	 * posici-n en la que se encuentra dentro de la lista existentes.
	 * <li><b>datosXML :</b> Lista que guarda la información de los campos que se
	 * obtienen del xml
	 * <li><b>doi :</b> String que guarda la direcci-n <code>doi</code> del
	 * articulo, si cuenta con ella.
	 * <li><b>autores :</b> String que almacena a los autores del articulo separados
	 * por '<<<' p. ej. "autor1 apellido<<<autor2 apellido"
	 * <li><b>keywords :</b> String que guarda las palabras clave del articulo,
	 * separadas por '<<<' p. ej. "keyword1<<<keyword2<<<keyword3"
	 * <li><b>resumen :</b> String que almacena el resumen del articulo y su
	 * respectivo idioma (puede haber articulos con resumen en otros idiomas)
	 * separados por '<<<' p. ej. "es<<<resumen<<<en<<<resumen<<<pt<<<resumen"
	 * <li><b>idArt :</b> String que almacena la clave del articulo
	 * <li><b>title : </b> String que almacena el titulo del articulo y su repectivo
	 * idioma (existen titulos con m-s de un idioma) separados pod '<<<' p. ej.
	 * "es<<<titulo<<<en<<<titulo<<<pt<<<titulo"
	 * <li><b>detPos :</b> Entero, este contador tambien se a-ade a la lista
	 * <code>exitentes</code> para indicar en que posici-n de la lista
	 * <code>datosXML</code> se encuentra para obtener el campo
	 * </ul>
	 */
	public void getCamposXML(Document document, String origen, String cveArticulo) {/*tarea1 , String origen) origen se pone aqui en setteado en true, y valor*/
		String doi_text = "";
		String autores_text = "";
		String autoresAp_text = "";
		String keywords_text = "";
		String resumen_text = "";
		String idArt = "";
		String article_title_text = "";
		String surnames = "";
		String contenido = "";
		Boolean indizar = false;
		contenido_recursivo = "";
		
		
		/////////////*** Se obtiene clave de revista y se valida la clave y verifica su estado en la base de datos ***** ///////////////
		
		Node claveRevista = document.selectSingleNode("/article/front/journal-meta/journal-id");
		
		if(claveRevista.getText().trim().matches("[0-9]*")|origen.equals("Redalyc")) {
			if(revistaEnBase(claveRevista.getText().trim())|origen.equals("Redalyc")) {
				fieldsRev.get(0).setExiste(true);
				fieldsRev.get(0).setContenidoField(claveRevista.getText().trim());
				indizar = true;
			}else {
				FicheroLog.logger.info("\n************ ERROR clave de Revista no existente en DB... Articulo No se Indizara **********");
				indizar = false;
			}
		}else {
			FicheroLog.logger.info("\n************ ERROR clave de Revista Invalida en XML... Articulo No se Indizara **********");
			indizar = false;
		}
		
		////////////************** Si la clave de Revista es valida se prosigue a obtener los siguientes datos ********////////////////////
		
		if(indizar) {
			
			Node doi = document.selectSingleNode("/article/front/article-meta/article-id[@pub-id-type='doi']");
			if(doi != null) {
				doi_text = doi.getText().trim().replaceAll("\n", "").replaceAll("\\s+", "");
				fieldsRev.get(13).setExiste(true);
				fieldsRev.get(13).setContenidoField(doi_text);
			}
			
			Node journalTitle = document.selectSingleNode("/article/front/journal-meta/journal-title-group/journal-title");
			if(journalTitle != null) {
				fieldsRev.get(1).setExiste(true);
				fieldsRev.get(1).setContenidoField(
						journalTitle.getText().trim().replaceAll("\n", " ").replaceAll("\\s+", " "));
			}
			
			Node issn = document.selectSingleNode("/article/front/journal-meta/issn[@pub-type='ppub']");
			if(issn != null) {
				fieldsRev.get(26).setExiste(true);
				fieldsRev.get(26).setContenidoField(
						issn.getText().trim());
			}
			Node issne = document.selectSingleNode("/article/front/journal-meta/issn[@pub-type='epub']");
			if(issne != null) {
				fieldsRev.get(27).setExiste(true);
				fieldsRev.get(27).setContenidoField(
						issne.getText().trim());
			}
			
			
			Node publisherName = document.selectSingleNode("/article/front/journal-meta/publisher/publisher-name");
			if(publisherName != null) {
				fieldsRev.get(2).setExiste(true);
				fieldsRev.get(2).setContenidoField(
						publisherName.getText().trim().replaceAll("\n", " ").replaceAll("\\s+", " "));
			}
			
			
			Node country = document.selectSingleNode("/article/front/journal-meta/publisher/publisher-loc/country");
			if(country != null) {
				fieldsRev.get(3).setExiste(true);
				fieldsRev.get(3).setContenidoField(
					country.getText().trim().replaceAll("\n", " ").replaceAll("\\s+", " "));
			}
			fieldsRev.get(28).setExiste(true);
			fieldsRev.get(28).setContenidoField(origen);
			
			/////////////********* Obteniendo titulo del articulo y su contenido de los hijos de titulo *****//////////
			Node articleTitle = document.selectSingleNode("/article/front/article-meta/title-group/article-title");
			if(articleTitle != null) {
				String idioma = articleTitle.valueOf("@xml:lang") != null ? articleTitle.valueOf("@xml:lang") : "es";
				idioma = !articleTitle.valueOf("@xml:lang").isEmpty() ? articleTitle.valueOf("@xml:lang") : "es";
				
				article_title_text += idioma + "<<<";
				article_title_text += articleTitle.getText().replaceAll("\n", " ").replaceAll("\\s+", " ");
			}else {
				article_title_text += "es<<<";
			}
//			System.out.println("article_title_text1:::" + article_title_text);
			List<Node> childs_articleTitle = document.selectNodes("/article/front/article-meta/title-group/article-title/*");
			if(childs_articleTitle != null) {
				for(Node child : childs_articleTitle) {
					article_title_text += child.getText().replaceAll("\n ", " ").replaceAll("\\s+", " ");
				}
//				System.out.println("article_title_text2:::" + article_title_text);
			}
			//****************************** Termina obtención de titulo **************************************////////
			
			/////////********* Obteniendo otros titulos del articulo y su contenido de los hijos de otros titulos *****//////////
			List<Node> transTitles = document.selectNodes("/article/front/article-meta/title-group/trans-title-group/trans-title");
			if(transTitles != null) {
				for(Node childTransT : transTitles) {
					article_title_text += "<<<" + childTransT.valueOf("@xml:lang") + "<<<" + childTransT.getText().replaceAll("\n ", " ").replaceAll("\\s+", " ");
					List<Node> chilsTitleTrans = document.selectNodes("/article/front/article-meta/title-group/trans-title-group/trans-title/*");
					if(chilsTitleTrans != null) {
						for(Node childTrans : chilsTitleTrans) {
							article_title_text += childTrans.getText().replaceAll("\n", " ").replaceAll("\\s+", " ");
						}
					}
					
				}
			}
			//****************************** Termina obtención de otros titulos  *********************************////////
			
			List<Node> autores = document.selectNodes("/article/front/article-meta/contrib-group/contrib[@contrib-type='author']/name");
			if(autores != null) {
				Node surname, given_names;
				for(Node autor : autores) {
					given_names = autor.selectSingleNode("given-names");
					surname = autor.selectSingleNode("surname");
					
					autoresAp_text += surname != null ? surname.getText().replaceAll("\n", " ").replaceAll("\\s+", " ") + "<" : "";
					autoresAp_text += given_names != null ? given_names.getText().replaceAll("\n", " ").replaceAll("\\s+", " ") : "";
					
					if(given_names != null) {
						autores_text += given_names.getText().replaceAll("\n", " ").replaceAll("\\s+", " ") + " ";
					}
					if(surname != null) {
						autores_text += surname.getText().replaceAll("\n", " ").replaceAll("\\s+", " ");
					}
					autores_text += "<<<";
					autoresAp_text += "<<<";
				}
			}
			
			Node year = document.selectSingleNode("/article/front/article-meta/pub-date/year");
			if(year != null) {
				fieldsRev.get(6).setExiste(true);
				fieldsRev.get(6).setContenidoField(
						year.getText().trim().replaceAll("\n", " ").replaceAll("\\s+", " "));
			}
			
			Node volume = document.selectSingleNode("/article/front/article-meta/volume");
			if(volume != null) {
				fieldsRev.get(7).setExiste(true);
				fieldsRev.get(7).setContenidoField(
						volume.getText().trim().replaceAll("\n", " ").replaceAll("\\s+", " "));
			}
			
			
			Node issue = document.selectSingleNode("/article/front/article-meta/issue");
			if(issue != null) {
			fieldsRev.get(8).setExiste(true);
			fieldsRev.get(8).setContenidoField(
					issue.getText().trim().replaceAll("\n", " ").replaceAll("\\s+", " "));
			}
			 
			Node fpage = document.selectSingleNode("/article/front/article-meta/fpage");
			if(fpage != null) {
				fieldsRev.get(23).setExiste(true);
				fieldsRev.get(23).setContenidoField(
						fpage.getText().trim());
			}
			
			Node lpage = document.selectSingleNode("/article/front/article-meta/lpage");
			if(lpage != null) {
				fieldsRev.get(24).setExiste(true);
				fieldsRev.get(24).setContenidoField(
						lpage.getText().trim());
			}
			
			Node abstractt = document.selectSingleNode("/article/front/article-meta/abstract");
			if(abstractt != null) {
				String idiomaAbstract = abstractt.valueOf("@xml:lang") != null ? abstractt.valueOf("@xml:lang") : "es";
				idiomaAbstract = !abstractt.valueOf("@xml:lang").isEmpty() ? abstractt.valueOf("@xml:lang") : "es";
				resumen_text +=  idiomaAbstract + ":";
				resumen_text += abstractt.getText().replaceAll("\n", " ").replaceAll("\\s+", " ");
				List<Node> childsAbstract = abstractt.selectNodes("*");
				if(childsAbstract != null) {
					contenido_recursivo = "";
					getContenido(childsAbstract, "abstract");
					resumen_text += " " + contenido_recursivo.replaceAll("\n", " ").replaceAll("\\s+", " ");
//					for(Node childAbs : childsAbstract) {
//						if(!childAbs.getName().equals("title")) {
//							resumen_text += " " + childAbs.getText().replaceAll("\n", " ").replaceAll("\\s+", " ");
//						}
//					}
				}
				resumen_text += "<<<";
			}
			
			List<Node> abstractTrans = document.selectNodes("/article/front/article-meta/trans-abstract");
			if(abstractTrans != null) {
				for(Node abstractT : abstractTrans) {
					String idiomaAbstract = abstractT.valueOf("@xml:lang") != null ? abstractT.valueOf("@xml:lang") : "es";
					idiomaAbstract = !abstractT.valueOf("@xml:lang").isEmpty() ? abstractT.valueOf("@xml:lang") : "es";
					resumen_text +=  idiomaAbstract + ":";
					resumen_text += abstractT.valueOf("@xml:lang") + ":";
					resumen_text += abstractT.getText().replaceAll("\n", " ").replaceAll("\\s+", " ");
					List<Node> nodesabs = abstractT.selectNodes("*");
					if(nodesabs != null) {
						contenido_recursivo = "";
						getContenido(nodesabs, "abstract");
						resumen_text += " " + contenido_recursivo.replaceAll("\n", " ").replaceAll("\\s+", " ");
						
//						for (Node node : nodesabs) {
//							if(!node.getName().equals("title")) {
//							resumen_text += " " + node.getText().replaceAll("\n", " ").replaceAll("\\s+", " ");
//							}
//						}
					}
					resumen_text += "<<<";
				}
			}
			
			List<Node> keywords = document.selectNodes("/article/front/article-meta/kwd-group");
			if(keywords != null) {
				for (Node node : keywords) {
					String idiomaAbstract = node.valueOf("@xml:lang") != null ? node.valueOf("@xml:lang") : "es";
					idiomaAbstract = !node.valueOf("@xml:lang").isEmpty() ? node.valueOf("@xml:lang") : "es";
					keywords_text += idiomaAbstract;
					keywords_text += ":";
					List<Node> kwd = node.selectNodes("kwd");
					if(kwd != null) {
						for (Node node2 : kwd) {
							List<Node> node3 = node2.selectNodes("*");
							
							if(node3 != null) {
								contenido_recursivo = "";
								getContenido(node3, "");
								contenido_recursivo = contenido_recursivo.replaceAll("\n", " ").replaceAll("\\s+", " ");
							}
							keywords_text += (node2.getText() + contenido_recursivo).replaceAll("\n", " ").replaceAll("\\s+", " ") + "<<";
						}
						keywords_text += "<<<";
					}
				}
			}
			
			List<Node> body = document.selectNodes("/article/body/*");
			if(body != null) {
				contenido_recursivo = "";
				getContenido(body, "");
				contenido_recursivo = contenido_recursivo.replaceAll("\n", " ").replaceAll("\\s+", " ");
			}
			fieldsRev.get(10).setExiste(true);
			fieldsRev.get(10).setContenidoField(contenido_recursivo);

		}
		
		////////////************** Si la clave de Revista es valida se obtienen sus datos de la base y se indexa ********////////////////////
		
		if (indizar) {
			fieldsRev.get(5).setExiste(true);
			fieldsRev.get(5).setContenidoField(article_title_text);
			fieldsRev.get(4).setExiste(true);
//			System.out.println("origen::::" + origen + ":redalyc?::" + origen.equals("Redalyc") 
//			+ "::fields:::" + (origen.equals("Redalyc") ? cveArticulo : claveID));
			fieldsRev.get(4).setContenidoField(origen.equals("Redalyc") ? cveArticulo : claveID);
			
			if (!resumen_text.equals("") && !resumen_text.equals(" ") && resumen_text != null) {
				fieldsRev.get(9).setExiste(true);
				if(resumen_text.length() > 4) {
				resumen_text = resumen_text.substring(0, resumen_text.length() - 3);
				fieldsRev.get(9).setContenidoField(resumen_text.replaceAll("\n", "").replaceAll("\\s+", " ")
						.replaceAll(": ", ":").replaceAll(" :", ":"));
				}
				
				
			} else {
				fieldsRev.get(9).setNameField("resumen");
				fieldsRev.get(9).setExiste(false);
			}
			fieldsRev.get(11).setExiste(true);
			fieldsRev.get(11).setContenidoField(autores_text);
			
			fieldsRev.get(25).setExiste(true);
			if(!autoresAp_text.equals("") && !autoresAp_text.equals(" ")) {
				autoresAp_text = autoresAp_text.substring(0, autoresAp_text.length() - 3);
				fieldsRev.get(25).setContenidoField(autoresAp_text);
			}
			

			if (!keywords_text.equals("") && !keywords_text.equals(" ") && keywords_text != null) {
				fieldsRev.get(12).setExiste(true);
				keywords_text = keywords_text.replaceAll("<<<<<", "<<<");
				keywords_text = keywords_text.substring(0, keywords_text.length() - 3);
				fieldsRev.get(12).setContenidoField(keywords_text);
			} else {
				fieldsRev.get(12).setNameField("palClave");
				fieldsRev.get(12).setExiste(false);
			}
			if(origen.equals("Redalyc")) {
				getCamposDBRedalyc(cveArticulo);
			}else {
				getCamposDB();
			}
			indexar();
			log = " ---> XML " + claveID + " con formato valido Indizado <<<<<<";
			// txtarea.setText(txtarea.getText() + (log + "\n"));
			FicheroLog.logger.info(log);
			// txtarea.setText(log);
		} else {
			log = " ---> XML " + claveID + " con formato NO valido NO se ha indizado <<<<<<";
			// txtarea.setText(txtarea.getText() + (log + "\n"));
			FicheroLog.logger.info(log);
		}

	}

	/**
	 * Obtiene los datos de articulos que vienen desde la base de datos
	 */
	public void getCamposDB() {
		ResultSet resultados = null;
		String query = resourceBundle.getString("datosArtdb") + fieldsRev.get(0).getContenidoField() + ";";
		try {
			resultados = consultar.consultar(query, connect);
			if (resultados != null) {
				while (resultados.next()) {
					fieldsRev.get(14).setExiste(true);
					fieldsRev.get(14).setContenidoField(resultados.getString("edorev"));
					fieldsRev.get(15).setExiste(true);
					fieldsRev.get(15).setContenidoField(resultados.getString("cvearea"));
					fieldsRev.get(16).setExiste(true);
					fieldsRev.get(16).setContenidoField(resultados.getString("namearea"));
					fieldsRev.get(17).setExiste(true);
					fieldsRev.get(17).setContenidoField(resultados.getString("cveinst"));
					fieldsRev.get(18).setExiste(true);
					fieldsRev.get(18).setContenidoField(resultados.getString("cvepais"));
					fieldsRev.get(19).setExiste(true);
					fieldsRev.get(19).setContenidoField(resultados.getString("cveidioma"));
					fieldsRev.get(20).setExiste(true);
					fieldsRev.get(20).setContenidoField(resultados.getString("nameidioma"));

				}

			}
			String query2 = resourceBundle.getString("numTerminado") + claveID + "';";
			ResultSet resultados2 = consultar.consultar(query2, connect);
			String imgPortada = "";
			while (resultados2.next()) {
				fieldsRev.get(21).setExiste(true);
				fieldsRev.get(21).setContenidoField(resultados2.getString("cverevnum"));
				try {
					imgPortada = resultados2.getString("nomimgdes").toString();
				} catch (Exception ex) {
					imgPortada = "-";
				}

				if (!imgPortada.equals("-") && !imgPortada.equals(null) && !imgPortada.equals("")) {
					fieldsRev.get(22).setExiste(true);
					fieldsRev.get(22).setContenidoField(imgPortada);
				}
			}

		} catch (Exception ex) {
			// txtarea.setText(txtarea.getText() + "ERROR! Met-do getCamposDB" + ": " +
			// ex.getMessage() + " Indizaci-n cancelada");
			FicheroLog.logger.warning("ERROR! Met-do getCamposDB" + ": " + ex.getMessage() + " Indizaci-n cancelada");
			System.exit(0);
		}

	}
	
	/**
	 * Obtiene los datos de articulos que vienen desde la base de datos
	 */
	public void getCamposDBRedalyc(String cveArticulo) {
		//tarea1
		try {
			ResultSet resultados = null;
			String query1 = resourceBundle.getString("obtCveRevistaRedalyc") + cveArticulo;
//			System.out.println("query1::" + query1);
			resultados = oraclesql.consultar(query1, oracleConnect);
			if (resultados != null) {
				while (resultados.next()) {
					fieldsRev.get(0).setExiste(true);
					fieldsRev.get(0).setContenidoField(resultados.getString("cveentrev"));
//					System.out.println("fieldsRev.get(0):::" + resultados.getString("cveentrev"));
				}
				String query = resourceBundle.getString("datosArtdbRedalyc") + fieldsRev.get(0).getContenidoField();
//				System.out.println("query::" + query);
				resultados = oraclesql.consultar(query, oracleConnect);
				if (resultados != null) {
					while (resultados.next()) {
						fieldsRev.get(14).setExiste(true);
						fieldsRev.get(14).setContenidoField(resultados.getString("edorev"));
						fieldsRev.get(15).setExiste(true);
						fieldsRev.get(15).setContenidoField(resultados.getString("cvearea"));
						fieldsRev.get(16).setExiste(true);
						fieldsRev.get(16).setContenidoField(resultados.getString("namearea"));
						fieldsRev.get(17).setExiste(true);
						fieldsRev.get(17).setContenidoField(resultados.getString("cveinst"));
						fieldsRev.get(18).setExiste(true);
						fieldsRev.get(18).setContenidoField(resultados.getString("cvepais"));
						fieldsRev.get(19).setExiste(true);
						fieldsRev.get(19).setContenidoField(resultados.getString("cveidioma"));
						fieldsRev.get(20).setExiste(true);
						fieldsRev.get(20).setContenidoField(resultados.getString("nameidioma"));
						fieldsRev.get(3).setExiste(true);
						fieldsRev.get(3).setContenidoField(resultados.getString("namePais"));
	
					}
//					System.out.println("edorev::" + fieldsRev.get(14).getContenidoField() +
//							"cvearea::" + fieldsRev.get(15).getContenidoField() +
//							"namearea::" + fieldsRev.get(16).getContenidoField() +
//							"cveinst::" + fieldsRev.get(17).getContenidoField() +
//							"cvepais::" + fieldsRev.get(18).getContenidoField() +
//							"cveidioma::" + fieldsRev.get(19).getContenidoField() +
//							"nameidioma::" + fieldsRev.get(20).getContenidoField() +
//							"pais::" + fieldsRev.get(3).getContenidoField());
				}
				String query2 = resourceBundle.getString("numTerminadoRedalyc") + cveArticulo;
//				System.out.println("query2:::" + query2);
				ResultSet resultados2 = oraclesql.consultar(query2, oracleConnect);
				String imgPortada = "";
				while (resultados2.next()) {
					fieldsRev.get(21).setExiste(true);
					fieldsRev.get(21).setContenidoField(resultados2.getString("cverevnum"));
					try {
						imgPortada = resultados2.getString("nomimgdes").toString();
					} catch (Exception ex) {
						imgPortada = "-";
					}
	
					if (!imgPortada.equals("-") && !imgPortada.equals(null) && !imgPortada.equals("")) {
						fieldsRev.get(22).setExiste(true);
						fieldsRev.get(22).setContenidoField(imgPortada);
					}
				}
//				System.out.println("cverevnum::" + fieldsRev.get(21).getContenidoField() +
//							"nomimgdes::" + fieldsRev.get(22).getContenidoField());
			}
		} catch (Exception ex) {
			// txtarea.setText(txtarea.getText() + "ERROR! Met-do getCamposDB" + ": " +
			// ex.getMessage() + " Indizaci-n cancelada");
			FicheroLog.logger.warning("ERROR! Met-do getCamposDB" + ": " + ex.getMessage() + " Indizaci-n cancelada");
			System.exit(0);
		}

	}

	/**
	 * Obtiene la clave del articulo del el nombre de el XML, para determinar de
	 * igual manera si es valido el formato o no
	 * 
	 * @param path
	 *            Ruta del XML
	 * @return Un String con la clave del articulo obtenido del el nombre del xml
	 */
	public String obtenerClave(String path) {
		String baseName = FilenameUtils.getBaseName(path);
		if (baseName.matches(".*[A-Za-z].*")) {
			return "";
		} else {
			return baseName;
		}
	}


	public Boolean consultNumTerminado(String claveArt) {
		Boolean terminado = false;
		int edojats = 0;
		int revcsg = 0;
		String edoentrev = "0";
		String query = resourceBundle.getString("numTerminado") + claveArt + "';";
		ResultSet resultado = consultar.consultar(query, connect);
		if (resultado != null) {
			try {
				while (resultado.next()) {
					edojats = resultado.getInt("edojats");
					revcsg = resultado.getInt("revcsg");
					edoentrev = resultado.getString("edoentrev");
				}
				if ((edojats == 3 || edojats == 1) && revcsg == 1 && edoentrev.equals("1")) {
					terminado = true;
				} else {
					terminado = false;
				}
			} catch (Exception ex) {
				FicheroLog.logger.warning(
						"ERROR! Metódo consultNumTerminado" + ": " + ex.getMessage() + " Indización cancelada");
				System.exit(0);
			}

		}
		return terminado;
	}
	
	public Boolean articuloESeliminado(String claveArt) {
		Boolean eliminado = false;
		int bndelmart = 0;

		String query = resourceBundle.getString("articuloEliminado") + claveArt + "';";
		ResultSet resultado = consultar.consultar(query, connect);
		if (resultado != null) {
			try {
				while (resultado.next()) {
					bndelmart = resultado.getInt("bndelmart");
				}
				if (bndelmart == 0) {
					eliminado = false;
				} else {
					eliminado = true;
				}
			} catch (Exception ex) {
				FicheroLog.logger.warning(
						"ERROR! Metódo getArticuloEliminado" + ": " + ex.getMessage() + " Indización cancelada");
				System.exit(0);
			}

		}
		return eliminado;
	}
	
	public void getContenido (List<Node> lista, String tipo) {
		if(lista != null) {
			for (Node node : lista) {
				if(tipo.equals("abstract") || tipo.equals("trans-abstract")) {
					if(!node.getName().equals("title")) {
						contenido_recursivo += node.getText();
						List<Node> child = node.selectNodes("*");
						getContenido(child, tipo);
					}
				}else {
					contenido_recursivo += node.getText();
					List<Node> child = node.selectNodes("*");
					getContenido(child, tipo);
				}
				
			}
		}
	}

	public Boolean revistaEnBase(String claveRev) {
		Boolean existe = false;
		String res = "";
		String query = resourceBundle.getString("comprobarRevista") + claveRev + ";";
		ResultSet resultado = consultar.consultar(query, connect);
		if (resultado != null) {
			try {
				while (resultado.next()) {
					res = resultado.getString("cveentrev");
					if(!res.equals("")) {
						existe = true;
					}
				}
			} catch (Exception ex) {
				FicheroLog.logger.warning(
						"ERROR! Metódo revistaEnBase" + ": " + ex.getMessage() + " Comprobacion de Revista Cancelada");
				System.exit(0);
			}

		}
		return existe;
	}

	/**
	 * Indiza el documento creado dentro del -ndice
	 */
	public void indexar() {
		try {
			generatedcont = generatedcont + 1;
			writoo.addDocument(indexDoc.indizarDocumento(fieldsRev));
			writoo.commit();
		} catch (Exception ex) {
			// txtarea.setText(txtarea.getText() + "ERROR! Met-do indexar" + ": " +
			// ex.getMessage() + " Indizaci-n cancelada");
			FicheroLog.logger.warning("ERROR! Met-do indexar" + ": " + ex.getMessage() + " Indizaci-n cancelada");
			System.exit(0);
		}
	}

	/**
	 * Crea el modelo del documento que sera indexado
	 * 
	 * @return Una lista con los campos creados y su contenido inicializado
	 */
	public ArrayList<ModeloField> startFielsRev() {
		fieldsRev.add(0, new ModeloField("claveRevista", "", false, 0));
		fieldsRev.add(1, new ModeloField("nombreRevista", "", false, 0));
		fieldsRev.add(2, new ModeloField("institucion", "", false, 0));
		fieldsRev.add(3, new ModeloField("pais", "", false, 0));
		fieldsRev.add(4, new ModeloField("claveArt", "", false, 0));
		fieldsRev.add(5, new ModeloField("tituloArt", "", false, 0));
		fieldsRev.add(6, new ModeloField("anio", "", false, 0));
		fieldsRev.add(7, new ModeloField("volumen", "", false, 0));
		fieldsRev.add(8, new ModeloField("numero", "", false, 0));
		fieldsRev.add(9, new ModeloField("resumen", "", false, 0));
		fieldsRev.add(10, new ModeloField("contenido", "", false, 0));
		fieldsRev.add(11, new ModeloField("autores", "", false, 0));
		fieldsRev.add(12, new ModeloField("palClave", "", false, 0));
		fieldsRev.add(13, new ModeloField("doi", "", false, 0));
		fieldsRev.add(14, new ModeloField("edoRev", "", false, 0));
		fieldsRev.add(15, new ModeloField("cveArea", "", false, 0));
		fieldsRev.add(16, new ModeloField("area", "", false, 0));
		fieldsRev.add(17, new ModeloField("cveInst", "", false, 0));
		fieldsRev.add(18, new ModeloField("cvePais", "", false, 0));
		fieldsRev.add(19, new ModeloField("cveIdioma", "", false, 0));
		fieldsRev.add(20, new ModeloField("idioma", "", false, 0));
		fieldsRev.add(21, new ModeloField("cveNumero", "", false, 0));
		fieldsRev.add(22, new ModeloField("imgPortada", "", false, 0));
		fieldsRev.add(23, new ModeloField("fpage", "", false, 0));
		fieldsRev.add(24, new ModeloField("lpage", "", false, 0));
		fieldsRev.add(25, new ModeloField("autoresCitation", "", false, 0));
		fieldsRev.add(26, new ModeloField("issn", "", false, 0));
		fieldsRev.add(27, new ModeloField("issne", "", false, 0));
		fieldsRev.add(28, new ModeloField("origen", "", false, 0));//tarea1 agregar el campo origen
		return fieldsRev;
	}

	/**
	 * Crear un CharArraySet con las estopwords que ser-n utilizadas por el
	 * analizador
	 * 
	 * @return Una lista de tipo CharArraySet que contiene las stopwords
	 */
	public CharArraySet stopwords() {
		CharArraySet stopSet = null;
		try {
			stopSet = CharArraySet.copy(StandardAnalyzer.STOP_WORDS_SET);
			BufferedReader bufferedReader = new BufferedReader(new FileReader(resourceBundle.getString("stopwords")));
			String palabraStopWord;

			while ((palabraStopWord = bufferedReader.readLine()) != null) {
				stopSet.add(palabraStopWord);
			}
			bufferedReader.close();
		} catch (Exception ex) {

		}
		return stopSet;
	}

}
