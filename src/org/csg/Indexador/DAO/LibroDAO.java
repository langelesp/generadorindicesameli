package org.csg.Indexador.DAO;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.nio.file.Paths;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import org.apache.commons.io.FilenameUtils;
import org.apache.lucene.analysis.CharArraySet;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.csg.Indexador.DocumentoIndex;
import org.csg.Indexador.FicheroLog;
import org.csg.Indexador.Modelo.ModeloField;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.XPath;
import org.dom4j.io.SAXReader;

import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class LibroDAO extends Thread{
	
	private StandardAnalyzer analyzer;
	private Directory index;
	private IndexWriterConfig config;
	private IndexWriter writoo;
	private Directory directoryA;
	private DocumentoIndex indexDoc = new DocumentoIndex();
	private FicheroLog ficherolog = new FicheroLog();
	private ResourceBundle resourceBundle = null;
	private ResourceBundle bundlePaths = null;
	
	private String log = "";
	private String rutaIndice = "";
	private String claveArticulo = "";
	private String idsContribs = "";
	private String progreso = "";
	
	private ArrayList<ModeloField> fieldsLibros = new ArrayList<>();
	private ArrayList<String> filesXML = new ArrayList<>();
	private ArrayList<Object> componentes = new ArrayList<>();
	
	public LibroDAO(){
	}
	
	public LibroDAO(String rutaIndex, ArrayList<String> filesXML, ArrayList<Object> componentes) {
		this.rutaIndice = rutaIndex;
		this.filesXML = filesXML;
		this.componentes = componentes;
		this.ficherolog.crearLog(rutaIndice);
		this.bundlePaths = ResourceBundle.getBundle("paths");
		this.resourceBundle = ResourceBundle.getBundle("querys");
		try {
			this.directoryA = FSDirectory.open(Paths.get(bundlePaths.getString("rutaDefaultIndexArt")));
			this.analyzer = new StandardAnalyzer(stopwords());
			this.index = FSDirectory.open(Paths.get(rutaIndice));
			this.config = new IndexWriterConfig(analyzer);
			this.writoo = new IndexWriter(index, config);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	@Override
	public void run() {
		log = ">>>>>>  Inicia la lectura de los XMLs <<<<<<";
		FicheroLog.logger.info(log);
		String rutaLog = FicheroLog.fechaS;
		Boolean parseStatus = true;
		
		int total = filesXML.size();
		double progress = 0;
		double aux = 0;
		
		try {
			for (int i = 0; i < filesXML.size(); i++) {
				parseStatus = true;
				startFielsLibros();
				//System.out.println("*************************" + filesXML.get(i) + "***********************");
				getDatosLibrosXML(filesXML.get(i));
				fieldsLibros.clear();
				
				aux = ((i + 1) * 1);
				progress = (aux / total);
				progreso = String.valueOf(progress * 100);
				progreso = progreso.substring(0, progreso.indexOf('.'));
				((TextField) componentes.get(8)).setText(progreso + "%");
				((ProgressBar) componentes.get(6)).setProgress(progress);
			}
			log = "\n|************ Se indizaron: " + writoo.numDocs() + " Libros **********|";
			((TextArea) componentes.get(5)).setText("************ Se indizaron: " + writoo.numDocs() + " Libros **********");
			FicheroLog.logger.info(log);
			writoo.close();
			FicheroLog.fh.close();
		}
		catch (Exception ex) {
			FicheroLog.logger.warning("\n************ ERROR de acceso a los XML's. Indexación CANCELADA **********");
			((TextArea) componentes.get(5)).setText("************ ERROR de acceso a los XML's. Indexación CANCELADA **********");
			System.out.println("->"+ex);
		}
		
		((RadioButton) componentes.get(0)).setDisable(false);
		((RadioButton) componentes.get(1)).setDisable(false);
		((RadioButton) componentes.get(10)).setDisable(false);
		((RadioButton) componentes.get(11)).setDisable(false);
		((RadioButton) componentes.get(13)).setDisable(false);
		
		((Button) componentes.get(2)).setDisable(false);
	}
	
	public void getDatosLibrosXML(String rutaXML) {
		String aux="";
		try {
			File inputFile = new File(rutaXML);
			aux=inputFile.getPath();
			SAXReader reader = new SAXReader();
			Document document = reader.read(inputFile);
			
			System.out.println("\n"+inputFile.getPath()+"\n");
			Map<String, String> uris = new HashMap<String, String>();
			uris.put("oai", "http://www.openarchives.org/OAI/2.0/");
			uris.put("oai_dc", "http://www.openarchives.org/OAI/2.0/oai_dc/");
			XPath xpath = document.createXPath("/oai:OAI-PMH/oai:ListRecords/oai:record/oai:metadata/oai_dc:dc");
			xpath.setNamespaceURIs(uris);
			List<Node> nodos = xpath.selectNodes(document);
			
			if (nodos.isEmpty()) {
				FicheroLog.logger.info("\n>>>>> ** El documento no contiene datos ** <<<<<<");
			}
			else {
				for (Node nodo: nodos) {
					Node tipoDocumento = nodo.selectSingleNode("dc:type[last()]");
					if(tipoDocumento!= null) {
						//quizá agregar validación en caso de no existir title;
						if (tipoDocumento.getText().toLowerCase().equals("libro") || tipoDocumento.getText().toLowerCase().equals("book")) {
							fieldsLibros.get(0).setContenidoField(nodo.selectSingleNode("dc:title").getText());
							fieldsLibros.get(1).setContenidoField(nodo.selectSingleNode("dc:identifier[last()]").getText());
							System.out.println("titulo: " + fieldsLibros.get(0).getContenidoField());
							//System.out.println("link: " + fieldsLibros.get(1).getContenidoField());
							if (fieldsLibros.get(1).getContenidoField().contains("clacso")) {
								fieldsLibros.get(2).setContenidoField("CLACSO");
							}
							else if (fieldsLibros.get(1).getContenidoField().contains("hdl.handle")) {
								fieldsLibros.get(2).setContenidoField("UAEM");
							}else {
								continue;
							}
							//System.out.println("Fuente: " + fieldsLibros.get(2).getContenidoField());
							//agregue cambios 17-12-19 150721005
							//tipoDocumento = nodo.selectSingleNode("dc:type[last()]");
							//if(nodo.selectSingleNode("dc:creator").getText()!=null)
							if(nodo.selectSingleNode("dc:creator")!=null)
								fieldsLibros.get(3).setContenidoField(nodo.selectNodes("dc:creator").stream().map(n ->String.valueOf(n.getText())).collect(Collectors.joining("<<<")));
							if(nodo.selectSingleNode("dc:subject")!=null)
								fieldsLibros.get(4).setContenidoField(nodo.selectNodes("dc:subject").stream().map(n ->String.valueOf(n.getText())).collect(Collectors.joining("<<<")));
							if(nodo.selectSingleNode("dc:description")!=null)
								fieldsLibros.get(5).setContenidoField(nodo.selectSingleNode("dc:description").getText());
							if(nodo.selectSingleNode("dc:date")!=null)
								fieldsLibros.get(6).setContenidoField(nodo.selectSingleNode("dc:date").getText());
							if(nodo.selectSingleNode("dc:language")!=null)
								fieldsLibros.get(7).setContenidoField(nodo.selectSingleNode("dc:language").getText());
							if(nodo.selectSingleNode("dc:publisher")!=null)
								fieldsLibros.get(8).setContenidoField(nodo.selectSingleNode("dc:publisher").getText());
							//http://portal.amelica.org/coleccionLibros.oa?pal=
							indexar();
						}
					}
				}
			}
		}
		catch (Exception ex) {
			FicheroLog.logger.warning(">>>> ** Ocurrio un error mientras se obtenian los datos"
					+ " del XML método getDatosLibrosXML** <<<< -> "+aux);
			System.out.println(aux+"<-> "+ ex);
			ex.printStackTrace();
		}
	}
	
	public void indexar() {
		try {
			writoo.addDocument(indexDoc.indizarDocumento(fieldsLibros));
			writoo.commit();
		} catch (Exception ex) {
			FicheroLog.logger.warning("ERROR! Metodo Indexar" + ": " + ex.getMessage() + " Indización cancelada");
			System.exit(0);
		}
	}
	
	public ArrayList<ModeloField> startFielsLibros() {
		fieldsLibros.add(0, new ModeloField("titulo", "", true, -1));
		fieldsLibros.add(1, new ModeloField("link", "", true, -1));
		fieldsLibros.add(2, new ModeloField("fuente", "", true, -1));
		
		//agregué estos cambios 17-12-19
		fieldsLibros.add(3, new ModeloField("autores", "", true, -1));
		fieldsLibros.add(4, new ModeloField("palabrasClave", "", true, -1));
		fieldsLibros.add(5, new ModeloField("descripcion", "", true, -1));
		fieldsLibros.add(6, new ModeloField("anio", "", true, -1));
		fieldsLibros.add(7, new ModeloField("lenguaje", "", true, -1));
		fieldsLibros.add(8, new ModeloField("editor", "", true, -1));

		return fieldsLibros;
	}
	
	public CharArraySet stopwords() {
		CharArraySet stopSet = null;
		try {
			stopSet = CharArraySet.copy(StandardAnalyzer.STOP_WORDS_SET);
			BufferedReader bufferedReader = new BufferedReader(new FileReader(bundlePaths.getString("stopwords")));
			String palabraStopWord;

			while ((palabraStopWord = bufferedReader.readLine()) != null) {
				stopSet.add(palabraStopWord);
			}
			bufferedReader.close();
		} catch (Exception ex) {

		}
		return stopSet;
	}
}
