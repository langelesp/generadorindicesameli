package org.csg.Indexador.DAO;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.ResourceBundle;

import org.apache.lucene.analysis.CharArraySet;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.csg.ConectionPostgreSQL.PostgreSQLJDBC;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;

public class ProcesoLlenarTablaPalClave extends Thread{

	Directory directoryA;
	StandardAnalyzer analyzer;
	IndexSearcher searcher;
	ResourceBundle resourceBundle = null;
	ScoreDoc[] hits = null;

	PostgreSQLJDBC postgresql = new PostgreSQLJDBC();
	private Connection connect = null;
	ResultSet resulSet = null;
	ArrayList<Object> componentes = new ArrayList<>();

	public ProcesoLlenarTablaPalClave(String pathArt) {
		try {
			resourceBundle = ResourceBundle.getBundle("querys");
			directoryA = FSDirectory.open(Paths.get(pathArt));
			analyzer = new StandardAnalyzer(stopwords());
			this.connect = postgresql.conectar();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public ProcesoLlenarTablaPalClave(String pathArt, ArrayList<Object> componentes) {
		try {
			resourceBundle = ResourceBundle.getBundle("querys");
			directoryA = FSDirectory.open(Paths.get(pathArt));
			analyzer = new StandardAnalyzer(stopwords());
			this.connect = postgresql.conectar();
			this.componentes = componentes;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void run() {
		postgresql.deleteRowsPalcve(connect);
		postgresql.deleteRowsPalCveFil(connect);
		((TextArea) componentes.get(5)).setText("Llenando tabla.....");
		obtenerPalabrasDesdeIndice();
		postgresql.llenarTblPalcveFil(connect);
		postgresql.closeConnect(connect);
		((TextArea) componentes.get(5)).setText("Se ha llenado la Tabla! Verificar...");
		
		((RadioButton) componentes.get(0)).setDisable(false);
		((RadioButton) componentes.get(1)).setDisable(false);
		((RadioButton) componentes.get(10)).setDisable(false);
		((RadioButton) componentes.get(11)).setDisable(false);
		((RadioButton) componentes.get(13)).setDisable(false);
		
		((Button) componentes.get(2)).setDisable(false);
		((Label) componentes.get(3)).setText("");
	}

	public void obtenerPalabrasDesdeIndice() {
		Query qu = null;
		String palabras = "";
		String claveArt = "";
		try {
			try {
				qu = new QueryParser(" ", analyzer).parse("claveArt:[0 TO 9]");
			} catch (ParseException e) {

				e.printStackTrace();
			}
			int hitsPerPage = 10000;
			IndexReader reader;
			reader = DirectoryReader.open(directoryA);
			IndexSearcher searcher = new IndexSearcher(reader);
			TopDocs docs = searcher.search(qu, hitsPerPage);
			ScoreDoc[] hits = docs.scoreDocs;
			for (int i = 0; i < hits.length; i++) {
				int docId = hits[i].doc;
				Document doc = searcher.doc(docId);
				palabras = doc.get("palClave");
				claveArt = doc.get("claveArt");
				if(palabras != "" && palabras != " ") {
					separarPalabrasClave(palabras , claveArt);
				}
			}
			reader.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public void separarPalabrasClave(String palabras, String claveArt){
		
		String palIdiomas[] = palabras.split("<<<");
		String palabraClave = "";
		int posDosPuntos = 0;
		int pos = 0;
		
		for(int i = 0; i < palIdiomas.length; i++){
			String palClave[] = palIdiomas[i].split("<<");
			for(int j = 0; j < palClave.length; j++){
				if(j == 0) {
					if(palClave[0].contains(":")) {
						posDosPuntos = palClave[0].indexOf(":");
						palabraClave = palClave[0].substring((posDosPuntos+1), palClave[0].length());
						palabraClave = palabraClave.trim().replaceAll("\n", " ").replaceAll("\\s+", " ");
						pos = obtenerUltimaInsercion();
						postgresql.insertPalClave(connect,pos, claveArt, palabraClave);
						System.out.println(palabraClave);
					}
				}else {
					palabraClave = palClave[j];
					palabraClave = palabraClave.trim().replaceAll("\n", " ").replaceAll("\\s+", " ");
					pos = obtenerUltimaInsercion();
					postgresql.insertPalClave(connect, pos, claveArt, palabraClave);
					System.out.println(palabraClave);
				}
			}
		}
		
	}
	
	public int obtenerUltimaInsercion(){
		int ultimo = 0;
		String query = "select * from (select ID from tblpalcve order by ID desc) as a limit 1";
		resulSet = postgresql.consultar(query, connect);
		try {
			if (resulSet != null) {
				while (resulSet.next()) {
					ultimo = resulSet.getInt("id");
				}
			}
		} catch (Exception ex) {

		}
		ultimo = ultimo + 1;
		return ultimo;
	}
	

	public CharArraySet stopwords() {
		CharArraySet stopSet = null;
		try {
			stopSet = CharArraySet.copy(StandardAnalyzer.STOP_WORDS_SET);
			BufferedReader bufferedReader = new BufferedReader(new FileReader(resourceBundle.getString("stopwords")));
			String palabraStopWord;

			while ((palabraStopWord = bufferedReader.readLine()) != null) {
				stopSet.add(palabraStopWord);
			}
			bufferedReader.close();
		} catch (Exception ex) {

		}
		return stopSet;
	}

}
