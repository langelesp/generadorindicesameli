package org.csg.ConnectionOracle;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

public class OracleSQLJDBC {
	private Statement stmt;
	ResourceBundle resourceBundle = ResourceBundle.getBundle("paths");
	ResourceBundle querysProp = ResourceBundle.getBundle("querys");
	
	public Connection connection() {
			Connection connection = null;
		try {
			Class.forName(resourceBundle.getString("OracleSQLJDBC"));
			connection = DriverManager.getConnection(resourceBundle.getString("connectionOracleSQLJDBC"),
			resourceBundle.getString("nameOracleSQLJDBC"), resourceBundle.getString("passOracleSQLJDBC"));
			connection.setAutoCommit(true);
			stmt = connection.createStatement();
		} catch (SQLException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return connection; 
	}
	
	public void consultapre(Connection connection){
		try {
		ResultSet rs = consultar("select * from atlas where atlas.id < 39", connection);
		if(rs!= null) {
			while(rs.next()) {
				System.out.println("id---" + rs.getString("ID") + "---nombre--" + rs.getString("NOMBRE") 
				+ "--UNIVERSO--" +  rs.getString("UNIVERSO"));
			}
		}
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Realiza una consulta a la base de datos
	 * 
	 * @param query
	 *            Cadena que contiene la consulta a realizar
	 * @param connect
	 *            Conexi�n realizada a la base de datos
	 * @return Variable de tipo ResulSet, que almacena el resultado obtenido de la
	 *         consulta
	 */
	public ResultSet consultar(String query, Connection connect) {
		ResultSet rs = null;
		try {
			
			rs = stmt.executeQuery(query);

		} catch (Exception ex) {
			System.err.println(ex.getMessage());
		}
		return rs;
	}

	/**
	 * Cierra una conexi�n realizada a la base de datos
	 * 
	 * @param connect
	 *            Indica la conexion que ser� cerrada
	 */
	public void closeConnect(Connection connect) {
		try {
			this.stmt.close();
			connect.close();
			System.out.println("Se cerro la conexión a la base");
		} catch (Exception ex) {
		}

	}
}
