package org.csg.ConectionPostgreSQL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

/**
 * Clase que realiza la conexi�n a la base de datos postgreSQL
 */
public class PostgreSQLJDBC {

	private Statement stmt = null;
	ResourceBundle resourceBundle = ResourceBundle.getBundle("paths");
	ResourceBundle querysProp = ResourceBundle.getBundle("querys");

	public PostgreSQLJDBC() {

	}

	/**
	 * Realiza la conexi�n a la base de datos, los datos como nombre del host,
	 * puerto, nombre de usuario y contrase�a se especifican en el archivo
	 * paths.properties
	 * 
	 * @return Variable de tipo Connect, que almacena una conexi�n a la base
	 */
	public Connection conectar() {
		Connection connect = null;
		try {
			Class.forName(resourceBundle.getString("PostgreSQLJDBC"));
			connect = DriverManager.getConnection(resourceBundle.getString("connectionPostgreSQLJDBC"),
					resourceBundle.getString("namePostgreSQLJDBC"), resourceBundle.getString("passPostgreSQLJDBC"));
			connect.setAutoCommit(true);
		} catch (Exception ex) {
		}
		return connect;
	}
	
	public void deleteRowsPalcve(Connection connect) {
        PreparedStatement st;
        try {
        	st = connect.prepareStatement(querysProp.getString("deleteRowsPalcve"));
            st.executeUpdate();
            st.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }
	public void deleteRowsPalCveFil(Connection connect) {
        PreparedStatement st;
        try {
        	st = connect.prepareStatement(querysProp.getString("deleteRowsPalCveFil"));
            st.executeUpdate();
            st.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

	public void insertPalClave(Connection connect, int id, String claveArt, String palClave) {
		PreparedStatement st;
		try {
			st = connect.prepareStatement(
					querysProp.getString("insertPalClave"));
			st.setInt(1, id);
			st.setString(2, claveArt);
			st.setString(3, palClave);
			st.executeUpdate();
			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void llenarTblPalcveFil(Connection connect) {
        PreparedStatement st;
        try {
        	st = connect.prepareStatement(querysProp.getString("llenarTblPalcveFil"));
            st.executeUpdate();
            st.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

	/**
	 * Realiza una consulta a la base de datos
	 * 
	 * @param query
	 *            Cadena que contiene la consulta a realizar
	 * @param connect
	 *            Conexi�n realizada a la base de datos
	 * @return Variable de tipo ResulSet, que almacena el resultado obtenido de la
	 *         consulta
	 */
	public ResultSet consultar(String query, Connection connect) {
		ResultSet rs = null;
		try {
			stmt = connect.createStatement();
			rs = stmt.executeQuery(query);

		} catch (Exception ex) {
			System.err.println(ex.getMessage());
		}
		return rs;
	}

	/**
	 * Cierra una conexi�n realizada a la base de datos
	 * 
	 * @param connect
	 *            Indica la conexion que ser� cerrada
	 */
	public void closeConnect(Connection connect) {
		try {
			this.stmt.close();
			connect.close();
			System.out.println("Se cerro la conexión a la base");
		} catch (Exception ex) {
		}

	}

}
