package application;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.fxml.FXMLLoader;

/**
 * Clase que contiene el m�todo principal. Esta clase hereda del tipo
 * <code>Application</code> de las aplicaciones de <code>JavaFX</code>.
 * 
 * Esta clase muestra la escena principal de la aplicaci�n, esta se indica en el
 * m�todo <code>start(Stage primaryStage)</code> ya que para todas las
 * aplicaciones JavaFX este m�todo representa el punto de entrada a la
 * aplicaci�n.
 * 
 * Los recursos mostrados en la escena principal se cargan a esta clase
 * indicando el archivo <code>fxml</code> que lo contiene, p. ej.
 * <code>FXMLLoader.load(getClass().getResource("VistaIndexadorCSG.fxml"));</code>
 * 
 * Las funciones que realizan todos los componentes de la vista se encuentan
 * indicados en la clase controlador
 * 
 * @see application.ControllerIndexadorCSG
 * @see application.VistaIndexadorCSG
 * 
 * 
 * @author Usuario
 *
 */
public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
			
			BorderPane root = (BorderPane) FXMLLoader.load(getClass().getResource("VistaIndexadorCSG.fxml"));
			Scene scene = new Scene(root, 958, 644);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setResizable(false);
			primaryStage.setScene(scene);
			primaryStage.show();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		launch(args);
	}
}
