package application;

import java.io.File;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.ResourceBundle;

import org.apache.commons.io.FilenameUtils;
import org.csg.ConectionPostgreSQL.PostgreSQLJDBC;
import org.csg.Indexador.DAO.ArticuloDAO;
import org.csg.Indexador.DAO.ArticuloSDAO;
import org.csg.Indexador.DAO.AutoresDAO;
import org.csg.Indexador.DAO.LibroDAO;
import org.csg.Indexador.DAO.ListaFilesXML;
import org.csg.Indexador.DAO.ProcesoLlenarTablaPalClave;
import org.csg.Indexador.DAO.RevistaDAO;
import org.csg.Indexador.DAO.jsonColeccionRevistas;
import org.csg.Indexador.Modelo.ModeloField;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;

/**
 * Clase que act�a como intermediario entre las clases de acceso a datos (DAO) y
 * la Vista, gestionando el flujo de informaci�n entre ellos y las
 * transformaciones para adaptar los datos a las necesidades de cada uno.
 * 
 * @author Usuario
 *
 */
public class ControllerIndexadorCSG extends Thread {

	PostgreSQLJDBC postgresql = new PostgreSQLJDBC();
	private Connection connect = null;
	ResourceBundle resbundle = ResourceBundle.getBundle("paths");
	ArrayList<String> cvesRevs = new ArrayList<>();
	ArrayList<ModeloField> fieldsRev = new ArrayList<>();
	ArrayList<String> filesXML = new ArrayList<>();
	ArrayList<Object> componentes = new ArrayList<>();
	ListaFilesXML listarxml = new ListaFilesXML();
	String rutaIndex = "";
	String rutaJatsRepo = "";
	@FXML
	RadioButton radioArt = new RadioButton();
	@FXML
	RadioButton radioRev = new RadioButton();
	@FXML
	RadioButton radioJson = new RadioButton();
	@FXML
	RadioButton radioLLenadoTabla = new RadioButton();
	@FXML
	RadioButton radioFAutor = new RadioButton();
	@FXML
	RadioButton radioLibros = new RadioButton();
	@FXML
	Button btnGenerar = new Button();
	@FXML
	Button btnValidarRutas = new Button();
	@FXML
	Label lblValConex = new Label();
	@FXML
	Label lblMensajeRutaRepo = new Label();
	@FXML
	Label lblRutaRepo = new Label();
	@FXML
	Label lblRutaindex = new Label();
	@FXML
	TextField txtJatsRepo = new TextField();
	@FXML
	TextField txtRutaIndex = new TextField();
	@FXML
	TextArea txtArea = new TextArea();
	@FXML
	ProgressBar pbProgress = new ProgressBar();
	@FXML
	TextField fieldprogress = new TextField();
	@FXML
	TextArea txtLog = new TextArea("Log -> ");
	// @FXML
	// ProgressIndicator waitMe = new ProgressIndicator();

	/**
	 * Al seleccionar el <code>RadioButton</code> de Art�culos, este m�todo asigna a
	 * los TextFiels correspondientes las rutas por default del JatsRepo y del
	 * índice, as� mismo valida que el <code>RadioButton 'radioRev'</code> no se
	 * encuentre activo
	 */
	@FXML
	public void activarRBArt() {
		if (radioArt.isSelected()) {
			radioRev.setSelected(false);
			radioJson.setSelected(false);
			radioFAutor.setSelected(false);
			radioLibros.setSelected(false);
			radioLLenadoTabla.setSelected(false);
			txtJatsRepo.setDisable(false);
			txtRutaIndex.setDisable(false);
			lblValConex.setText("");
			lblMensajeRutaRepo.setText("Ruta jatsRepo");
			txtJatsRepo.setPromptText(resbundle.getString("rutaDefaultJR"));
			txtRutaIndex.setText("");
			txtRutaIndex.setPromptText(resbundle.getString("rutaDefaultIndexArt"));
			asignarDefault(3, true);
			asignarDefault(4, false);
			txtArea.setText(" ");
		}
	}

	/**
	 * Al seleccionar el <code>RadioButton</code> de Revistas, este m�todo asigna al
	 * <code>TextField</code> correspondiente la ruta por default del �ndice, as�
	 * mismo desactiva el <code>TextField</code> de la ruta de JatsRepo, ya que en
	 * este caso no sera utilizada y valida que el
	 * <code>RadioButton 'radioArt'</code> no se encuentre activo
	 */
	@FXML
	public void activarRBRev() {
		if (radioRev.isSelected()) {
			radioArt.setSelected(false);
			radioJson.setSelected(false);
			radioFAutor.setSelected(false);
			radioLibros.setSelected(false);
			radioLLenadoTabla.setSelected(false);
			txtJatsRepo.setDisable(true);
			txtRutaIndex.setDisable(false);
			lblValConex.setText("");
			lblMensajeRutaRepo.setText("Ruta jatsRepo");
			pbProgress.setProgress(0);
			txtLog.setText("");
			txtJatsRepo.setPromptText(" ");
			txtJatsRepo.setText("");
			asignarDefault(3, true);
			asignarDefault(4, false);
			txtRutaIndex.setText("");
			txtRutaIndex.setPromptText(resbundle.getString("rutaDefaultIndexRev"));
			txtArea.setText(" ");
		}
	}

	@FXML
	public void activarRBJson() {
		if (radioJson.isSelected()) {
			radioArt.setSelected(false);
			radioRev.setSelected(false);
			radioFAutor.setSelected(false);
			radioLibros.setSelected(false);
			radioLLenadoTabla.setSelected(false);
			txtJatsRepo.setDisable(true);
			txtRutaIndex.setDisable(true);
			lblValConex.setText("");
			lblMensajeRutaRepo.setText("Ruta jatsRepo");
			pbProgress.setProgress(0);
			txtLog.setText("");
			txtJatsRepo.setPromptText(" ");
			txtJatsRepo.setText("");
			txtRutaIndex.setPromptText(" ");
			txtRutaIndex.setText(" ");
			asignarDefault(4, false);
			txtArea.setText("ADVERTENCIA!"
					+ "\nPara la generación de los Json, los índices de Revistas y Artículos deben estar actualizados"
					+ "\nSe recomienda generar primero los mencionados anteriormente..."
					+ "\nSi ya se han generado, se recomienda que ambos índices se encuentren en su ruta predeterminada."
					+ "\nLa ruta de almacenamiento de los Jsons será en contenido estatico de CSG. "
					+ "\nA continuación presiona el boton ->Validar<- para establecer una conexion a la base de datos"
					+ "\n y posteriormente selecciona ->Generar<- para comenzar con el proceso");
		}
	}

	@FXML
	public void activarRBLlenadoTablas() {
		if (radioLLenadoTabla.isSelected()) {
			radioArt.setSelected(false);
			radioRev.setSelected(false);
			radioJson.setSelected(false);
			radioFAutor.setSelected(false);
			radioLibros.setSelected(false);
			txtJatsRepo.setDisable(true);
			txtRutaIndex.setDisable(true);
			lblValConex.setText("");
			lblMensajeRutaRepo.setText("Ruta jatsRepo");
			pbProgress.setProgress(0);
			txtLog.setText("");
			txtJatsRepo.setPromptText(" ");
			txtJatsRepo.setText("");
			txtRutaIndex.setPromptText(" ");
			txtRutaIndex.setText(" ");
			asignarDefault(3, true);
			asignarDefault(4, false);
			txtArea.setText("ADVERTENCIA!"
					+ "\nPara el llenado de la tabla, el índice de articulos debe de estar actualizado"
					+ "\nSe recomienda generar primero el anteriormente mencionado..."
					+ "\nSi ya se ha generado, se recomienda que el índice se encuentre en su ruta predeterminada."
					+ "\nA continuación presiona el boton ->Validar<- para establecer una conexion a la base de datos"
					+ "\n y posteriormente selecciona ->Generar<- para comenzar con el proceso");
		}
	}

	@FXML
	public void activarRBFautor() {
		if (radioFAutor.isSelected()) {
			radioRev.setSelected(false);
			radioArt.setSelected(false);
			radioJson.setSelected(false);
			radioLibros.setSelected(false);
			radioLLenadoTabla.setSelected(false);
			txtJatsRepo.setDisable(false);
			lblValConex.setText("");
			lblMensajeRutaRepo.setText("Ruta jatsRepo");
			pbProgress.setProgress(0);
			txtLog.setText("");
			txtJatsRepo.setText("");
			txtRutaIndex.setDisable(false);
			txtRutaIndex.setText("");
			asignarDefault(3, true);
			asignarDefault(4, false);
			txtRutaIndex.setPromptText(resbundle.getString("rutaDefaultIndexFAutor"));
			txtJatsRepo.setPromptText(resbundle.getString("rutaDefaultJR"));
			txtArea.setText("ADVERTENCIA!"
					+ "\nPara el llenado de la tabla, los índices de articulos y revistas deben de estar actualizados"
					+ "\nSe recomienda generar primero los anteriormentes mencionados..."
					+ "\nSi ya se han generado, se recomienda que los índices se encuentre en su ruta predeterminada."
					+ "\nA continuación presiona el boton ->Validar<- para establecer una conexion a la base de datos"
					+ "\n y posteriormente seleccionar ->Generar<- para comenzar con el proceso");
		}
	}
	
	@FXML
	public void activarRBlibros() {
		if (radioLibros.isSelected()) {
			System.out.println("se activa el radio button para índice de libros");
			radioRev.setSelected(false);
			radioArt.setSelected(false);
			radioJson.setSelected(false);
			radioLLenadoTabla.setSelected(false);
			lblValConex.setText("");
			lblMensajeRutaRepo.setText("Ruta XML's");
			pbProgress.setProgress(0);
			txtLog.setText("");
			txtJatsRepo.setDisable(false);
			txtJatsRepo.setText("");
			txtRutaIndex.setDisable(false);
			txtRutaIndex.setText("");
			asignarDefault(3, true);
			asignarDefault(4, false);
			txtJatsRepo.setPromptText(resbundle.getString("rutaDefaultXMLlibros"));
			txtRutaIndex.setPromptText(resbundle.getString("rutaDefaultIndexLibros"));			
			txtArea.setText("");
		}
	}

	/**
	 * Al dar click sobre el <code>Button 'Validar'</code> se inicia con el proceso
	 * de validaci�n de rutas ingresadas. <br>
	 * <br>
	 * De acuerdo a las validaciones realizadas se determinara si el
	 * <code>Button 'Generar'</code> sera activado. Entre estas validaciones estan
	 * que la ruta indicada de jatsRepo contenga XMLs, y que la conexi�n a la base
	 * de datos este activa y no sea <code>null</code>
	 * 
	 * @see application.ControllerIndexadorCSG#validarRutas(String, TextField,
	 *      Label, String)
	 * @see application.ControllerIndexadorCSG#validarConexionDB()
	 * @see application.ControllerIndexadorCSG#listarXMLs(String)
	 */

	@FXML
	public void accionbtnValidar() {
		filesXML.clear();
		txtArea.clear();
		txtLog.clear();
		pbProgress.setProgress(0);
		fieldprogress.clear();
		Boolean continuar = false;
		try {
			if (radioRev.isSelected() || radioArt.isSelected() || radioFAutor.isSelected() || radioLibros.isSelected()) {
				if (radioRev.isSelected()) {
					rutaIndex = validarRutas(txtRutaIndex.getText(), txtRutaIndex, lblRutaindex,
							resbundle.getString("rutaDefaultIndexRev"));
				} else if (radioFAutor.isSelected()) {
					rutaJatsRepo = validarRutas(txtJatsRepo.getText(), txtJatsRepo, lblRutaRepo,
							resbundle.getString("rutaDefaultJR"));
					rutaIndex = validarRutas(txtRutaIndex.getText(), txtRutaIndex, lblRutaindex,
							resbundle.getString("rutaDefaultIndexFAutor"));
				} else if (radioArt.isSelected()) {
					rutaJatsRepo = validarRutas(txtJatsRepo.getText(), txtJatsRepo, lblRutaRepo,
							resbundle.getString("rutaDefaultJR"));
					rutaIndex = validarRutas(txtRutaIndex.getText(), txtRutaIndex, lblRutaindex,
							resbundle.getString("rutaDefaultIndexArt"));
				}
				else if (radioLibros.isSelected()) {
					rutaJatsRepo = validarRutas(txtJatsRepo.getText(), txtJatsRepo, lblRutaRepo,
							resbundle.getString("rutaDefaultXMLlibros"));
					rutaIndex = validarRutas(txtRutaIndex.getText(), txtRutaIndex, lblRutaindex,
							resbundle.getString("rutaDefaultIndexLibros"));
					System.out.println("ruta xml's: " + rutaJatsRepo);
					System.out.println("ruta indice de libros: " + rutaIndex);
				}
				continuar = (radioLibros.isSelected()) ? true : validarConexionDB();
				if (continuar) {
					if (radioArt.isSelected() || radioFAutor.isSelected() || radioLibros.isSelected()) {
						ListaFilesXML threadFiles = new ListaFilesXML(rutaJatsRepo, lblValConex, btnGenerar);
						threadFiles.run();
						// if (listarxml.getFilesXML().isEmpty()) {
						// lblValConex.setTextFill(Color.web("#EC7063"));
						// lblValConex.setText(
						// lblValConex.getText() + " �ERROR! No se encontrar�n XMLs en la ruta
						// indicada");
						// } else {
						// asignarDefault(3, false);
						// }
					} 
					else {
						asignarDefault(3, false);
					}

				}
			} else if (radioJson.isSelected() || radioLLenadoTabla.isSelected()) {
				continuar = validarConexionDB();
				asignarDefault(3, false);
				asignarDefault(4, true);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	/**
	 * Al ser presionado el <code>Button 'Generar'</code> se iniciara con el proceso
	 * de generaci�n de �ndices, dependiendo de la opci�n elegida. <br>
	 * Cuando este proceso inicia se desactivan los componentes como los
	 * <code>RadioButtons</code> y los <code>TextFields</code> <br>
	 * 
	 * @see org.csg.Indexador.DAO.ArticuloDAO
	 * @see org.csg.Indexador.DAO.RevistaDAO
	 */
	@FXML
	public void accionbtnGenerar() {
		btnValidarRutas.setDisable(true);
		crearListComp();
		asignarDefault(2, false);
		asignarDefault(7, false);
		if (radioRev.isSelected()) {
			eliminarIndex(rutaIndex);
			asignarDefault(3, true);
			asignarDefault(5, true);
			asignarDefault(6, false);
			Thread tP = new RevistaDAO(rutaIndex, connect, componentes);/**tarea1 conectarse a la base de redalyc oracle*/
			tP.start();
			if (!tP.isAlive()) {
				asignarDefault(3, false);
				asignarDefault(5, false);
				asignarDefault(6, true);
			}
			postgresql.closeConnect(connect);
		} else if (radioArt.isSelected()) {
			eliminarIndex(rutaIndex);
			asignarDefault(3, true);
			asignarDefault(5, true);
			asignarDefault(6, false);
			Thread hiloArt = new ArticuloSDAO(rutaIndex, ListaFilesXML.filesXML, connect, componentes);/**tarea1 caqui lee xmls artículos*/
			hiloArt.start();
		} else if (radioJson.isSelected()) {
			asignarDefault(3, true);
			asignarDefault(5, true);
			asignarDefault(6, false);
			Thread hiloJson = new jsonColeccionRevistas(resbundle.getString("rutaDefaultIndexRev"),
					resbundle.getString("rutaDefaultIndexArt"), resbundle.getString("rutaDefaultIndexLibros"), componentes);
			hiloJson.start();
		} else if (radioLLenadoTabla.isSelected()) {
			asignarDefault(3, true);
			asignarDefault(5, true);
			asignarDefault(6, false);
			Thread hiloLlenadoTablas = new ProcesoLlenarTablaPalClave(resbundle.getString("rutaDefaultIndexArt"),
					componentes);
			hiloLlenadoTablas.start();
		} else if (radioFAutor.isSelected()) {
			eliminarIndex(rutaIndex);
			asignarDefault(3, true);
			asignarDefault(5, true);
			asignarDefault(6, false);
			Thread hiloAutores = new AutoresDAO(rutaIndex, ListaFilesXML.filesXML, connect, componentes);
			hiloAutores.start();
		} else if (radioLibros.isSelected()) {
			System.out.println("Se prepara la generación del índice: " + rutaIndex);
			eliminarIndex(rutaIndex);
			asignarDefault(3, true);
			asignarDefault(5, true);
			asignarDefault(6, false);
			Thread hiloLibros = new LibroDAO(rutaIndex, ListaFilesXML.filesXML, componentes);
			hiloLibros.start();
		}
	}

	/**
	 * Guarda los componentes en una lista, para ser utilizados en las clases DAO.
	 * Ayuda a actualizar los datos de los componentes de la escena
	 */
	public void crearListComp() {
		componentes.add(0, radioArt);
		componentes.add(1, radioRev);
		componentes.add(2, btnValidarRutas);
		componentes.add(3, lblRutaindex);
		componentes.add(4, lblRutaRepo);
		componentes.add(5, txtArea);
		componentes.add(6, pbProgress);
		componentes.add(7, lblValConex);
		componentes.add(8, fieldprogress);
		componentes.add(9, txtLog);
		componentes.add(10, radioJson);
		componentes.add(11, radioLLenadoTabla);
		componentes.add(12, btnGenerar);
		componentes.add(13, radioFAutor);
		componentes.add(14, radioLibros);
	}

	/**
	 * Detecta cuando el cursor <code>isClicked</code> sobre el
	 * <code>TextField 'jatsRepo'</code> para desactivar o deshabilitar el
	 * <code>Button 'Generar'</code>
	 */
	@FXML
	public void cursorDetectedJats() {
		txtJatsRepo.setOnMouseClicked(e -> {
			asignarDefault(3, true);
			asignarDefault(2, true);
			asignarDefault(7, true);
		});
	}

	/**
	 * Detecta cuando el cursor <code>isClicked</code> sobre el
	 * <code>TextField 'jatsRepo'</code> para desactivar o deshabilitar el
	 * <code>Button 'Generar'</code>
	 */
	@FXML
	public void cursorDetectedIndex() {
		txtRutaIndex.setOnMouseClicked(e -> {
			asignarDefault(3, true);
			asignarDefault(2, true);
			asignarDefault(7, true);
		});
	}

	/**
	 * <p>
	 * Valida que la rutas ingresadas sean validas, en caso contrario se asignaran
	 * las rutas por default, estan son:
	 * <p>
	 * <ul>
	 * <li>jatsRepo -> C:\jatsRepo</li>
	 * <li>�ndice de Art�culos -> C:\indiceArticulos</li>
	 * <li>�ndice de Revistas -> C:\indiceRevistas</li>
	 * </ul>
	 * <p>
	 * En el caso del jatsRepo se validara que el directorio ingresado exista y que
	 * contenga los xmls correspondientes, de lo contrario no se iniciara con el
	 * proceso de indexaci�n. <br>
	 * En el caso de la ruta en donde se alojara el �ndice, se validara que esta
	 * exista. En caso contrario ser� creada, si el texto ingresado no es valido
	 * para la creaci�n de la ruta, se utilizaran las rutas por default
	 * <p>
	 * 
	 * @param ruta
	 *            String que indica la ruta ingresada que ser� validada
	 * @param txtfield
	 *            Indica el <code>TextField</code> de donde se extrajo la ruta a
	 *            validar
	 * @param label
	 *            Indica el <code>Label</code> al cual se enviara el mensaje de
	 *            aviso para indicar el estado de la ruta
	 * @param rutaDefault
	 *            String que indica la ruta por default a utilizar
	 * @return String que contiene la ruta a utilizar de acuerdo a las validaciones
	 *         previamente realizadas
	 */
	public String validarRutas(String ruta, TextField txtfield, Label label, String rutaDefault) {
		String rutaReturn = "";
		if (ruta.equals("") || ruta.equals(null)) {
			txtfield.setPromptText(rutaDefault);
			label.setTextFill(Color.web("#28B463")); // Verde
			label.setText("Se utilizará rutaDefault");
			rutaReturn = rutaDefault;
		} else {
			File file = new File(ruta);
			if (file.exists()) {
				label.setTextFill(Color.web("#28B463"));
				label.setText("Ruta Valida!...");
				rutaReturn = ruta;
			} else {
				if (!file.mkdirs()) {
					label.setTextFill(Color.web("#EC7063")); // Rojo
					label.setText("Ruta Invalida!. Se usara rutaDefault");
					txtfield.setPromptText(rutaDefault);
					rutaReturn = rutaDefault;
				} else {
					file.mkdirs();
					label.setTextFill(Color.web("#28B463"));
					label.setText("Ruta Valida Creada!.");
					rutaReturn = ruta;
				}

			}
		}
		return rutaReturn;
	}

	/**
	 * <p>
	 * Valida que la conexi�n a la base de datos no sea <code>null</code>
	 * <p>
	 * 
	 * @return Un valor booleano
	 *         <ul>
	 *         <li><code>true</code> -> Si la conexi�n fue aceptada y creada</li>
	 *         <li><code>false</code> -> Si la conexi�n no fue aceptada y es
	 *         <code>null</code></li>
	 *         </ul>
	 */
	public Boolean validarConexionDB() {
		boolean continuar = false;
		try {
			this.connect = postgresql.conectar();
			lblValConex.setText("Validando Conexi�n....");
			if (connect != null) {
				lblValConex.setTextFill(Color.web("#28B463"));
				lblValConex.setText("******* Successful Connection ******");
				continuar = true;
			} else {
				lblValConex.setTextFill(Color.web("#EC7063"));
				lblValConex.setText("Conexión rechazada. Verifique que el nombre del Host y el puerto sean"
						+ " correctos y que postmaster este aceptando conexiones TCP/IP.");
				asignarDefault(1, true);
				asignarDefault(2, true);
				continuar = false;
			}
		} catch (Exception ex) {

		} finally {
			postgresql.closeConnect(connect);
		}
		return continuar;
	}

	/**
	 * <p>
	 * Asigna a determinado componente de la escena un valor por default, la
	 * opciones son las siguientes:
	 * </p>
	 * <ul>
	 * <li>Opci�n 1: El valor de los <code>TextFiels</code> = " "</li>
	 * <li>Opci�n 2: El valor de los <code>Labels</code> = " "</li>
	 * <li>Opci�n 3: El <code>Button 'Generar'</code> = este o no deshabilitado</li>
	 * <li>Opci�n 4: El <code>Button 'Validar'</code> = este o no deshabilitado</li>
	 * <li>Opci�n 5: Los
	 * <code>ButtonRadio 'indiceArticulos' & 'indice Revistas' </code> = esten o no
	 * deshabilitados</li>
	 * <li>Opci�n 6: Las cajas de texto de las rutas esten o no desabilitadas</li>
	 * <li>Opci�n 7: El mensaje de aviso de conexi�n a la base de datos sea = "
	 * "</li>
	 * </ul>
	 * 
	 * @param opc
	 *            Entero que indica la opc�n a elegir
	 * @param bool
	 *            Valor booleano que indica el estado del componente a actualizar
	 */
	public void asignarDefault(int opc, Boolean bool) {
		switch (opc) {
		case 1:
			txtJatsRepo.setText("");
			txtJatsRepo.setPromptText(resbundle.getString("rutaDefaultJR"));
			txtRutaIndex.setText("");
			if (radioArt.isSelected()) {
				txtRutaIndex.setPromptText(resbundle.getString("rutaDefaultIndexArt"));
			} else if (radioRev.isSelected()) {
				txtRutaIndex.setPromptText(resbundle.getString("rutaDefaultIndexRev"));
			}
			break;
		case 2:
			lblRutaindex.setText("");
			lblRutaRepo.setText("");
			break;
		case 3:
			btnGenerar.setDisable(bool);
			break;
		case 4:
			btnValidarRutas.setDisable(bool);
			break;
		case 5:
			radioArt.setDisable(bool);
			radioRev.setDisable(bool);
			radioJson.setDisable(bool);
			radioLLenadoTabla.setDisable(bool);
			radioFAutor.setDisable(bool);
			break;
		case 6:
			txtJatsRepo.setEditable(bool);
			txtRutaIndex.setEditable(bool);
			break;
		case 7:
			lblValConex.setText("");
			break;
		}
	}

	/**
	 * Si existen ficheros o segmentos pertenecientes a un �ndice en la carpeta
	 * donde este se alojara, ser�n borrados.
	 * 
	 * @param pathIndex
	 *            String que indica la ruta a la que se accedera para corroborar
	 *            dicha acci�n
	 */
	public void eliminarIndex(String pathIndex) {
		File file = new File(pathIndex);
		if (file.isDirectory()) {
			try {
				File[] ficheros = file.listFiles();
				for (int i = 0; i < ficheros.length; i++) {
					if (ficheros[i].isFile() && !ficheros[i].getName().equals("stopwords.txt")) {
						ficheros[i].delete();
					}
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		} else {
			System.out.println("�ERROR! Directorio no valido -> M�todo eliminarIndex");
		}

	}

	/**
	 * Determina si existen archivos con extensi�n <b>xml</b> en el directorio
	 * indicada como jatsRepo, si existen ser�n agregadas su rutas a una lista
	 * 
	 * @see application.ControllerIndexadorCSG#filesXML
	 * 
	 * @param directory
	 *            String que indica el directorio del jatsRepo
	 */
	public void listarXMLs(String directory) {
		try {
			File file = new File(directory);
			if (file.exists()) {
				if (file.isDirectory()) {
					try {
						File[] ficherosJATS = file.listFiles();

						for (int i = 0; i < ficherosJATS.length; i++) {
							if (ficherosJATS[i].isDirectory()) {
								listarXMLs(ficherosJATS[i].toString());
							} else {
								String xml = ficherosJATS[i].toString();
								String ext = FilenameUtils.getExtension(xml);
								if (ext.equals("xml")) {
									filesXML.add(xml);
								}
							}
						}
					} catch (Exception ex) {
						System.out.println(">>>>>>  El directorio -> " + file.toString() + " no contiene archivos ");
					}
				}
			} else {
				System.out.println(">>>>>>  �ERROR!.. Directorio de XMLs No Valido ");
				System.exit(0);
			}
		} catch (Exception ex) {
			System.out.println(">>>>>>  �ERROR!.. Directorio de XMLs No Valido ");
			System.exit(0);
		}
	}
}
