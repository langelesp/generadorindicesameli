
package org.csg.indice;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;

import org.apache.lucene.analysis.CharArraySet;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexableField;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;



public class LecturaIndice {
	Directory directory;
	StandardAnalyzer analyzer;
	
	public LecturaIndice() {
		try {
		  directory = FSDirectory.open(Paths.get("C:\\Users\\Emmanuel\\Desktop\\Marcalyc\\Productos_Finales\\indicesAmeli\\indices19102020\\articulos"));
//	      directory = FSDirectory.open(Paths.get("C:\\Users\\Emmanuel\\Desktop\\Marcalyc\\Productos_Finales\\indicesAmeli\\indices19102020\\revistas"));
//		  directory = FSDirectory.open(Paths.get("C:\\Users\\Emmanuel\\Desktop\\Marcalyc\\Productos_Finales\\indicesAmeli\\libros"));
			analyzer = new StandardAnalyzer(stopwords());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	void leerIndex() {
		Query qu = null;
		try {
			try {
//     		qu = new QueryParser(" ", analyzer).parse("origen: [aA TO zZ]");//revistas//.parse("origen: [Bb TO Zz]");//.parse("clave:");
//     		qu = new QueryParser(" ", analyzer).parse("clave:260");//revistas//.parse("origen: [Bb TO Zz]");//.parse("clave:");
        	qu = new QueryParser(" ", analyzer).parse("claveRevista: 260");//articulos//.parse("claveRevista:");

			} catch (ParseException e) {

				e.printStackTrace();
			}

			
			int hitsPerPage = 500000;
			IndexReader reader;

			reader = DirectoryReader.open(directory);

			IndexSearcher searcher = new IndexSearcher(reader);
			TopDocs docs = searcher.search(qu, hitsPerPage);
			ScoreDoc[] hits = docs.scoreDocs;
			
			String ruta = "C:\\Users\\Emmanuel\\Desktop\\Marcalyc\\Productos_Finales\\indicesAmeli\\articulos_redalyc_extraidos_java/articulos_reda.txt";
            String claveArt = "contenido_prueba";
            File file = new File(ruta);
            
            // Si el archivo no existe es creado
            
            if (!file.exists()) {
                file.createNewFile();
            }
            FileWriter fw = new FileWriter(file);
            BufferedWriter bw = new BufferedWriter(fw);
			for (int i = 0; i < hits.length; ++i) {
				
				int docId = hits[i].doc;
				Document d = searcher.doc(docId);
				List<IndexableField> fields = d.getFields();
		//	System.out.println(d.get("clave"));
			
//	bw.write( "clave_articulo:" + d.get("clave"));
//	   bw.newLine();
				
	    	System.out.print("," + d.get("clave"));
		    System.out.print("," + d.get("nameRev"));
//			
			System.out.println("///" + i + "////////////////////////////////////////////////////////////////////////////");
			for (IndexableField field:fields) {
				if(field.name() == "directorio") {
					System.out.println("aqui entro");
					System.out.println(d.get(field.name()).split("<<<"));
				}else{
					System.out.println("--" + field.name() + "::" + d.get(field.name()));
				}
				
			}
			System.out.println("///////////////////////////////////////////////////////////////////////////////");
			System.out.println("");
			
    	    bw.write( "clave_articulo:" + d.get("claveArt"));
			    bw.newLine();
			
//				System.out.println( //ESLI
//					"\n claveArt: " + d.get("claveArt")
//					"\n tituloArt: " + d.get("tituloArt") +"::"+ i +
//					"\n palabrasClave: " + d.get("pais") +
//					"\n idioma: " + d.get("idioma") +
//					"\n fuedirectory = FSDirectory.open(Paths.get("C:\\Users\\Emmanuel\\Desktop\\Marcalyc\\Productos_Finales\\indicesAmeli\\revistas"));nte: " + d.get("origen") +
//					"\n resumen: " + d.get("resumen")
//			);
			}
			 bw.close();
			 
			System.out.println("Found " + hits.length + " hits.");
		} catch (IOException e) {

			e.printStackTrace();
		}
	}
	
	private void pintaArticulosFaltantes() {
		try {
			/*para ameli*/
			String [] articulos = getFileNames("C:\\Users\\Redalyc91Cap\\Desktop\\Marcalyc\\Productos_Finales\\indicesAmeli\\xmlsArticulos\\JatsRepoAmeli", new String[]{".xml"});
			Query qu = null;
			int hitsPerPage = 500000;
			IndexReader reader = DirectoryReader.open(FSDirectory.open(Paths.get("")));
			IndexSearcher searcher = new IndexSearcher(reader);
			for(String cveArticulo: articulos) {
				qu = new QueryParser(" ", analyzer).parse("origen: " + cveArticulo);//articulos//.parse("claveRevista:");
				TopDocs docs = searcher.search(qu, hitsPerPage);
				ScoreDoc[] hits = docs.scoreDocs;
				if(hits.length == 0) {
					System.out.println("faltante::" + cveArticulo);
				}
			}
			/*para redalyc*/
			articulos = getFileNames("C:\\Users\\Redalyc91Cap\\Desktop\\Marcalyc\\Productos_Finales\\indicesAmeli\\xmlsArticulos\\JatsRepoRedalyc", new String[]{".xml"});
			
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private String[] getFileNames(String path, String[] formats) throws Exception{
		/*System.out.println("invocacion de metodo de carga de la carpeta: " + path + " con formatos filtro: " + formats.toString()); */ 
		File carpet = new File(path);
		  final String [] auxFormats =  formats;
			FilenameFilter fileNameFilter = new FilenameFilter() {
	            @Override
	            public boolean accept(File dir, String name) {
	               if(name.lastIndexOf('.') > 0)
	               {
	                  // get last index for '.' char
	                  int lastIndex = name.lastIndexOf('.');
	                  //System.out.println("dir" + dir + " filename: " + name);
	                  // get extension
	                  String str = name.substring(lastIndex);
	                  // match path name extension
	                  for (int i = 0; i < auxFormats.length; i++){
	                	  if(str.equals(auxFormats[i]) | str.equals(auxFormats[i].toUpperCase()))
		                  {
		                     return true;
		                  }
	                  }
	               }
	               return false;
	            }
	         };
			String [] fontsPath = carpet.list(fileNameFilter);
			return fontsPath;
	}

	public CharArraySet stopwords() {
		CharArraySet stopSet = null;
		try {
			stopSet = CharArraySet.copy(StandardAnalyzer.STOP_WORDS_SET);
			BufferedReader bufferedReader = new BufferedReader(new FileReader("C:\\Users\\Redalyc91Cap\\Desktop\\Marcalyc\\Productos_Finales\\indicesAmeli\\stopwords.txt"));
			String palabraStopWord;

			while ((palabraStopWord = bufferedReader.readLine()) != null) {
				// System.out.println("Palabra: " + palabraStopWord);
				stopSet.add(palabraStopWord);
			}
			bufferedReader.close();
		} catch (Exception ex) {

		}
		return stopSet;
	}
}